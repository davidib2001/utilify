using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;

namespace Utilify.Platform.Demo.FractalGenerator.Worker
{
    [Serializable]
    public class LocalFractalWorker
    {
        private int xSegmentNum;
        private int ySegmentNum;
        private int segmentWidth;
        private int segmentHeight;
        private int xOffset;
        private int yOffset;
        private int xOffsetMain;
        private int yOffsetMain;
        private int zoom;
        private int iterations;
        //private Color[] palette;
        private Color[] colors;
        private float[] colorPositions;
        private Bitmap image;
        private Bitmap palette;

        public LocalFractalWorker() { }
        public LocalFractalWorker(
            int xSegment, int ySegment, int segmentWidth, int segmentHeight, int xOffset, int yOffset, int zoom, int iterations, ColorBlend blend)
        {
            this.xSegmentNum = xSegment;
            this.ySegmentNum = ySegment;
            this.segmentWidth = segmentWidth;
            this.segmentHeight = segmentHeight;
            this.xOffset = xOffset + xSegment * segmentWidth;
            this.yOffset = yOffset + ySegment * segmentHeight;
            this.zoom = zoom;
            this.iterations = iterations;
            this.colors = blend.Colors;
            this.colorPositions = blend.Positions;
            //this.palette = palette;
        }

        public void Generate()
        {
            CreatePalette();

            image = new Bitmap(segmentWidth, segmentHeight);

            for (double x = 0; x < segmentWidth; x++)
            {
                for (double y = 0; y < segmentHeight; y++)
                {
                    Complex z = new Complex(0, 0);
                    Complex c = new Complex((x + xOffset) / zoom, (y + yOffset) / zoom);

                    int i;
                    bool inSet = true;
                    for (i = 0; i < iterations; i++)
                    {
                        //z = cos(z) + z*log(c)
                        //z = (z*z) * Complex.Cos(z) + c;
                        //z = z * z * z * z + c;
                        //z = (z*Complex.Sin(z*z)) + Complex.Cos(c);
                        z = (z * z) + c;

                        if (Double.IsInfinity(z.real) | Double.IsNaN(z.imaginary))
                        {
                            inSet = false;
                            break;
                        }
                    }
                    if (inSet)
                        image.SetPixel((int)x, (int)y, Color.Black);
                    else
                    {
                        image.SetPixel((int)x, (int)y, palette.GetPixel(i/500,0));
                    }

                    //Thread.Sleep(1000);

                    // Draw fractal's coordinate center
                    if (Math.Abs((xOffsetMain + (x + xOffset))) <= 2 && Math.Abs((yOffsetMain + (y + yOffset))) <= 2)
                    {
                        image.SetPixel((int)x, (int)y, Color.Red);
                    }

                    // Draw viewport center
                    if (xSegmentNum * segmentWidth + x == 195 && ySegmentNum * segmentHeight + y == 210)
                        image.SetPixel((int)x, (int)y, Color.Yellow);

                }
            }

            //Graphics gr = Graphics.FromImage(image);
            //gr.InterpolationMode = InterpolationMode.Low;
            //gr.DrawImage(image, new Point(0, 0));
        }

        private void CreatePalette()
        {
            // Create colour palette.
            ColorBlend blend = new ColorBlend();
            blend.Colors = colors;
            blend.Positions = colorPositions;
            Rectangle rect = new Rectangle(0, 0, iterations/500, 1);
            palette = new Bitmap(iterations/500, 1);
            Graphics gr = Graphics.FromImage(palette);
            LinearGradientBrush brush = new LinearGradientBrush(rect, Color.Black, Color.White, 0.0F);
            brush.InterpolationColors = blend;
            gr.FillRectangle(brush, rect);
        }

        #region Getters

        public int XSegmentNum
        {
            get { return xSegmentNum; }
        }

        public int YSegmentNum
        {
            get { return ySegmentNum; }
        }

        public int SegmentWidth
        {
            get { return segmentWidth; }
        }

        public int SegmentHeight
        {
            get { return segmentHeight; }
        }

        public Bitmap Image
        {
            get
            {
                if (image == null)
                    throw new InvalidOperationException("Result has not been generated yet.");

                return image;
            }
        }

        #endregion

        #region Julia Set
        /*
        private void DoJulia()
        {
            Complex c = new Complex(-1.0, -5.0);

            for (double x = 0; x < width; x++)
            {
                for (double y = 0; y < height; y++)
                {
                    Complex z = new Complex((x + xOffset) / zoom, (y + yOffset) / zoom);

                    int i;
                    bool inSet = true;
                    for (i = 0; i < paletteSize; i++)
                    {
                        z = (z * z) + c;

                        if (Double.IsInfinity(z.real) | Double.IsNaN(z.imaginary))
                        {
                            inSet = false;
                            break;
                        }
                    }
                    if (inSet)
                        localBitmap.SetPixel((int)x, (int)y, Color.Black);
                    else
                    {
                        localBitmap.SetPixel((int)x, (int)y, palette[i]);
                    }
                }
            }
            localDisplay.Refresh();
        }
*/
        //public void Generate()
        //{
        //    image = new Bitmap(segmentWidth, segmentHeight);
        //    Complex c = new Complex(0.9222,0.7111);

        //    for (double x = 0; x < segmentWidth; x++)
        //    {
        //        for (double y = 0; y < segmentHeight; y++)
        //        {
        //            Complex z = new Complex((x + xOffset) / zoom, (y + yOffset) / zoom);

        //            int i;
        //            bool inSet = true;
        //            for (i = 0; i < palette.Length; i++)
        //            {
        //                //z = cos(z) + z*log(c)
        //                //z = (z*z) * Complex.Cos(z) + c;
        //                //z = z * z * z * z + c;
        //                z = (z * z * c) + (c * z);

        //                if (Double.IsInfinity(z.real) | Double.IsNaN(z.imaginary))
        //                {
        //                    inSet = false;
        //                    break;
        //                }
        //            }
        //            if (inSet)
        //                image.SetPixel((int)x, (int)y, Color.Black);
        //            else
        //            {
        //                image.SetPixel((int)x, (int)y, palette[i]);
        //            }

        //            // Draw fractal's coordinate center
        //            if (Math.Abs((xOffsetMain + (x + xOffset))) <= 2 && Math.Abs((yOffsetMain + (y + yOffset))) <= 2)
        //            {
        //                image.SetPixel((int)x, (int)y, Color.Red);
        //            }

        //            // Draw viewport center
        //            if (xSegmentNum * segmentWidth + x == 195 && ySegmentNum * segmentHeight + y == 210)
        //                image.SetPixel((int)x, (int)y, Color.Yellow);

        //        }
        //    }
        //}
        #endregion

    }
}
