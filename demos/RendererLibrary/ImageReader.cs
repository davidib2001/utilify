/** 
 * The template class for other image readers
 * This class defines a set of data and methods commonly to all
 * image readers.
 */
/**
 * This software is provided "AS IS," without a warranty of any kind.
 * anyone can use it for free,emails are welcomed concerning bugs or
 * suggestions.
 */
/**
 * ImageReader.java.  
 *
 * @version 1.0  03/28/2007
 * @author Wen Yu, yuwen_66@yahoo.com
 */

using System.IO;
using System.Drawing;
namespace Renderer
{
    public abstract class ImageReader
    {
        // Data field, place holder, the real values are to be set by a subclass
        internal int width = 0;
        internal int height = 0;
        internal int[] pix = null;
        internal Bitmap img = null;
        internal int bitsPerPixel = 0;
        // IndexedColor related variables 
        internal bool indexedColor = false;
        internal int[] colorPalette = null;
        // End of indexedColor related variables

        // Entry method, to be implemented by specific ImageReader subclass
        public abstract void unpackImage(Stream stream);
        /**
	    public abstract String[] getImageInfo()
	    {
		    //get the header information of the image
		    //to be done
	    }
	     */
        //Get the color depth of the image to determine whether a colorPalette exists
        public int getColorDepth()
        {
            return bitsPerPixel;
        }
        //Returns a colorPalette for an indexed-color image
        public int[] getColorPalette()
        {
            return colorPalette; // call this method if getImageColorDepth() <= 8
        }
        // Is it an indexedColor image
        public bool isIndexedColor()
        {
            return indexedColor;
        }
        //Returns the Dimension of the image
        public Size getImageSize()
        {
            return new Size(width, height);
        }
        //Returns an integer array representation of the image 
        //in alpha_RGB sequence
        public int[] getImageData()
        {
            return pix;// check about null pointer after method call ... 
        }
        //Returns the decoded image as a java Image object 
        //check about null pointer after method call!
        public Image getImage()
        {
            if (img != null)
            {
                return img;
            }
            else return null;
        }
    }
}