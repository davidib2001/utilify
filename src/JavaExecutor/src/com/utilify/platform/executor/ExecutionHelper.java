package com.utilify.platform.executor;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import com.utilify.framework.JobType;

public class ExecutionHelper {
    public static final String EXECUTION_JAR_FILENAME = "Utilify.Execution.jar";

    public static String getExecutorTypeName(JobType type) {
        String typeName = null;

        // Consider that ClientPlatform == Java
        if (type == JobType.CODE_MODULE) {
            typeName = "com.utilify.platform.executor.JvmExecutor";
        } else if (type == JobType.NATIVE_MODULE) {
        	typeName = "com.utilify.platform.executor.NativeExecutor";
        }
        return typeName;
    }

    public static URL getExecutionJarURL() throws MalformedURLException {
    	return (new File("./" + EXECUTION_JAR_FILENAME)).toURL();
    }
}
