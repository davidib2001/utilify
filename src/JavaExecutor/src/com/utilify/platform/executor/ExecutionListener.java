package com.utilify.platform.executor;

public interface ExecutionListener {
	void executionComplete(Object jvmExecutor, ExecutionCompleteEventArgs data);
}
