package com.utilify.platform.test;


import com.utilify.framework.ExecutionContext;
import com.utilify.framework.client.Executable;

@SuppressWarnings("serial")
public class AdditionJob implements Executable {
	
     int valueA;
     int valueB;
     int result;

     public AdditionJob(int valueA, int valueB) {
         this.valueA = valueA;
         this.valueB = valueB;
     }

     public void execute(ExecutionContext context) {
         System.out.println("Adding " + valueA + " and " + valueB);
         this.result = this.valueA + this.valueB;
     }

     public int getValueA()
     {
         return this.valueA;
     }

     public int getValueB()
     {
         return this.valueB;
     }

     public int getResult()
     {
         return this.result;
     }
 }