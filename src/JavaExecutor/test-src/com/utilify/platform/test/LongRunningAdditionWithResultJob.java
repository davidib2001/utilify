package com.utilify.platform.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.utilify.framework.ExecutionContext;
import com.utilify.framework.ResultRetrievalMode;
import com.utilify.framework.client.Executable;
import com.utilify.framework.client.ResultInfo;
import com.utilify.framework.client.Results;

@SuppressWarnings("serial")
public class LongRunningAdditionWithResultJob implements Executable, Results {

	int valueA;
	int valueB;
	int sleepTime;
	int result;

	String resultFilename = "addition.result";

	public LongRunningAdditionWithResultJob(int valueA, int valueB, int sleepTime) {
		this.valueA = valueA;
		this.valueB = valueB;
		this.sleepTime = sleepTime;
	}

	public void execute(ExecutionContext context) throws IOException {
		String resultPath = context.getWorkingDirectory() + File.separator
				+ resultFilename;
		System.out.println("Adding " + valueA + " and " + valueB + " to "
				+ resultPath + ".");
		this.result = this.valueA + this.valueB;
		File outputFile = new File(resultPath);
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
		String line1 = result + "\n";
		writer.write(line1, 0, line1.length());
		writer.flush();
		
		try { Thread.sleep(sleepTime); } catch (Exception e){ e.printStackTrace(); }
		
		String line2 = "slept for " + sleepTime + " ms.";
		writer.write(line2, 0, line2.length());
		writer.flush();
		writer.close();
	}

	public int getValueA() {
		return this.valueA;
	}

	public int getValueB() {
		return this.valueB;
	}

	public int getResult() {
		return this.result;
	}

	public ResultInfo[] getExpectedResults() {
		ResultInfo info = new ResultInfo("addition.result",
				ResultRetrievalMode.BACK_TO_ORIGIN);
		return new ResultInfo[] { info };
	}
}