package com.utilify.platform.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.utilify.framework.ApplicationInitializationException;
import com.utilify.framework.ErrorEvent;
import com.utilify.framework.ErrorListener;
import com.utilify.framework.JobCompletedEvent;
import com.utilify.framework.JobCompletedListener;
import com.utilify.framework.client.Application;
import com.utilify.framework.client.Job;
import com.utilify.framework.client.ResultContent;
import com.utilify.framework.client.ResultStatusInfo;

public class SuperAdditionTestProgram implements JobCompletedListener, ErrorListener {

    private static Logger logger = Logger.getLogger(SuperAdditionTestProgram.class.getName());
    private static Application app = null;

    private int numJobsDone = 0 ;
//    public static void main1(String[] args) throws IOException {
//    
//    	URL u = new URL("http://192.168.0.9:8080/Utilify.Platform.Manager/ApplicationManager");
//    	System.out.println("Its ok " + u.toString());
//    }
    
    public static void main(String[] args) throws IOException, 
    	ApplicationInitializationException, InterruptedException {

    	Logger rootLogger = Logger.getLogger("");
    	Handler[] handlers = rootLogger.getHandlers();
    	for (int i = 0; i < handlers.length; i++){
    		if (handlers[i] instanceof ConsoleHandler){
    			handlers[i].setLevel(Level.ALL);
    		}
    	}
    	
        logger.info("Started logging...");

        SuperAdditionTestProgram prog = new SuperAdditionTestProgram();
        prog.test();
        
        System.out.println("Press enter to exit...");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        br.readLine();
    }

    private void test() throws ApplicationInitializationException, InterruptedException, IOException{
    	logger.fine("Starting client api test...");

    	//Collection<DependencyInfo> deps = DependencyResolver.detectDependencies(SuperAdditionJob.class);
    	
        app = new Application("My Addition Application"); //, deps);
        
        for (int i = 0; i < 25; i++) {
            SuperAdditionJob job = new SuperAdditionJob(i, i);
            app.addJob(job);
        }

        app.addErrorListener(this);
        app.addJobCompletedListener(this);
        
        logger.info("Submitting Application");
        app.start();

        System.out.println("Waiting for results...");
    }
    
	public void onError(ErrorEvent event) {
		System.out.println("I blew up: \n" +  event.getException().toString());
		if (event.getException().getCause() != null)
			System.out.print("Cause: ");
			event.getException().getCause().printStackTrace();
	}

	public void onJobCompleted(JobCompletedEvent event)  {
		Job remoteJob = event.getJob();
        SuperAdditionJob myJob = (SuperAdditionJob)remoteJob.getCompletedInstance();
        synchronized (this) {
        	numJobsDone++;	
		}
        String s = String.format("%d-th Job %s Completed in %d ms. %d + %d + %d = %d",
                numJobsDone,
        		remoteJob.getId(),
                (event.getJob().getFinishTime().getTime() - remoteJob.getStartTime().getTime()),
                myJob.getValueA(),
                myJob.getValueB(),
                myJob.GetInputValue(),
                myJob.getResult());
        System.out.println(s);
        // Try to get the result file

        try {
	        ResultStatusInfo[] resultInfos = app.getJobResults(remoteJob);
	        System.out.println(String.format("Getting %d Results", resultInfos.length));
	        for (ResultStatusInfo info : resultInfos)
	        {
	            System.out.println(String.format("Getting content for result %s filename %s", info.getResultId(), info.getInfo().getFilename()));
	            ResultContent content = app.getResultContent(info);
	            System.out.println(String.format("Got content %d bytes", content.getContent().length));
	            File resultsDir = new File("results");
	            if (!resultsDir.exists())
	            	resultsDir.mkdir();
	            File resultFile = new File("results/result_" + info.getResultId());
	            if(resultFile.createNewFile()) {
	            	FileOutputStream stream = new FileOutputStream(resultFile);
		            stream.write(content.getContent());
		            stream.flush();
		            stream.close();
	            }
	        }
        } catch (Exception e) {
        	e.printStackTrace();
        	System.out.println("Had a bit of trouble writing out those results. I'm dead.");
        }
	}
}
