using System;
using System.Collections.Generic;
using Utilify.Platform;
using System.Reflection;

namespace Utilify.Framework
{
    internal class ClientJobTable
    {
        #region Dictionaries for keeping track of Jobs
        
        // NOTE : Multiple lists are used here to improve performance and prevent having to iterate through multiple lists
        // Need to ensure that they are kept in sync.

        List<Job> newJobs = new List<Job>();

        // ONLY used for Jobs that have been submitted but have not sent their dependencies yet.
        // Dictionary mapping JobIds to Jobs 
        Dictionary<String, Job> idToSubmittedJobs =
            new Dictionary<string, Job>();

        // ONLY used for Jobs that should be monitored!
        // Dictionary mapping JobIds to Jobs 
        Dictionary<String, Job> idToActiveJobs =
            new Dictionary<string, Job>();

        // ONLY used for Jobs that are Complete or Cancelled!
        // Dictionary mapping JobIds to Jobs 
        Dictionary<String, Job> idToInactiveJobs =
            new Dictionary<string, Job>();


        #endregion

        Logger logger = new Logger();

        #region Events

        internal event EventHandler<JobStatusEventArgs> JobStatusChanged;

        #endregion

        /// <summary>
        /// Creates an empty ClientJobTable.
        /// </summary>
        internal ClientJobTable() { }
       
        private void ChangeJobStatus(Job job, JobStatus status)
        {
            ////use reflection to change the status: since it is internal:
            //PropertyInfo prop = typeof(Job).GetProperty("Status", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            //if (prop != null)
            //{
            //    prop.SetValue(job, status, null);
            //}
            job.Status = status;
        }

        /// <summary>
        /// Adds a Job to the ClientJobTable.
        /// </summary>
        /// <remarks>
        /// The 'Add' methods are the only way a Job can be introduced to the ClientJobTable.
        /// </remarks>
        /// <param name="job"></param>
        internal void Add(Job job)
        {
            if (job == null)
                throw new ArgumentNullException("job");

            if (job.Id != null && job.Id.Trim().Length > 0)
                throw new ArgumentException("Cannot Add Job. Job already has an Id, so it must already be submitted.", "job");

            if (job.Status != JobStatus.UnInitialized)
                throw new ArgumentException("Cannot Add Job with Status = " + job.Status, "job");
            
            Add(job, JobStatus.UnInitialized);
        }

        private void Add(Job job, JobStatus status)
        {
            lock (this)
            {
                if (newJobs.Contains(job))
                    throw new ArgumentException("Cannot Add Job. Job already exists.", "job");

                // Should be atomic
                newJobs.Add(job);

                // Raise the Event.
                JobStatusEventArgs args = new JobStatusEventArgs(status);
                EventHelper.RaiseEvent<JobStatusEventArgs>(JobStatusChanged, this, args);                
            }
        }

        /// <summary>
        /// Changes the Status of a Job.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="newStatus"></param>
        internal void ChangeStatus(Job job, JobStatus newStatus)
        {
            if (job == null)
                throw new ArgumentNullException("job");

            if ((job.Id == null || job.Id.Trim().Length == 0))
                throw new ArgumentException("Job id cannot be null or empty", "job");

            // Check for valid state transition.
            JobStatus oldStatus = job.Status;
            if (!IsValidStatusChange(oldStatus, newStatus))
                throw new InvalidTransitionException(String.Format("Cannot update Job Status. Job cannot transition from {0} to {1}.", oldStatus, newStatus));

            lock (this)
            {
                switch (newStatus)
                {
                    case JobStatus.Submitted:

                        if (!newJobs.Contains(job))
                            throw new ArgumentException("Cannot update Job Status. Job does not exist.");
                        // Add to list of submitted jobs.
                        idToSubmittedJobs.Add(job.Id, job);
                        // Remove from newJobs.
                        newJobs.Remove(job);
                        break;
                    case JobStatus.Completed:
                    case JobStatus.Cancelled:

                        //cancelled jobs must have an id: they should be known to the Manager.
                        //otherwise we lose track of jobs: so what we do is: when a job that is not yet submitted is cancelled,
                        //we submit it to the Manager, but also tell the Manager to cancel it.

                        if (String.IsNullOrEmpty(job.Id) || (!idToActiveJobs.ContainsKey(job.Id) && !idToSubmittedJobs.ContainsKey(job.Id)))
                            throw new ArgumentException("Cannot update Job Status. Job does not exist.");
                        // Add to Inactive list
                        idToInactiveJobs.Add(job.Id, job);
                        if (oldStatus == JobStatus.Submitted)
                        {
                            // Remove Job from list so its dependencies are not submitted again.
                            idToSubmittedJobs.Remove(job.Id);
                        }
                        else
                        {
                            // Remove Job from list so its not monitored again.
                            idToActiveJobs.Remove(job.Id);
                        }
                        break;
                    default:
                        if (oldStatus == JobStatus.Submitted)
                        {
                            if (String.IsNullOrEmpty(job.Id) || !idToSubmittedJobs.ContainsKey(job.Id))
                                throw new ArgumentException("Cannot update Job Status. Job does not exist.");
                            // Add to Active list
                            idToActiveJobs.Add(job.Id, job);
                            // Remove Job from list so its dependencies are not submitted again.
                            idToSubmittedJobs.Remove(job.Id);
                        } 
                        // else do nothing
                        break;
                }

                // Change internal Job status
                // Need to set Status inside the Job too (shouldnt change the status anywhere outside here!).
                // For now, lets make private and use reflection to prevent others from being tempted to change the status.
                // Note: No need to change status for Adding Job.
                ChangeJobStatus(job, newStatus);

                // Raise the Event.
                JobStatusEventArgs args = new JobStatusEventArgs(newStatus);
                EventHelper.RaiseEvent<JobStatusEventArgs>(JobStatusChanged, this, args);                
            }
        }

        /// <summary>
        /// Checks whether the given Status change is a valid one.
        /// </summary>
        /// <param name="oldStatus"></param>
        /// <param name="newStatus"></param>
        /// <returns></returns>
        private bool IsValidStatusChange(JobStatus oldStatus, JobStatus newStatus)
        {
            bool isValid = false;

            switch (oldStatus)
            {
                case JobStatus.UnInitialized:
                    if (newStatus == JobStatus.Submitted ||
                        newStatus == JobStatus.Cancelled)
                        isValid = true;
                    break;
                case JobStatus.Submitted:
                    if (newStatus == JobStatus.Ready ||
                        newStatus == JobStatus.Scheduled ||
                        newStatus == JobStatus.Executing ||
                        newStatus == JobStatus.Completing ||
                        newStatus == JobStatus.Completed ||
                        newStatus == JobStatus.Cancelled)
                        isValid = true;
                    break;
                case JobStatus.Ready:
                    if (newStatus == JobStatus.Scheduled ||
                        newStatus == JobStatus.Executing ||
                        newStatus == JobStatus.Completing ||
                        newStatus == JobStatus.Completed ||
                        newStatus == JobStatus.Cancelled)
                        isValid = true;
                    break;
                case JobStatus.Scheduled: //scheduled jobs can be reset to Ready if executors don't pick it up
                    if (newStatus == JobStatus.Ready ||
                        newStatus == JobStatus.Executing ||
                        newStatus == JobStatus.Completing ||
                        newStatus == JobStatus.Completed ||
                        newStatus == JobStatus.Cancelled)
                        isValid = true;
                    break;
                case JobStatus.Executing:
                    if (newStatus == JobStatus.Scheduled || // Job will go from Executing -> Scheduled if the Job was Reset
                        newStatus == JobStatus.Ready || // Job may go from Executing -> Ready on the Client if a Reset Job goes from Scheduled -> Ready before Client has time to update.
                        newStatus == JobStatus.Completing ||
                        newStatus == JobStatus.Completed ||
                        newStatus == JobStatus.Cancelled)
                        isValid = true;
                    break;
                case JobStatus.Completing:
                    if (newStatus == JobStatus.Scheduled || //a job may go from Completing -> Scheduled  if it was reset. it goes Completing -> Ready -> Scheduled before Client has time to update.
                        newStatus == JobStatus.Ready || // Job may go from Completing -> Ready on the Client if a job was reset.
                        newStatus == JobStatus.Completed ||
                        newStatus == JobStatus.Cancelled)
                        isValid = true;
                    break;
            }

            return isValid;
        }

        /// <summary>
        /// Returns all the Ids for Jobs based on StatusGroup
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        internal String[] GetJobIds(StatusGroup status)
        {
            lock (this)
            {
                List<String> jobs = new List<String>();
                switch (status)
                {
                    case StatusGroup.SubmittedWaitingForDependencies:
                        foreach (string key in idToSubmittedJobs.Keys)
                        {
                            Job j = idToSubmittedJobs[key];
                            if (!j.SentDependencies)
                                jobs.Add(key);
                        }
                        break;
                    case StatusGroup.Submitted:
                        jobs.AddRange(idToSubmittedJobs.Keys);
                        break;
                    case StatusGroup.Active:
                        jobs.AddRange(idToActiveJobs.Keys);
                        break;
                    case StatusGroup.Inactive:
                        jobs.AddRange(idToInactiveJobs.Keys);
                        break;
                }
                return jobs.ToArray();
            }
        }

        internal Job[] GetJobs(StatusGroup status)
        {
            return GetJobs(status, 0); //limit = 0, means get all
        }

        /// <summary>
        /// Returns all the Jobs based on StatusGroup
        /// </summary>
        /// <param name="status"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        internal Job[] GetJobs(StatusGroup status, int limit)
        {
            Job[] jobs = null;
            ICollection<Job> source = null;
            lock (this)
            {                
                switch (status)
                {
                    case StatusGroup.SubmittedWaitingForDependencies:
                        source = new List<Job>();
                        foreach (Job j in idToSubmittedJobs.Values)
                        {
                            if (!j.SentDependencies)
                                source.Add(j);
                        }
                        break;
                    case StatusGroup.Submitted:
                        source = idToSubmittedJobs.Values;
                        break;
                    case StatusGroup.Active:
                        source = idToActiveJobs.Values;
                        break;
                    case StatusGroup.Inactive:
                        source = idToInactiveJobs.Values;
                        break;
                    case StatusGroup.New:
                        source = newJobs;                        
                        break;
                }

                int size = (source.Count > limit && limit > 0) ? limit : source.Count;
                jobs = new Job[size];
                int i = 0;
                foreach(Job job in source)
                {
                    if (i >= jobs.Length)
                        break;

                    jobs[i++] = job;                    
                }
            }

            if (jobs == null)
                jobs = new Job[0]; //to make sure we don't return a null array.

            return jobs;
        }

        /// <summary>
        /// Returns the job with the given Id and status, if it exists.
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        internal Job GetJob(String jobId)
        {
            if (jobId == null)
                throw new ArgumentNullException("jobId");

            if (jobId.Trim().Length == 0)
                throw new ArgumentException("Job id cannot be empty");
                
            Job job = null;
            lock (this)
            {
                if (idToSubmittedJobs.ContainsKey(jobId))
                    job = idToSubmittedJobs[jobId];
                else if (idToActiveJobs.ContainsKey(jobId))
                    job = idToActiveJobs[jobId];
                else if (idToInactiveJobs.ContainsKey(jobId))
                    job = idToInactiveJobs[jobId];
                return job;
            }
        }

        /// <summary>
        /// Gets all Jobs.
        /// </summary>
        internal Job[] Jobs
        {
            get
            {
                lock (this)
                {
                    List<Job> jobs = new List<Job>(newJobs);
                    jobs.AddRange(idToSubmittedJobs.Values);
                    jobs.AddRange(idToActiveJobs.Values);
                    jobs.AddRange(idToInactiveJobs.Values);
                    return jobs.ToArray();                    
                }
            }
        }

        /// <summary>
        /// Returns the count of Jobs with a given Status.
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        internal int GetCount(StatusGroup status)
        {
            lock (this)
            {
                int count = 0;
                switch (status)
                {
                    case StatusGroup.SubmittedWaitingForDependencies:
                        foreach (Job j in idToSubmittedJobs.Values)
                        {
                            if (!j.SentDependencies)
                                count++;
                        }
                        break;
                    case StatusGroup.Submitted:
                        count = idToSubmittedJobs.Count;
                        break;
                    case StatusGroup.Active:
                        count = idToActiveJobs.Count;
                        break;
                    case StatusGroup.Inactive:
                        count = idToInactiveJobs.Count;
                        break;
                    case StatusGroup.New:
                        count = newJobs.Count;
                        break;
                }
                return count;
            }
        }

        /// <summary>
        /// Gets the total count of Jobs for all Statuses.
        /// </summary>
        internal int Count
        {
            get 
            {
                lock (this)
                {
                    int count = idToSubmittedJobs.Count; //this includes the SubmittedWaitingForDependencies
                    count += idToActiveJobs.Count;
                    count += idToInactiveJobs.Count;
                    count += newJobs.Count;
                    return count;
                }
            }
        }

        protected virtual void OnJobStatusChanged(JobStatus status)
        {
            JobStatusEventArgs args = new JobStatusEventArgs(status);
            EventHelper.RaiseEventAsync<JobStatusEventArgs>(JobStatusChanged, this, args);
        }

        internal bool Exists(Job job)
        {
            return newJobs.Contains(job)
                || idToSubmittedJobs.ContainsValue(job)
                || idToActiveJobs.ContainsValue(job)
                || idToInactiveJobs.ContainsValue(job);
        }

    }

    /// <summary>
    /// Event args passed when a Job changes status.
    /// </summary>
    internal class JobStatusEventArgs : EventArgs
    {
        private JobStatus status;

        internal JobStatusEventArgs()
            : base()
        { }

        internal JobStatusEventArgs(JobStatus status)
            : base()
        {
            this.status = status;
        }

        internal JobStatus Status
        {
            get { return status; }
            set { status = value; }
        }
    }

    internal enum StatusGroup
    {
        /// <summary>
        /// UnInitialized Jobs
        /// </summary>
        New = 0, 
        /// <summary>
        /// All Other active jobs i.e. not (Uninitialized, SubmittedWaitingForDependencies, Submitted, Completed, Cancelled)
        /// </summary>
        Active = 1,
        /// <summary>
        /// Complete or Cancelled Jobs
        /// </summary>
        Inactive = 2, 
        /// <summary>
        /// Submitted jobs that have no dependencies left to send
        /// </summary>
        Submitted = 3,
        /// <summary>
        /// Submitted Jobs that may not have their dependencies sent
        /// </summary>
        SubmittedWaitingForDependencies = 4
    }
}
