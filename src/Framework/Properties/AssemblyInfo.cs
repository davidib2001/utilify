﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Utilify.Framework")]
[assembly: AssemblyDescription("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("5e2c2f16-d0fb-4b67-9394-6c722cf7854c")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]

//required to make sure this library is usable by all .NET / CLR Languages
[assembly: CLSCompliant(true)]

#if CODE_ANALYSIS //Issue 054: can get the correct attrib code from FxCop - Copy As
//[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
#endif

#if TEST
[assembly: InternalsVisibleTo("Utilify.Framework.Tests")]
[assembly: InternalsVisibleTo("Utilify.Client.Tests")]
//[assembly: InternalsVisibleTo("Utilify.Platform.Executor.Tests")]
//[assembly: InternalsVisibleTo("Utilify.Platform.Tests.Common")]
//[assembly: InternalsVisibleTo("TestConsole")]
#endif