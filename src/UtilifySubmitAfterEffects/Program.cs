﻿using System;
using System.IO;
using Utilify.Framework.CommandLineClient.Utility;
using Utilify.Platform;

namespace Utilify.Framework.CommandLineClient.UtilifySubmitAfterEffects
{
    class Program
    {
        private static ConnectionSettings settings;
        private static ApplicationManagerClient client;
        private static Arguments arguments;

        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                PrintUsage();
                return;
            }
            arguments = new Arguments(args);
            DoConnection();

            // if its just the check then exit.
            if (arguments["check"] != null)
                Environment.Exit(0);

            // otherwise do submit
            DoSubmit();
        }

        private static void DoSubmit()
        {
            string fileArg = arguments["file"];
            string rqindexArg = arguments["rqindex"];
            string segmentsArg = arguments["segments"];
            string framesArg = arguments["frames"];

            if (!File.Exists(fileArg))
            {
                Fail("Project file not found.");
            }

            int rqindex = -1;
            if (!Int32.TryParse(rqindexArg, out rqindex) && rqindex <= 0)
            {
                Fail("Invalid Render Queue Index.");
            }

            int segments = -1;
            if (!Int32.TryParse(segmentsArg, out segments) && segments <= 0)
            {
                Fail("Invalid number of Segments specified.");
            }

            int frames = -1;
            if (!Int32.TryParse(framesArg, out frames) && frames <= 0)
            {
                Fail("Invalid number of Frames.");
            }

            // Get the path to After Effects
            string appVersion = "CS3";
            string aerenderPath = @"C:\Program Files\Adobe\Adobe After Effects " + appVersion + @"\Support Files";
                //Path.Combine("%programfiles%",@"\Adobe\Adobe After Effects " + appVersion + @"\Support Files");
            //aerenderPath = Environment.ExpandEnvironmentVariables(aerenderPath); // fill in program files dir (otherwise it doesnt get expanded for local service?)

            // Submit the Application
            string appId = client.SubmitApplication("After Effects");

            // Submit the Jobs. We now have all the information we need to submit a Job.
            int framesPerJob = frames / segments;
            for (int i = 0; i < segments; i++)
            {
                int startFrame = (i * framesPerJob) + 1;
                int endFrame = (i + 1) * framesPerJob;
                NativeJob job = new NativeJob();
                job.Command = Path.Combine(aerenderPath, "aerender.exe");
                job.Arguments = " -s " + startFrame + " -e " + endFrame +
                                " -rqindex " + rqindex +
                                " -project \"" + fileArg + "\"";

                // Ignoring JobIds
                client.SubmitJob(appId, job);
            }

            Console.WriteLine(appId);
        }

        private static void Fail(string msg)
        {
            if (msg != null)
                Console.WriteLine(msg);
            PrintUsage();
            Environment.Exit(1);
        }

        private static void PrintUsage()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("    UtilifySubmitAfterEffects <command> [command args] /h <protocol://hostname:port> /u <user:pass>");
        }

        private static void DoConnection()
        {
            string hostArg = arguments["h"];
            string userArg = arguments["u"];
            UriBuilder hostUri = null;
            string[] credential = null;

            try
            {
                if (string.IsNullOrEmpty(hostArg))
                {
                    Fail("Hostname not specified.");
                }
                hostUri = new UriBuilder(hostArg);

                if (!hostUri.Scheme.Equals(Uri.UriSchemeNetTcp)) 
                {
                    Fail("Invalid communication protocol.");
                }

                if (hostUri.Port < 0)
                    Fail("Invalid port.");

                if (string.IsNullOrEmpty(userArg))
                    Fail("Username/pass not specified");

                credential = userArg.Split(':');

                if (credential.Length != 2)
                    Fail("Invalid username/pass");

                settings = new ConnectionSettings(hostUri.Host, hostUri.Port);
                settings.CredentialType = CredentialType.Username;
                settings.UsernameCredential.Username = credential[0];
                settings.UsernameCredential.Password = credential[1];

                client = new ApplicationManagerClient(settings);
                client.CheckService();
            }
            catch (Exception e)
            {
                Fail("Could not connect to Manager." + e.GetType());
            }
        }
    }
}
