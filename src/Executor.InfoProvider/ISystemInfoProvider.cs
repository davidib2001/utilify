using Utilify.Platform.Contract;

namespace Utilify.Platform.Executor.InfoProvider
{
    public interface ISystemInfoProvider
    {
        SystemInfo GetSystemInfo(string id);

        ProcessorSystemInfo[] GetProcessorInfo();

        DiskSystemInfo[] GetDiskInfo();

        MemorySystemInfo GetMemoryInfo();

        OSInfo GetOSInfo();
    }
}
