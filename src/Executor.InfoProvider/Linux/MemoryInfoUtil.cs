using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Threading;
using Utilify.Platform.Contract;

namespace Utilify.Platform.Executor.InfoProvider.Linux
{
    //todoMono: need to figure out how to get these values from Linux
    internal class MemoryInfoUtil
    {

        public static long GetTotalMemory()
        {
            return 0;
        }

        public static long GetAvailableMemory()
        {
            return 0;
        }

        public static MemorySystemInfo GetMemoryInfo()
        {
            MemorySystemInfo info = new MemorySystemInfo(GetTotalMemory());
            return info;
        }

        public static MemoryPerformanceInfo GetMemoryPerfInfo()
        {
            // Issue 056: May decide to report Current Usage of this process instead of overal system usage
            MemoryPerformanceInfo perfInfo = new MemoryPerformanceInfo(GetAvailableMemory(), GetTotalMemory() - GetAvailableMemory());
            return perfInfo;
        }
    }
}
