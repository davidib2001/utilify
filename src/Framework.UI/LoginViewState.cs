﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilify.Framework.UI
{
    /// <summary>
    /// Specifies the state of the login user interface
    /// </summary>
    public enum LoginViewState
    {
        /// <summary>
        /// The login form is shown.
        /// </summary>
        LoginForm = 0,
        /// <summary>
        /// The user interface changes to show the connection process.
        /// </summary>
        Connecting = 1,
        /// <summary>
        /// The advanced settings interface is shown.
        /// </summary>
        AdvancedSettings = 2
    }
}
