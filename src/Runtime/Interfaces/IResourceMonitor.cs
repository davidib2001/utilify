using Utilify.Platform.Contract;

#if !MONO
using System.ServiceModel;
#endif

namespace Utilify.Platform
{
#if !MONO
    [ServiceContract(Namespace = Namespaces.Manager)]
    [DataContractFormat(Style = OperationFormatStyle.Document)]
    //to use Xml Serializer : [XmlSerializerFormat(Style = OperationFormatStyle.Document, Use = OperationFormatUse.Literal)]
#endif
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1, EmitConformanceClaims = true, Namespace = Namespaces.ManagerCompat)]
    public interface IResourceMonitor : IManagerService
    {

#if !MONO
        [OperationContract(IsOneWay = true)]
        //[FaultContract(typeof(ArgumentFault))]
        //[FaultContract(typeof(ServerFault))]
        //[FaultContract(typeof(AuthorizationFault))]
        //[FaultContract(typeof(AuthenticationFault))]
#endif
        //[WebMethod]
        void Ping(string id);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthorizationFault))]
        [FaultContract(typeof(AuthenticationFault))]
#endif
        //[WebMethod]
        string Register(SystemInfo info);

#if !MONO
        [OperationContract(IsOneWay = true)]
        //[FaultContract(typeof(EntityNotFoundFault))]
        //[FaultContract(typeof(ArgumentFault))]
        //[FaultContract(typeof(ServerFault))]
        //[FaultContract(typeof(AuthorizationFault))]
        //[FaultContract(typeof(AuthenticationFault))]
#endif
        //[WebMethod]
        void UpdatePerformance(PerformanceInfo info);

#if !MONO
        [OperationContract(IsOneWay = true)]
#endif
        //[WebMethod]
        void UpdateLimits(LimitInfo info);

    }
}

