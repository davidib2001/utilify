using Utilify.Platform.Contract;

#if !MONO
using System.ServiceModel;
#endif

namespace Utilify.Platform
{

#if !MONO
    [ServiceContract(Namespace = Namespaces.Manager)]
    [DataContractFormat(Style = OperationFormatStyle.Document)]
    //to use Xml Serializer : [XmlSerializerFormat(Style = OperationFormatStyle.Document, Use = OperationFormatUse.Literal)]
#endif
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1, EmitConformanceClaims = true, Namespace = Namespaces.ManagerCompat)]
    public interface IJobManager : IManagerService
    {
#if !MONO
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthorizationFault))]
        [FaultContract(typeof(AuthenticationFault))]
#endif
        //[WebMethod]
        ApplicationSubmissionInfo GetApplication(string executorId, string applicationId);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthorizationFault))]
        [FaultContract(typeof(AuthenticationFault))]
#endif
        //[WebMethod]
        JobSubmissionInfo[] GetJobs(string executorId);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthorizationFault))]
        [FaultContract(typeof(AuthenticationFault))]
#endif
        //[WebMethod]
        string[] GetCancelledJobs(string executorId);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthorizationFault))]
        [FaultContract(typeof(AuthenticationFault))]
#endif
        //[WebMethod]
        DependencyContent GetDependency(string dependencyId);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthorizationFault))]
        [FaultContract(typeof(AuthenticationFault))]
#endif
        //[WebMethod]
        void SendCompletedJob(JobCompletionInfo info);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(InvalidOperationFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthorizationFault))]
        [FaultContract(typeof(AuthenticationFault))]
#endif
        //[WebMethod]
        void SendResult(ResultContent content);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthorizationFault))]
        [FaultContract(typeof(AuthenticationFault))]
#endif
        //[WebMethod]
        void UpdateCurrentJobs(JobExecutionInfo info);

    }
}

