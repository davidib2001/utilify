
package com.utilify.framework;

import com.utilify.framework.client.Application;

/**
 * The listener interface for receiving job completed events. 
 * The class that is interested in processing a job completed event implements this interface, 
 * and the object created with that class is registered with an {@link Application}, 
 * using it's {@link Application#addJobCompletedListener} method. When the job completed event occurs, 
 * that object's {@link #onJobCompleted} method is invoked. 
 */
public interface JobCompletedListener {
	/**
	 * Invoked when a job is completed. 
	 * @param event - the event object the contains the completed job information.
	 */
	void onJobCompleted(JobCompletedEvent event);
}
