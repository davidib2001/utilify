
package com.utilify.framework;

import com.utilify.framework.client.Application;
import com.utilify.framework.client.Job;

/**
 * Represents a job submitted event that occurs when a job is submitted.
 * A job completed event is raised by the {@link Application} class, to inform the user when a job is 
 * submitted and passes a reference to that job incase the user wants to hold onto it.
 */
public class JobSubmittedEvent {
	private Job job;
	
	/**
	 * Constructs an instance of the <code>JobSubmittedEvent</code> class with the specified job.
	 * @param job - an instance of {@link Job}.
	 */
	public JobSubmittedEvent(Job job){
		this.job = job;
	}
	
	/**
	 * Gets the job associated with this event.
	 * @return an instance of <code>Job</code>.
	 */
	public Job getJob(){
		return job;
	}
}
