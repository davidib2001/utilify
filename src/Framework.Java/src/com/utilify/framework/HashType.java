
package com.utilify.framework;

/**
 * Represents the type of algorithm to use for hashing data. The following hashing algorithms are supported:<br>
 * <li>MD5</li>
 * <li>SHA1</li>
 * <li>SHA256</li>
 * <li>SHA384</li>
 * <li>SHA512</li>
 */
public enum HashType {

	/**
	 * The MD5 hash algorithm
	 */
	MD5("MD5"),
    /**
     * The SHA 1 hash algorithm
     */
    SHA1("SHA-1"),
    /**
     * The SHA 256 hash algorithm
     */
    SHA256("SHA-256"), //default
    /**
     * The SHA 384 hash algorithm
     */
    SHA384("SHA-384"),
    /**
     * The SHA 512 hash algorithm
     */
    SHA512("SHA-512");
    
    private final String value;

    HashType(String v) {
        value = v;
    }

    /**
     * Gets the value of the <code>HashType</code>
     * @return the value of the type (as a String).
     */
    public String value() {
        return value;
    }

    /**
     * Parses the specified value to create an <code>HashType</code> object.
     * @param v - the value from which to create the <code>HashType</code>.
     * @return the <code>HashType</code> object
     * @throws IllegalArgumentException - if the specified value is not one of the acceptable type values.
     */
    public static HashType fromValue(String v) {
        for (HashType c: HashType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
