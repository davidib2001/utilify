
package com.utilify.framework;

import com.utilify.framework.client.Application;

/**
 * Represents an error event that occurs due to an exception raised during the internal processing in the framework.
 * An error event is raised by the {@link Application} class, to inform the user when an exception occurs on a background thread.
 */
public class ErrorEvent {
	
	private Throwable exception;
	
	/**
	 * Constructs an instance of the <code>ErrorEvent</code> class with the specified exception
	 * that caused this error event.
	 * @param exception - the exception that caused this error event.
	 */
	public ErrorEvent(Throwable exception){
		this.exception = exception;
	}
	
	/**
	 * Gets the exception that caused this error event
	 * @return - the exception object.
	 */
	public Throwable getException(){
		return exception;
	}
}
