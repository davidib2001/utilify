
package com.utilify.framework;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for ClientPlatform.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="JobType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DotNet"/>
 *     &lt;enumeration value="Java"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */

/**
 * Represents the originating platform for a job. Currently the platform can be of the following types:<br>
 * <li>DotNet</li><br>
 * Jobs submitted via the .Net platform.
 * <li>Java</li><br>
 * Jobs submitted via the Java platform.
 */
@XmlType(name = "ClientPlatform")
@XmlEnum
public enum ClientPlatform {

    /**
     * Client platform for a Job submitted from .Net
     */
    @XmlEnumValue("DotNet")
    DOT_NET("DotNet"),
    /**
     * Client platform for a Job submitted from Java
     */
    @XmlEnumValue("Java")
    JAVA("Java");
    private final String value;

    ClientPlatform(String v) {
        value = v;
    }

    /**
     * Gets the value of the <code>ClientPlatform</code>
     * @return the value of the type (as a String).
     */
    public String value() {
        return value;
    }

    /**
     * Parses the specified value to create an <code>ClientPlatform</code> object.
     * @param v - the value from which to create the <code>ClientPlatform</code>.
     * @return the <code>ClientPlatform</code> object
     * @throws IllegalArgumentException - if the specified value is not one of the acceptable type values.
     */
    public static ClientPlatform fromValue(String v) {
        for (ClientPlatform c: ClientPlatform.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
