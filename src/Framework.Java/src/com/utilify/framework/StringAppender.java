package com.utilify.framework;

/**
 *
 */
public final class StringAppender {

	private StringBuilder builder;

	/**
	 * 
	 */
	public StringAppender() {
		builder = new StringBuilder();
	}

	/**
	 * Appends the specified value to the existing string.
	 * @param value
	 * @return the StringAppender object
	 */
	public StringAppender append(String value) {
		builder.append(value);
		return this;
	}

	/**
	 * Appends the specified value, followed by a new line character ('\n') to the existing string.
	 * @param value
	 * @return the StringAppender object
	 */
	public StringAppender appendLine(String value) {
		builder.append(value + '\n');
		return this;
	}

	/**
	 * Appends a new line character ('\n') to the existing string.
	 * @return the StringAppender object
	 */
	public StringAppender appendLine() {
		builder.append('\n');
		return this;
	}

	//public StringAppender appendFormat(String format, params object[] args) {
	//	builder.AppendFormat(format, args);
	//  return this;
	//}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return builder.toString();
	}
}
