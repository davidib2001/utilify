
package com.utilify.framework;

/**
 * Thrown when a job could not be submitted to the remote Manager for any reason.
 */
public class JobSubmissionException extends FrameworkException {
	
	private static final long serialVersionUID = 5439430702365222223L;

	/**
	 * Constructs a JobSubmissionException with no detail message.
	 */
	public JobSubmissionException() {
		super();
	}

	/**
	 * Constructs a JobSubmissionException with the specified detail message.
	 * @param message - the detail message.
	 */
	public JobSubmissionException(String message) {
		super(message);
	}

	/**
	 * Constructs a JobSubmissionException with the specified detail message and cause.
	 * @param message - the detail message.
	 * @param cause - the cause of the exception.
	 */
	public JobSubmissionException(String message, Throwable cause) {
		super(message, cause);
	}
}
