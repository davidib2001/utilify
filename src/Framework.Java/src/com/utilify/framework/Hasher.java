
package com.utilify.framework;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

/**
 * The utility class used to create file hashes for versioning.
 * This class is for internal framework use only.
 */
public final class Hasher {
	
	private static Logger logger = Logger.getLogger(Hasher.class.getName());
	
    /**
     * Computes the hash of the specified input string using the specified hash algorithm.
     * @param input - the input string to hash.
     * @param algorithm - the hashing algorithm to use.
     * @return the hash of the input converted to a UTF-16 encoded string.
     * @throws IllegalArgumentException if <code>input</code> or <code>algorithm> is a <code>null</code> reference,.
     * or <code>input</code> is an empty string.
     */
    public static String computeHash(String input, HashType algorithm) {
        if (input == null || input.trim().length()==0)
            throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": input");
        if (algorithm == null)
        	throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": algorithm");

        byte[] inputBytes = null;
		try {
			inputBytes = input.getBytes("UTF-16");
		} catch (UnsupportedEncodingException e) {
			//we know this should never happen since UTF16 is supported
			logger.fine("Error encoding string to UTF16: " + e.toString());
		}
        
        byte[] hash = null;
        MessageDigest hasher = getHasher(algorithm);
        
        hasher.update(inputBytes);
        hash = hasher.digest();

        return getHexString(hash);
    }

	/**
	 * Computes the hash of the specified file using the specified hash algorithm.
	 * @param file - the file to hash.
	 * @param algorithm - the hashing algorithm to use.
	 * @return the hash of the input converted to a UTF-16 encoded string.
	 * @throws IOException if there is an I/O error when reading the file.
	 * @throws IllegalArgumentException if <code>file</code> or <code>algorithm is a <code>null</code> reference.  
	 */
	public static String computeHash(File file, HashType algorithm) throws IOException {
		if (file == null)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": file");
        if (algorithm == null)
        	throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": algorithm");
		
		return computeHash(new FileInputStream(file), algorithm);
	}
	

    /**
     * Computes the hash of the data read from the specified input stream using the specified hash algorithm.
     * @param inputStream - the stream to read the data to hash.
     * @param algorithm - the hashing algorithm to use.
     * @return the hash of the input converted to a UTF-16 encoded string.
     * @throws IOException if there is an I/O error when reading the stream.
     * @throws IllegalArgumentException if <code>inputStream</code> or <code>algorithm is a <code>null</code> reference.
     */
    public static String computeHash(InputStream inputStream, HashType algorithm) throws IOException {
        if (inputStream == null)
            throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": inputStream");
        if (algorithm == null)
        	throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": algorithm");
        
        byte[] hash = null;
        MessageDigest hasher = getHasher(algorithm);
        BufferedInputStream bufferedStream = new BufferedInputStream(inputStream);
        DigestInputStream dis = new DigestInputStream(bufferedStream, hasher);
        dis.on(true);
        byte[] temp = new byte[1024];
        try {
	        while (dis.read(temp) > 0) {
	        	//this automatically updates the digest
	        }
	        hash = hasher.digest();
        } finally {
        	try {
	        	dis.close();
	        	bufferedStream.close();
        	} catch (IOException ix) {
        		logger.fine("Error closing stream: " + ix.toString());
        	}
        }
        
        return getHexString(hash);
    }
    
    private static final byte[] HEX_CHAR_TABLE = {
        (byte)'0', (byte)'1', (byte)'2', (byte)'3',
        (byte)'4', (byte)'5', (byte)'6', (byte)'7',
        (byte)'8', (byte)'9', (byte)'a', (byte)'b',
        (byte)'c', (byte)'d', (byte)'e', (byte)'f'
      };    

    /**
     * Gets a hexadecimal string representation of the given bytes. (lowercase)
     * @param input - the array of bytes to convert to a hex-string.
     * @return a hex representation of the input
     */
    private static String getHexString(byte[] input) {
    	byte[] hex = new byte[2 * input.length];
    	int index = 0;

    	for (byte b : input) {
    		int v = b & 0xFF;
    		hex[index++] = HEX_CHAR_TABLE[v >>> 4];
    		hex[index++] = HEX_CHAR_TABLE[v & 0xF];
    	}

    	String output = null;
    	try {
    		output = new String(hex, "ASCII");
    	} catch (UnsupportedEncodingException e) {
    		// we know this should never happen in reality
    		logger.fine("Error getting hex string from bytes: " + e.toString());
    	}
    	return output;
    }

    private static MessageDigest getHasher(HashType algorithm) {
    	MessageDigest hasher = null;
    	try {
			hasher = MessageDigest.getInstance(algorithm.value());
		} catch (NoSuchAlgorithmException e) {
			//we know that the HashType has only implemented algorithms
			logger.fine("Error getting MessageDigest for algorithm: " + algorithm + 
					"\n" + e.toString());
		}
		return hasher;
    }
}

