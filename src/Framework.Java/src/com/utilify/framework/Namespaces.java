
package com.utilify.framework;

/**
 * Internal utility class to contain XML namespace constants.
 */
public final class Namespaces {
    private static final String baseNs = "http://schemas.utilify.com/2007/09/";

    /**
     * The ManagerCompat namespace.
     */
    public static final String ManagerCompat = baseNs + "ManagerCompat";
    /**
     * The Manager namespace.
     */
    public static final String Manager = baseNs + "Manager";
    /**
     * The Executor namespace.
     */
    public static final String Executor = baseNs + "Executor";
    /**
     * The Client namespace.
     */
    public static final String Client = baseNs + "Client";
    /**
     * The Framework namespace.
     */
    public static final String Framework = baseNs + "Framework";
}
