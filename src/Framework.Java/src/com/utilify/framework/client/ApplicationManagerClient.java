
package com.utilify.framework.client;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;
import com.utilify.framework.ApplicationInitializationException;
import com.utilify.framework.ApplicationNotFoundException;
import com.utilify.framework.ApplicationProperties;
import com.utilify.framework.ApplicationStatus;
import com.utilify.framework.DependencyNotFoundException;
import com.utilify.framework.DependencyScope;
import com.utilify.framework.EntityNotFoundException;
import com.utilify.framework.EntityType;
import com.utilify.framework.ErrorMessage;
import com.utilify.framework.FrameworkException;
import com.utilify.framework.Helper;
import com.utilify.framework.JobNotFoundException;
import com.utilify.framework.JobStatus;
import com.utilify.framework.JobSubmissionException;
import com.utilify.framework.JobType;
import com.utilify.framework.ResultNotFoundException;
import com.utilify.framework.dependency.DependencyResolver;
import com.utilify.framework.manager.ApplicationManager;
import com.utilify.framework.manager.IApplicationManager;

/**
 * The <code>ApplicationManagerClient</code> is the starting point to the light-weight API
 * that provides access to the Utilify distributed execution platform. It wraps the Application Manager 
 * web service and includes methods to create, submit, and monitor applications, and jobs. It is similar to the 
 * {@link Application} class, however, does not automatically monitor the jobs and does not notify
 * the user with status events.
 */
public final class ApplicationManagerClient {
	private Logger logger;
	private IApplicationManager manager;
	
	/**
	 * Constructs a new instance of <code>ApplicationManagerClient</code>.
	 */
	public ApplicationManagerClient(){
		logger = Logger.getLogger(this.getClass().getName());
        ApplicationProperties props = new ApplicationProperties();
        ApplicationManager appmgr = new ApplicationManager(props.getManagerUrl());
        this.manager = appmgr.getApplicationManagerDefaultEndPoint();
	}
	
	/**
	 * Starts a new application with the specified name on the remote Manager.
	 * @param applicationName
	 * 	the name of the submitted application.
	 * @return 
	 * 	the Id of the newly submitted application.
	 * @throws ApplicationInitializationException
	 * 	if the application could not be initialized on the Manager for any reason.
	 * @throws IllegalArgumentException
	 * 	if <code>applicationName</code> is <code>null</code> or empty.
	 */
	public String startApplication(String applicationName) throws ApplicationInitializationException {
		if (applicationName == null || applicationName.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": applicationName");
		
		return startApplication(applicationName, null, QosInfo.getDefault());
	}
	
	/**
	 * Starts a new application with the specified name, and dependencies on the remote Manager.
	 * @param applicationName
	 * 	the name of the submitted application.
	 * @param dependencies
	 * 	a list of {@link DependencyInfo} for the application.
	 * @return
	 * 	the Id of the newly submitted application.
	 * @throws ApplicationInitializationException
	 * 	if the application could not be initialized on the Manager for any reason.
	 * @throws IllegalArgumentException
	 * 	if <code>applicationName</code> is <code>null</code> or empty, or the list of dependencies contains a <code>null</code> reference.
	 */
	public String startApplication(String applicationName, Iterable<? extends DependencyInfo> dependencies)
		throws ApplicationInitializationException{
		if (applicationName == null || applicationName.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": applicationName");
		
		return startApplication(applicationName, dependencies, QosInfo.getDefault());		
	}
	
	/**
	 * Starts a new application with the specified name, dependencies, and QoS (quality of service) parameters on the remote Manager.
	 * @param applicationName
	 * 	the name of the submitted application.
	 * @param dependencies
	 * 	a list of {@link DependencyInfo} for the application.
	 * @param qos
	 * 	the quality of service parameters for the application.
	 * @return 
	 * 	the Id of the newly submitted application.
	 * @throws ApplicationInitializationException
	 * 	if the application could not be initialized on the Manager for any reason.
	 * @throws IllegalArgumentException
	 * 	if <code>applicationName</code> is <code>null</code> or empty, or the list of dependencies contains a <code>null</code> reference.
	 */
	public String startApplication(String applicationName, Iterable<? extends DependencyInfo> dependencies, QosInfo qos)
		throws ApplicationInitializationException {
		
		if (applicationName == null || applicationName.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": applicationName");
		if (qos == null)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": qos");
		
		ArrayList<DependencyInfo> deps = new ArrayList<DependencyInfo>();
		if (dependencies != null) {
        	for (DependencyInfo dep : dependencies) {
                if (dep == null)
                	throw new IllegalArgumentException("Elements of the dependencies list should not be null");
                deps.add(dep);
            }
        }
		ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(applicationName,
                "User", deps.toArray(new DependencyInfo[0]), qos);
		
		String appId = manager.submitApplication(appInfo);
		
		if (appId == null || appId.trim().length() == 0)
            throw new ApplicationInitializationException("Could not initialize application.");
		
		return appId;
	}
	
	/**
	 * Pauses the application with the specified Id.
	 * @param applicationId 
	 * 	the Id of the application to pause.
	 * @throws ApplicationNotFoundException
	 * 	if an application with the specified was not found.
	 * @throws IllegalStateException
	 * 	if the application is not yet initialised, already paused, or already stopped.
	 * @throws IllegalArgumentException
	 * 	if <code>applicationId</code> is <code>null</code> or empty.
	 */
	public void pauseApplication(String applicationId) throws ApplicationNotFoundException, IllegalStateException {
		if (applicationId == null || applicationId.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": applicationId");
		
		try {
			manager.pauseApplication(applicationId);
		} catch (EntityNotFoundException e) {
			//has to be an app-not-found exception
			if (e.getType() == EntityType.APPLICATION)
				throw new ApplicationNotFoundException(e.getMessage(), e.getEntityId());
			
			throw e; //this is a runtime-exception anyway
		}
	}
	
	/**
	 * Resumes the (previously paused) application with the specified Id.
	 * @param applicationId
	 * 	the Id of the application to resume.
	 * @throws ApplicationNotFoundException
	 *	if an application with the specified was not found.
	 * @throws IllegalStateException
	 *	if the application is not paused.
	 * @throws IllegalArgumentException
	 * 	if <code>applicationId</code> is <code>null</code> or empty.
	 */
	public void resumeApplication(String applicationId) throws ApplicationNotFoundException, IllegalStateException {
		if (applicationId == null || applicationId.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": applicationId");
		
		try {
			manager.resumeApplication(applicationId);
		} catch (EntityNotFoundException e) {
			//has to be an app-not-found exception
			if (e.getType() == EntityType.APPLICATION)
				throw new ApplicationNotFoundException(e.getMessage(), e.getEntityId());

			throw e; //this is a runtime-exception anyway
		}
	}
	
	/**
	 * Cancels the application with the specified Id.
	 * @param applicationId
	 *	the Id of the application to abort.
	 * @throws ApplicationNotFoundException
	 *	if an application with the specified was not found.
	 * @throws IllegalStateException
	 *	if the application is not yet initialized or if it is already stopped.
	 * @throws IllegalArgumentException
	 * 	if <code>applicationId</code> is <code>null</code> or empty.
	 */
	public void cancelApplication(String applicationId) throws ApplicationNotFoundException, IllegalStateException {
		if (applicationId == null || applicationId.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": applicationId");
		
		try {
			manager.abortApplication(applicationId);
		} catch (EntityNotFoundException e) {
			//has to be an app-not-found exception
			if (e.getType() == EntityType.APPLICATION)
				throw new ApplicationNotFoundException(e.getMessage(), e.getEntityId());

			throw e; //this is a runtime-exception anyway
		}
	}
	
	/**
	 * Cancels the job with the specified Id.
	 * @param jobId
	 *	the Id of the job to cancel.
	 * @throws JobNotFoundException
	 * 	if a job with the specified Id was not found.
	 * @throws IllegalStateException
	 * 	if the job is already completed or cancelled.
	 * @throws IllegalArgumentException
	 *	if the <code>jobId</code> is null or empty.
	 */
	public void cancelJob(String jobId) throws JobNotFoundException {
		if (jobId == null || jobId.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": jobId");
		
		try {
			manager.abortJob(jobId);
		} catch (EntityNotFoundException e) {
			//has to be an job-not-found exception
			if (e.getType() == EntityType.JOB)
				throw new JobNotFoundException(e.getMessage(), e.getEntityId());
			
			throw e; //this is a runtime-exception anyway
		}
	}
	
	/**
	 * Submits the specified job as part of the application with the specified Id to the remote Manager for execution.
	 * @param applicationId
	 * 	the Id of the application which the job is part of.
	 * @param job
	 * 	the job to submit.
	 * @return
	 * 	the Id of the submitted job.
	 * @throws IOException
	 * 	if there was an I/O error reading the job's dependency files (if any).
	 * @throws FrameworkException 
	 * @throws IllegalArgumentException
	 * 	if <code>job</code> is a <code>null</code> reference, or <br>
	 * 	if <code>applicationId</code> is <code>null</code> or empty.
	 */
	public String submitJob(String applicationId, Executable job) throws IOException, FrameworkException{
		if (applicationId == null || applicationId.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": applicationId");
		if (job == null)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": job");
		
		return submitJob(applicationId, job, true);
	}

	/**
	 * Submits the specified job as part of the application with the specified Id to the remote Manager for execution.
	 * @param applicationId
	 * 	the Id of the application which the job is part of.
	 * @param job
	 * 	the job to submit.
	 * @param autoDetectDependencies
	 * 	a value specifying whether to automatically detect the Java modules the job requires for remote execution. If <code>true</code>, all the required class/jar files
	 * are detected and added to the list of dependencies declared (by implementing the {@link Dependent} interface).    
	 * @return
	 * 	the Id of the submitted job.
	 * @throws IOException
	 * 	if there was an I/O error reading the job's dependency files (if any).
	 * @throws FrameworkException 
	 * @throws IllegalArgumentException
	 * 	if <code>job</code> is a <code>null</code> reference, or <br>
	 * 	if <code>applicationId</code> is <code>null</code> or empty.
	 */
	public String submitJob(String applicationId, Executable job, boolean autoDetectDependencies) throws IOException, FrameworkException {
		if (applicationId == null || applicationId.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": applicationId");
		if (job == null)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": job");
		
		logger.fine("Getting declared dependencies for job...");
		// 1. find dependencies
		List<DependencyInfo> deps = new ArrayList<DependencyInfo>();
		// if the object implements this interface, then depJob won't be null
		if (job instanceof Dependent) {
			Dependent depJob = (Dependent) job;
			DependencyInfo[] jobDeps = depJob.getDependencies();
			if (jobDeps != null && jobDeps.length > 0) {
				for (int i = 0; i < jobDeps.length; i++) {
					deps.add(jobDeps[i]);
				}
			}
		}
		
		logger.fine("Autodetect dependencies turned on = " + autoDetectDependencies);
		// 1.1 If needed: auto-detect and add module dependencies
		if (autoDetectDependencies){
			logger.fine("Auto-detecting dependencies for job...");
			Collection<DependencyInfo> detectedDeps = DependencyResolver.detectDependencies(job.getClass());
			for (DependencyInfo dep : detectedDeps) {
				// if the module is found in the app's common dependencies
				// or
				// user-declared dependencies for this job :ignore it
				// otherwise add it to the job's dependency list
				if (!deps.contains(dep))
					deps.add(dep);
			}
		}

		logger.fine("Getting declared result info. for job...");
		// 2 --- find expected results
		List<ResultInfo> res = new ArrayList<ResultInfo>();
		if (job instanceof Results) {
			Results resJob = (Results) job;
			ResultInfo[] results = resJob.getExpectedResults();
			if (results != null && results.length > 0) {
				for (int i = 0; i < results.length; i++) {
					res.add(results[i]);
				}
			}
		}

		logger.fine("Serializing job...");
		// 3 --- job instance
		byte[] instance = Helper.serialize(job);

		//4 find job type
		JobType type;
		if (job instanceof NativeExecutable)
			type = JobType.NATIVE_MODULE;
		else
			type = JobType.CODE_MODULE;

		// 5 --- create submissioninfo to send this job off remotely
		JobSubmissionInfo jobInfo = new JobSubmissionInfo(applicationId, type, instance, deps, res);
		
		logger.fine("Submitting job to remote manager...");
		ArrayOfJobSubmissionInfo jobArray = new ArrayOfJobSubmissionInfo(new JobSubmissionInfo[] { jobInfo });
		ArrayOfstring jobIdArray = manager.submitJobs(jobArray);
		String jobId = null;
		if (jobIdArray != null){
			List<String> ids=  jobIdArray.getStrings();
			if (ids != null && ids.size() > 0)
				jobId = ids.get(0);
			else
				throw new JobSubmissionException(ErrorMessage.JobSubmissionErrorMessage);
		} else {
			throw new JobSubmissionException(ErrorMessage.JobSubmissionErrorMessage);
		}
		
		return jobId;
	}
	
	/**
	 * Gets a list of unresolved dependencies, for the specified application or job. This is the list of dependencies that the remote Manager could not resolve
	 * - and therefore need to be sent to the Manager from the client for the application / job to be scheduled for execution.
	 * @param applicationOrJobId
	 * 	Id of the application or job
	 * @param scope
	 * 	the scope of the dependency. If an application Id is specified, scope should be {@link DependencyScope#APPLICATION} otherwise it should be {@link DependencyScope#JOB}
	 * @return
	 * 	an array of {@link DependencyInfo} objects that are yet to be resolved on the Manager.
	 * @throws ApplicationNotFoundException
	 * 	if the specified scope was <code>DepdenencyScope.APPLICATION</code> and the application with the specified Id was not found.
	 * @throws JobNotFoundException
	 * 	if the specified scope was <code>DepdenencyScope.JOB</code> and the job with the specified Id was not found.
	 * @throws FrameworkException
	 * 	if there was an internal error getting the unresolved dependencies.
	 */
	public DependencyInfo[] getUnresolvedDependencies(String applicationOrJobId, DependencyScope scope) throws ApplicationNotFoundException, JobNotFoundException, FrameworkException {
		if (applicationOrJobId == null || applicationOrJobId.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": applicationOrJobId");
		if (scope == null)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": scope");
		
		DependencyInfo[] deps = null;
		
		try {
			ArrayOfDependencyInfo infos = 
				manager.getUnresolvedDependencies(new DependencyQueryInfo(applicationOrJobId, scope));
			if (infos == null)
				throw new FrameworkException("Could not get unresolved dependencies: " + ErrorMessage.InvalidServerResponseMessage);
			deps = infos.toArray();
		} catch (EntityNotFoundException e) {
			//has to be an app/job-not-found exception
			if (e.getType() == EntityType.APPLICATION)
				throw new ApplicationNotFoundException(e.getMessage(), e.getEntityId());
			if (e.getType() == EntityType.JOB)
				throw new JobNotFoundException(e.getMessage(), e.getEntityId());
			throw e; //this is a runtime-exception anyway
		}
		
		return deps;
	}
	
	/**
	 * Sends the contents of the specified file for the specified dependency, to the remote Manager.
	 * @param dependencyId
	 * 	the Id of the dependency, for which the contents are sent.
	 * @param file
	 * 	the file from which to read the contents.
	 * @throws IOException
	 * 	if there was an I/O error reading the file.
	 * @throws DependencyNotFoundException
	 * 	if a dependency with the specified Id was not found.
	 * @throws IllegalArgumentException
	 * 	if <code>dependencyId</code> is <code>null</code> or empty, or <code>file</code> is a <code>null</code> reference.
	 */
	public void sendDependency(String dependencyId, File file) throws IOException, DependencyNotFoundException{
		if (dependencyId == null || dependencyId.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": dependencyId");
		if (file == null)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": file");
		if (file.isDirectory())
			throw new IllegalArgumentException("Sending directories is not supported.");
		byte[] content = Helper.readFile(file);
		sendDependency(dependencyId, content);
	}
	
	/**
	 * Sends the specified contents for the specified dependency, to the remote Manager.
	 * @param dependencyId
	 * 	the Id of the dependency, for which the contents are sent.
	 * @param content
	 * 	the content to send.
	 * @throws DependencyNotFoundException
	 * 	if a dependency with the specified Id was not found.
	 * @throws IllegalStateException
	 * 	if the dependency content has already been resolved on (or previously sent to) the Manager.
	 * @throws IllegalArgumentException
	 * 	if <code>dependencyId</code> or <code>content</code>is <code>null</code> or empty.
	 */
	public void sendDependency(String dependencyId, byte[] content) throws DependencyNotFoundException {
		if (dependencyId == null || dependencyId.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": dependencyId");
		if (content == null || content.length == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": content");
		DependencyContent dependency = new DependencyContent(dependencyId, content);
		try {
			manager.sendDependency(dependency);
		} catch (EntityNotFoundException e) {
			//has to be an dep-not-found exception
			if (e.getType() == EntityType.DEPENDENCY)
				throw new DependencyNotFoundException(e.getMessage(), e.getEntityId());
			throw e; //this is a runtime-exception anyway
		}
	}
	
	/**
	 * Gets the status of the specified application.
	 * @param applicationId
	 * 	the Id of the application.
	 * @return
	 * 	the status of the job.
	 * @throws IllegalArgumentException
	 * 	if <code>applicationId</code> is <code>null</code> or empty.
	 * @throws ApplicationNotFoundException
	 * 	if an application with the specified Id was not found.
	 * @throws FrameworkException
	 * 	if there was an internal error getting the application status.
	 */
	public ApplicationStatus getApplicationStatus(String applicationId) throws ApplicationNotFoundException, FrameworkException{
		if (applicationId == null || applicationId.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": applicationId");

		ApplicationStatus status = null;
		try {
			ApplicationStatusInfo info = manager.getApplicationStatus(applicationId);
			if (info != null)
				status = info.getStatus();
			else
				throw new FrameworkException("Could not get application status: " + ErrorMessage.InvalidServerResponseMessage);
		} catch (EntityNotFoundException e) {
			//has to be an app-not-found exception
			if (e.getType() == EntityType.APPLICATION)
				throw new ApplicationNotFoundException(e.getMessage(), e.getEntityId());
			throw e; //this is a runtime-exception anyway
		}
		
		return status;
	}
	
	/**
	 * Gets the status of the specified job.
	 * @param jobId
	 * 	the Id of the job.
	 * @return
	 *	the status of the job.
	 * @throws IllegalArgumentException
	 * 	if <code>jobId</code> is <code>null</code> or empty.
	 * @throws FrameworkException
	 * 	if there was an internal error getting the job status.
	 */
	public JobStatus getJobStatus(String jobId) throws FrameworkException{
		if (jobId == null || jobId.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": jobId");

		ArrayList<String> jobIds = new ArrayList<String>();
		jobIds.add(jobId);
		List<JobStatus> statuses = getJobStatus(jobIds);
		
		if (statuses == null || statuses.size() != 1 || statuses.get(0) == null)
			throw new FrameworkException("Could not get job status: " + ErrorMessage.InvalidServerResponseMessage);
		
		return statuses.get(0);
	}
	
	/**
	 * Gets the status of the jobs with the specified Ids.
	 * @param jobIds
	 * 	the list of job Ids for which the status is required.
	 * @return
	 * 	list of job status objects representing the status of the jobs.
	 * @throws IllegalArgumentException
	 * 	if <code>jobIds</code> is <code>null</code> or one of its elements is <code>null</code> or empty.
	 * @throws FrameworkException
	 * 	if there was an internal error getting the status.
	 */
	public List<JobStatus> getJobStatus(Iterable<String> jobIds) throws FrameworkException{
		if (jobIds == null)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": jobIds");
		
		ArrayOfstring idArray = new ArrayOfstring();		
		for (String jobId : jobIds){
			if (jobId == null || jobId.trim().length() == 0)
				throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": element of jobIds");
			idArray.getStrings().add(jobId);
		}
		
		if (idArray.getStrings().size() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": jobIds");
		
		ArrayList<JobStatus> statuses = null;
		
		ArrayOfJobStatusInfo statusInfoArray = manager.getJobStatus(idArray);
		if (statusInfoArray != null){
			JobStatusInfo[] infos = statusInfoArray.toArray();
			if (infos == null || infos.length == 0)
				throw new FrameworkException("Could not get job status: " + ErrorMessage.InvalidServerResponseMessage);
			statuses = new ArrayList<JobStatus>();
			for (JobStatusInfo info : infos) {
				if (info == null || info.getStatus() == null)
					throw new FrameworkException("Could not get job status: " + ErrorMessage.InvalidServerResponseMessage);
				statuses.add(info.getStatus());
			}
		} else {
			throw new FrameworkException("Could not get job status: " + ErrorMessage.InvalidServerResponseMessage);
		}
		return statuses;
	}
	
	/**
	 * Gets the completed job instance from the Manager.
	 * If the job has not finished execution, it returns <code>null</code>.
	 * @param jobId
	 * 	the Id of the job to get.
	 * @return
	 * 	the instance of the completed job.
	 * @throws IOException
	 * 	if there was an error re-constructing the job instance from the information returned by the Manager. 
	 * @throws JobNotFoundException
	 * 	if a job with the specified Id was not found.
	 * @throws FrameworkException
	 * 	if there was an internal error getting the job instance.
	 * @throws IllegalArgumentException
	 * 	if <code>jobId</code> is <code>null</code> or empty.
	 */
	public Executable getCompletedJob(String jobId) throws IOException, JobNotFoundException, FrameworkException {
		if (jobId == null || jobId.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": jobId");

		Executable job = null;
		
		try {
			JobCompletionInfo jobInfo = manager.getCompletedJob(jobId);
			//jobInfo can be null and that is NOT an error : it is possible when the job is not yet done.
			if (jobInfo != null)
				job = (Executable)Helper.deserialize(jobInfo.getJobInstance());
			
		} catch (EntityNotFoundException e) {
			//has to be an job-not-found exception
			if (e.getType() == EntityType.JOB)
				throw new JobNotFoundException(e.getMessage(), e.getEntityId());

			throw e; //this is a runtime-exception anyway
		}
		return job;
	}
	
	/**
	 * Gets the result information for the specified jobs. If there were no results declared during
	 * job submission, this method returns an empty array.
	 * @param jobId
	 * 	the Id of the job to for which the result information is retrieved.
	 * @return
	 * 	an array of {@link ResultStatusInfo} containing information about the available results for the job.
	 * @throws IllegalArgumentException
	 * 	if <code>jobId</code> is <code>null</code> or empty.
	 * @throws JobNotFoundException
	 * 	if a job with the specified Id was not found.
	 * @throws FrameworkException
	 * 	if there was an internal error getting the result information.
	 */
	public ResultStatusInfo[] getJobResults(String jobId) throws JobNotFoundException, FrameworkException {
		if (jobId == null || jobId.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": jobId");
		
		ResultStatusInfo[] statuses = null;
		try {
			ArrayOfResultStatusInfo resStatusArray = manager.getJobResults(jobId);
			if (resStatusArray == null)				
				throw new FrameworkException("Could not get job result status: " + ErrorMessage.InvalidServerResponseMessage);
			statuses = resStatusArray.toArray();
			if (statuses == null || statuses.length == 0)				
				throw new FrameworkException("Could not get job result status: " + ErrorMessage.InvalidServerResponseMessage);
			
		} catch (EntityNotFoundException e) {
			//has to be an job-not-found exception
			if (e.getType() == EntityType.JOB)
				throw new JobNotFoundException(e.getMessage(), e.getEntityId());

			throw e; //this is a runtime-exception anyway
		}
		return statuses;
	}
	
	/**
	 * Gets the contents of the specified result.
	 * @param resultId
	 * 	the Id of the result for which contents are retrieved.
	 * @return
	 * 	the binary data of the result.
	 * @throws ResultNotFoundException
	 * 	if the result with the specified Id was not found.
	 * @throws FrameworkException
	 * 	if there was an internal error getting the result.
	 */
	public byte[] getResult(String resultId) throws ResultNotFoundException, FrameworkException {
		if (resultId == null || resultId.trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": resultId");
		
		ResultContent content = null;
		try {
			content = manager.getResult(resultId);
			if (content == null)
				throw new FrameworkException("Could not get result: " + ErrorMessage.InvalidServerResponseMessage);
		} catch (EntityNotFoundException e) {
			//has to be an job-not-found exception
			if (e.getType() == EntityType.RESULT)
				throw new ResultNotFoundException(e.getMessage(), e.getEntityId());
			
			throw e; //this is a runtime-exception anyway
		}
		return content.getContent();
	}
}
