
package com.utilify.framework.client;

/**
 * Thrown when a method is called on a disposed {@link Disposable} object.
 */
public class ObjectDisposedException extends RuntimeException {

	private static final long serialVersionUID = 7419355365966822912L;
	
	/**
	 * Creates an instance of the <code>ObjectDisposedException</code> class with the specified message.
	 * @param message
	 * 	the detail message.
	 */
	public ObjectDisposedException(String message){
		super(message);
	}
	/**
	 * Creates an instance of the <code>ObjectDisposedException</code> class with the specified message 
	 * and a reference to the cause of this exception.
	 * @param message
	 * 	the detail message.
	 * @param cause
	 * 	the cause of this exception.
	 */
	public ObjectDisposedException(String message, Throwable cause){
		super(message, cause);
	}
}
