
package com.utilify.framework.client;

/**
 * The interface to implement to declare expected results for a job. The class that implements this interface (which should be the one that
 * implements the {@link Executable} interface that represents an executable job),
 * declares that it produces certain files and/or resources as a result of execution. 
 * The list of these expected results is retrieved 
 * by the framework during job submission, using the {@link #getExpectedResults()} method.
 */
public interface Results {
	/**
	 * Gets the list of results expected to be produced by this job.
	 * @return
	 * 	the list of results.
	 */
	ResultInfo[] getExpectedResults();
}
