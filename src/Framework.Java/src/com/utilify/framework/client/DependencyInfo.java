
package com.utilify.framework.client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.DependencyType;
import com.utilify.framework.ErrorMessage;
import com.utilify.framework.HashType;
import com.utilify.framework.Hasher;
import com.utilify.framework.Helper;
import com.utilify.framework.dependency.DependencyResolver;

/*
 * <p>Java class for DependencyInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DependencyInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Filename" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Hash" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Modified" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Size" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="Type" type="{http://schemas.utilify.com/2007/09/Framework}DependencyType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents information about a dependency. Dependencies are files or resources used in the execution of a job.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DependencyInfo", propOrder = {
    "filename",
    "hash",
    "id",
    "modified",
    "name",
    "size",
    "type"
})
public class DependencyInfo implements Serializable {

    @XmlElement(name = "Filename", required = true, nillable = true)
    private String filename;
    @XmlElement(name = "Hash", required = true, nillable = true)
    private String hash;
    @XmlElement(name = "Id", nillable = true)
    private String id;
    @XmlElement(name = "Modified", required = true)
    @XmlSchemaType(name = "dateTime")
    private Date modified;
    @XmlElement(name = "Name", required = true, nillable = true)
    private String name;
    @XmlElement(name = "Size")
    private long size;
    @XmlElement(name = "Type", required = true)
    private DependencyType type;
    
    @XmlTransient
    private String localPath = "";
    /*
     * Gets the local path of the dependency file. This property is for internal use only. (It is used only 
     * on the client side) 
     */
    String getLocalPath() {
        return localPath; 
    }

    private DependencyInfo(){}
    
    /**
     * Creates an instance of the <code>DependencyInfo</code> class with the specified name, filename and type.
     * @param name
     * 	the name of the dependency.
     * @param fileNameOrUrl
     * 	the filename or url of the dependency. 
     * @param type
     * 	the type of dependency.
     * @throws IllegalArgumentException
     * 	if <code>name</code> or <code>fileNameOrUrl</code> is <code>null</code> or empty, or <br>
     * <code>type</code> is <code>null</code>.
     * @throws IOException
     * 	if there was an I/O error reading the dependency file.
     * @throws MalformedURLException
     * 	if the specified type is {@link DependencyType#REMOTE_URL}, and the <code>fileNameOrUrl</code> is a badly formatted url. 
     */
    public DependencyInfo(String name, String fileNameOrUrl, DependencyType type) throws IOException, MalformedURLException {
    	setName(name);
		setType(type);
		
    	if (fileNameOrUrl == null || fileNameOrUrl.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": fileNameOrUrl");

		if (type != DependencyType.REMOTE_URL){
			//it is a local file
			File f = new File(fileNameOrUrl);
			if (!f.exists())
				throw new FileNotFoundException("Cannot find file " + fileNameOrUrl);
			
			if (f.isDirectory())
				f = Helper.zipDirectory(f);
			
			setFilename(f.getName()); //we need only the name : don't expose local path to the remote service
			setModified(new Date(f.lastModified()));
			setSize(f.length());
			
			this.localPath = f.getCanonicalPath();
			
			//calculate hash and set it  
			setHash(Hasher.computeHash(f, HashType.SHA256));
		} else {
			//remote url : can't set modified / size
			new URL(fileNameOrUrl);
			setFilename(fileNameOrUrl);
			setSize(0);
			setModified(Helper.getMinUtcDate());
			//even for remote urls we calculate hash and set it - to avoid argument null exceptions from the server.  
			setHash(Hasher.computeHash(fileNameOrUrl, HashType.SHA256));
		}
	}
    
    /**
     * Gets the filename of this dependency.
     * 
     * @return
     *  the name or url of the dependency file.   
     */
    public String getFilename() {
        return filename;
    }

    private void setFilename(String value) {
    	if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": fileName");
        this.filename = value;
    }

    /**
     * Gets the hash of the dependency file contents.
     * 
     * @return
     * 	the hash of the dependency file contents.   
     */
    public String getHash() {
        return hash;
    }

    private void setHash(String value) {
    	if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": hash");
        this.hash = value;
    }

    /**
     * Gets the Id of this dependency.
     * 
     * @return
     * 	the Id of this dependency.   
     */
    public String getId() {
        return id;
    }


    @SuppressWarnings("unused") //set by JAXB
	private void setId(String value) {
    	if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": id");
        this.id = value;
    }

    /**
     * Gets the last modified time of this dependency..
     * 
     * @return
     * 	the last modified time of the dependency file.    
     */
    public Date getModified() {
        return modified;
    }

    private void setModified(Date value) {
    	if (value == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": modified");
        this.modified = value;
    }

    /**
     * Gets the name of this dependency.
     * @return
     *	the name of the dependency.
     */
    public String getName() {
        return name;
    }

    private void setName(String value) {
    	if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": name");
        this.name = value;
    }

    /**
     * Gets the size of this dependency.
     * @return
     * 	the size (in bytes) of the dependency file.
     */
    public long getSize() {
        return size;
    }

    private void setSize(long value) {
    	if (value < 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeLessThan + " zero : name");
        this.size = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link DependencyType }
     *     
     */
    public DependencyType getType() {
        return type;
    }

    private void setType(DependencyType value) {
    	if (value == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": type");
        this.type = value;
    }

	/**
	 * Returns an instance of <code>DependencyInfo</code> constructed from the .class or .jar file 
	 * for the specified class. This instance can then be used during application / job submission, to declare 
	 * that the application of job depends on the specified Java class.
	 * @param aClass
	 * 	the class from which to create the <code>DependencyInfo</code>.
	 * @return
	 * 	an instance of <code>DependencyInfo</code>.
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public static DependencyInfo fromClass(Class<? extends Object> aClass) throws IOException, MalformedURLException {
		if (aClass == null)
			throw new IllegalArgumentException("Class cannot be null");
		String location = DependencyResolver.getLocation(aClass);
		DependencyInfo info = new DependencyInfo(aClass.getSimpleName() + "_Class",
				location, DependencyType.JAVA_MODULE);
		return info;
	}

    /**
     * Gets a string representation of this object.
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        return String.format("DependencyInfo : %1$s, %2$s, type: %3$s, modified: %4$s, hash %5$s",
            name, filename, type, modified, hash);
    }
    
    /**
     * @param other
     * @return true, if the specified object is equal to this object, false otherwise
     * @see java.lang.Object#equals(Object)
     */
    public boolean Equals(final Object other)
    {
        if (other == null)
            return false;

        if (other.getClass() != this.getClass())
            return false;

        boolean areEqual = false;
        // 
        final DependencyInfo otherInfo = (DependencyInfo) other;
        areEqual = (otherInfo.getName() == this.getName()) &&
            (otherInfo.getType() == this.getType()) &&
            (otherInfo.getFilename() == this.getFilename()) &&
            (otherInfo.getModified() == this.getModified()) &&
            (otherInfo.getSize() == this.getSize()) && 
            (otherInfo.getHash() == this.getHash());
        return areEqual;
    }    
}
