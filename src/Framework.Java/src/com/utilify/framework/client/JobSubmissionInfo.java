
package com.utilify.framework.client;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.ClientPlatform;
import com.utilify.framework.ErrorMessage;
import com.utilify.framework.JobType;

/*
 * <p>Java class for JobSubmissionInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="JobSubmissionInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClientPlatform" type="{http://schemas.utilify.com/2007/09/Framework}ClientPlatform"/>
 *         &lt;element name="Dependencies" type="{http://schemas.utilify.com/2007/09/Client}ArrayOfDependencyInfo"/>
 *         &lt;element name="ExpectedResults" type="{http://schemas.utilify.com/2007/09/Client}ArrayOfResultInfo"/>
 *         &lt;element name="JobId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JobInstance" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="JobType" type="{http://schemas.utilify.com/2007/09/Framework}JobType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents the information needed to submit a job to the remote Manager.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JobSubmissionInfo", propOrder = {
    "applicationId",
    "clientPlatform",
    "dependencies",
    "expectedResults",
    "jobId",
    "jobInstance",
    "jobType"
})
public class JobSubmissionInfo {

    @XmlElement(name = "ApplicationId", required = true, nillable = true)
    private String applicationId;
    @XmlElement(name = "ClientPlatform", required = true)
    private ClientPlatform clientPlatform;
    @XmlElement(name = "Dependencies", required = true, nillable = true)
    private ArrayOfDependencyInfo dependencies;
    @XmlElement(name = "ExpectedResults", required = true, nillable = true)
    private ArrayOfResultInfo expectedResults;
    @XmlElement(name = "JobId", nillable = true)
    private String jobId;
    @XmlElement(name = "JobInstance", required = true, nillable = true)
    private byte[] jobInstance;
    @XmlElement(name = "JobType", required = true)
    private JobType jobType;
    
    private JobSubmissionInfo() { }
    
    /**
     * Creates an instance of the <code>JobSubmissionInfo</code> class with the specified application Id,
     * type and instance.
     * The job is submitted with an empty list of declared dependencies and results.
     * @param applicationId
     * 	the Id of the application the job is a part of.
     * @param type
     * 	the type of this job.
     * @param instance
     * 	the serialized instance of the job.
     * @throws IllegalArgumentException
     * 	if <code>applicationId</code> or <code>instance</code> is <code>null</code> or empty, 
     * or <code>type</code> is <code>null</code>.
     */
    public JobSubmissionInfo(String applicationId, JobType type, byte[] instance) {
    	this(applicationId, type, instance, null, null);
	}
    
    /**
     * Creates an instance of the <code>JobSubmissionInfo</code> class with the specified application Id,
     * type, instance and dependencies.
     * The job is submitted with an empty list of declared results.
     * @param applicationId
     * 	the Id of the application the job is a part of.
     * @param type
     * 	the type of this job.
     * @param instance
     * 	the serialized instance of the job.
     * @param deps
     * 	the list of dependencies of the job.
     * @throws IllegalArgumentException
     * 	if <code>applicationId</code> or <code>instance</code> is <code>null</code> or empty, 
     * or <code>type</code> is <code>null</code>.
     */
    public JobSubmissionInfo(String applicationId, JobType type, byte[] instance, 
    		Collection<DependencyInfo> deps) {
    	this(applicationId, type, instance, deps, null);
	}
   
    /**
     * Creates an instance of the <code>JobSubmissionInfo</code> class with the specified application Id,
     * type, instance, dependencies and results.
     * @param applicationId
     * 	the Id of the application the job is a part of.
     * @param type
     * 	the type of this job.
     * @param instance
     * 	the serialized instance of the job.
     * @param deps
     * 	the list of dependencies of the job.
     * @param res
     *	the list of results expected to be produced by the job.
     * @throws IllegalArgumentException
     * 	if <code>applicationId</code> or <code>instance</code> is <code>null</code> or empty, 
     * or <code>type</code> is <code>null</code>.
     */
    public JobSubmissionInfo(String applicationId, JobType type, byte[] instance, 
    		Collection<DependencyInfo> deps, Collection<ResultInfo> res) {
		setApplicationId(applicationId);
		setJobType(type);
		setClientPlatform(ClientPlatform.JAVA); // Jobs submitted using this client should ALWAYS send JAVA.
		setJobInstance(instance);
		setDependencies(deps);
		setExpectedResults(res);
	}

	/**
     * Gets the  application Id for this job.
     * @return
     * 	the Id of the application the job is part of.
     */
    public String getApplicationId() {
        return applicationId;
    }

    private void setApplicationId(String value) {
        if (value == null || value.trim().length() == 0)
        	throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": applicationId");
    	this.applicationId = value;
    }

    /**
     * Gets the dependencies for the job.
     * If there are no dependencies, an empty array is returned.
     * @return
     * 	the list of dependencies for this job.
     */
    public DependencyInfo[] getDependencies() {
    	if (dependencies == null)
    		return new DependencyInfo[0];

    	return dependencies.toArray();
    }

    private void setDependencies(Collection<DependencyInfo> value) {
    	if (value == null)
    		this.dependencies = new ArrayOfDependencyInfo();
    	else
    		this.dependencies = new ArrayOfDependencyInfo(value);
    }

    /**
     * Gets the list of results expected to be produced by execution of this job.
     * If there are no results expected, an empty array is returned.
     * @return
     * 	the list of expected results for this job.  
     */
    public ResultInfo[] getExpectedResults() {
    	if (expectedResults == null)
    		return new ResultInfo[0];

    	return expectedResults.toArray();
    }

    private void setExpectedResults(Collection<ResultInfo> value) {
    	if (value == null)
    		this.expectedResults = new ArrayOfResultInfo();
    	else
    		this.expectedResults = new ArrayOfResultInfo(value);
    }

    /**
     * Gets the job Id.
     * @return
     * 	the Id of this job.    
     */
    public String getJobId() {
        return jobId;
    }

    @SuppressWarnings("unused") //set by JAXWS
	private void setJobId(String value) {
        if (value == null || value.trim().length() == 0)
        	throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": jobId");
        this.jobId = value;
    }

    /**
     * Gets the job instance.
     * @return
     *	the byte[] representing the serialized job instance.
     */
    public byte[] getJobInstance() {
        return jobInstance;
    }

    private void setJobInstance(byte[] value) {
        if (value == null || value.length == 0)
        	throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": jobInstance");
        this.jobInstance = value;
    }

    /**
     * Gets the job type.
     * @return
     * 	the type of this job.
     */
    public JobType getJobType() {
        return jobType;
    }

    private void setJobType(JobType value) {
        if (value == null )
        	throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": jobType");
        this.jobType = value;
    }
    
    /**
     * Gets the client platform.
     * @return
     * 	the platform from which this job was sent.
     */
    public ClientPlatform getClientPlatform() {
        return this.clientPlatform;
    }

    private void setClientPlatform(ClientPlatform value) {
        if (value == null)
        	throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": clientPlatform");
        this.clientPlatform = value;
    }
}
