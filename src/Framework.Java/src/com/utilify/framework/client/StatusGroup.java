package com.utilify.framework.client;

public enum StatusGroup {
	NEW,
	ACTIVE,
	INACTIVE;
}
