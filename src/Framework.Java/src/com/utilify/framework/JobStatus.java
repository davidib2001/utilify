
package com.utilify.framework;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for JobStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="JobStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UnInitialized"/>
 *     &lt;enumeration value="Submitted"/>
 *     &lt;enumeration value="Ready"/>
 *     &lt;enumeration value="Scheduled"/>
 *     &lt;enumeration value="Executing"/>
 *     &lt;enumeration value="Completed"/>
 *     &lt;enumeration value="Cancelled"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */

/**
 * Represents the status of job. At any time, a job can have one of the following 
 * statuses: <br>
 * <li>UnInitialized</li><br>
 * The job is yet to be initialized on the remote Manager. 
 * <li>Submitted</li><br>
 * The job is submitted to the Manager.
 * <li>Ready</li><br>
 * The job is ready to run. This means that the job's dependencies have been resolved and any Qos constraints on the parent application
 * have been met. Only jobs that belong to a ready application are eligible to be scheduled to run on an Executor.
 * <li>Scheduled</li><br>
 * The job is scheduled to be run on an Executor.
 * <li>Executing</li><br>
 * The job is running on an Executor.
 * <li>Completed</li><br>
 * The job execution is complete. This does not specify whether the execution was successful - 
 * only that it is complete and there is nothing more to do.
 * <li>Cancelled</li><br>
 * The job was cancelled (generally in response to a user initiated abort method call).
 */
@XmlType(name = "JobStatus")
@XmlEnum
public enum JobStatus {

    /**
     * Status for an un-initialized (i.e newly created) job.
     */
    @XmlEnumValue("UnInitialized")
    UN_INITIALIZED("UnInitialized"),
    /**
     * Status for a submitted job.
     */
    @XmlEnumValue("Submitted")
    SUBMITTED("Submitted"),
    /**
     * Status for a ready job.
     */
    @XmlEnumValue("Ready")
    READY("Ready"),
    /**
     * Status for a scheduled job.
     */
    @XmlEnumValue("Scheduled")
    SCHEDULED("Scheduled"),
    /**
     * Status for an executing job.
     */
    @XmlEnumValue("Executing")
    EXECUTING("Executing"),
    /**
     * Status for a completing job.
     */
    @XmlEnumValue("Completing")
    COMPLETING("Completing"),
    /**
     * Status for a completed job.
     */
    @XmlEnumValue("Completed")
    COMPLETED("Completed"),
    /**
     * Status for a cancelled job.
     */
    @XmlEnumValue("Cancelled")
    CANCELLED("Cancelled");
    private final String value;

    JobStatus(String v) {
        value = v;
    }

    /**
     * Gets the value of the job status.
     * @return the job status value as a String.
     */
    public String value() {
        return value;
    }

    /**
     * Parses the specified value to create an <code>JobStatus</code> object.
     * @param v - the value from which to create the <code>JobStatus</code>.
     * @return the <code>JobStatus</code> object
     * @throws IllegalArgumentException - if the specified value is not one of the acceptable status values.
     */
    public static JobStatus fromValue(String v) {
        for (JobStatus c: JobStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
