
package com.utilify.framework.manager;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.client.DependencyContent;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dependency" type="{http://schemas.utilify.com/2007/09/Client}DependencyContent" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dependency"
})
@XmlRootElement(name = "SendDependency")
public class SendDependency {

    @XmlElement(nillable = true)
    protected DependencyContent dependency;

    /**
     * Gets the value of the dependency property.
     * 
     * @return
     *     possible object is
     *     {@link DependencyContent }
     *     
     */
    public DependencyContent getDependency() {
        return dependency;
    }

    /**
     * Sets the value of the dependency property.
     * 
     * @param value
     *     allowed object is
     *     {@link DependencyContent }
     *     
     */
    public void setDependency(DependencyContent value) {
        this.dependency = value;
    }

}
