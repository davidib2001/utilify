
package com.utilify.framework.manager;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.client.ResultContent;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetResultResult" type="{http://schemas.utilify.com/2007/09/Client}ResultContent" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getResultResult"
})
@XmlRootElement(name = "GetResultResponse")
public class GetResultResponse {

    @XmlElement(name = "GetResultResult", nillable = true)
    protected ResultContent getResultResult;

    /**
     * Gets the value of the getResultResult property.
     * 
     * @return
     *     possible object is
     *     {@link ResultContent }
     *     
     */
    public ResultContent getGetResultResult() {
        return getResultResult;
    }

    /**
     * Sets the value of the getResultResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultContent }
     *     
     */
    public void setGetResultResult(ResultContent value) {
        this.getResultResult = value;
    }

}
