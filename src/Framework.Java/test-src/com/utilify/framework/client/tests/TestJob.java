package com.utilify.framework.client.tests;

import java.io.Serializable;
import java.util.Date;

import com.utilify.framework.ExecutionContext;
import com.utilify.framework.client.Executable;

public class TestJob implements Executable, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4037577684811628846L;
	private int i;
	private java.util.Date when;

	public void execute(ExecutionContext context) {
		i = 42;
		when = new Date();
	}

	public int getI() {
		return i;
	}

	public java.util.Date getWhen() {
		return when;
	}
}