package com.utilify.framework.client.remote.tests;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.utilify.framework.ApplicationInitializationException;
import com.utilify.framework.ApplicationStatus;
import com.utilify.framework.JobStatus;
import com.utilify.framework.client.Application;
import com.utilify.framework.client.DependencyInfo;
import com.utilify.framework.client.ObjectDisposedException;
import com.utilify.framework.client.Job;
import com.utilify.framework.client.tests.TestJob;

	/*
	 * [Category("Manager-Client Integration Tests")]
	 */

public class ApplicationTest {
	
	/*
        //[ExpectedException(typeof(InvalidOperationException))] //todoDiscuss: what is the expected behaviour here?
        public void CancelJobNotStarted()
        {
            Application app = new Application();
            TestJob job = new TestJob();
            using (app as IDisposable)
            {
                app.AddJob(job);
                app.CancelJob(job);
            }
        } ]
	*/

	//Make sure server is running before this test runs

	@Test(expected = IllegalArgumentException.class)
	public void createApplicationNullNameCtor2() {
		new Application(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createApplicationNullNameCtor3() throws IOException, MalformedURLException {
		List<DependencyInfo> deps = new ArrayList<DependencyInfo>();
		deps.add(DependencyInfo.fromClass(this.getClass()));
		new Application(null, deps);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createApplicationEmptyNameCtor2() {
		new Application(" ");
	}

	@Test(expected = IllegalArgumentException.class)
	public void createApplicationEmptyNameCtor3() throws IOException, MalformedURLException {
		List<DependencyInfo> deps = new ArrayList<DependencyInfo>();
		deps.add(DependencyInfo.fromClass(this.getClass()));
		new Application(" ", deps);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createApplicationNullDepElementsCtor3() throws IOException, MalformedURLException {
		List<DependencyInfo> deps = new ArrayList<DependencyInfo>();
		deps.add(DependencyInfo.fromClass(this.getClass()));
		deps.add(null);
		new Application("me", deps);
	}

	@Test
	public void createApplicationCtor1() {
		Application app = new Application();
		Assert.assertEquals("The initial status of a newly created app should be 'uninitialized'",
				ApplicationStatus.UNINITIALIZED, app.getStatus());
		//Assert.assertNotNull("GetDependencies should not return null, even if we don't set any dependencies",
				//app.getDependencies());
	}

	@Test
	public void createApplicationCtor2() {
		Application app = new Application("me");
		Assert.assertEquals("The initial status of a newly created app should be 'uninitialized'",
				ApplicationStatus.UNINITIALIZED, app.getStatus());
		//Assert.assertNotNull("GetDependencies should not return null, even if we don't set any dependencies",
				//app.getDependencies());
	}

	@Test
	public void createApplicationCtor3() throws IOException, MalformedURLException {
		List<DependencyInfo> deps = new ArrayList<DependencyInfo>();
		deps.add(DependencyInfo.fromClass(this.getClass()));
		Application app = new Application("me", deps);
		Assert.assertEquals("The initial status of a newly created app should be 'uninitialized'",
				ApplicationStatus.UNINITIALIZED, app.getStatus());
		//Assert.assertNotNull("GetDependencies should not return null, even if we don't set any dependencies",
				//app.getDependencies());
	}

	@Test(expected = IllegalArgumentException.class)
	public void addJobNull() {
		Application app = new Application();
		try{
			app.addJob(null);
		} finally {
			app.dispose();
		}
	}
	
	@Test
	public void addJob() {
		Application app = new Application();
		// a 'using' block: start block 
		TestJob job = new TestJob();
		try{
			app.addJob(job);
			//JobStatus status = app.GetJobStatus(job)
			// todo: finish application tests after impl
			// Assert.AreEqual(JobStatus.UnInitialized, status, "Initial status of a job should be 'uninitialized'.")
		} finally {
			if (app != null)
				app.dispose();
		}
	}

	@Test (expected = ObjectDisposedException.class)
	public void addJobToDisposedApplication() {
		Application app = new Application();
		app.dispose();
		app.addJob(new TestJob());
	}

	@Test (expected = IllegalStateException.class)
	public void cancelNewApplication() {
		Application app = new Application();
		try{
			app.cancel();
		} finally {
			app.dispose();
		} 
	}

	@Test
	public void cancelSubmittedApplication() throws ApplicationInitializationException {
		Application app = new Application();
		try {
			app.start();
			app.cancel();
			Assert.assertEquals("The status of a cancelled application should be 'Stopped'",
					ApplicationStatus.STOPPED, app.getStatus());
		} finally {
			app.dispose();
		}
	}

	@Test (expected = IllegalStateException.class)
	public void cancelApplicationTwice() throws ApplicationInitializationException {
		Application app = new Application();
		try { 
			app.start();
			app.cancel();
			Assert.assertEquals("The status of a cancelled application should be 'Stopped'",
					ApplicationStatus.STOPPED, app.getStatus());
			app.cancel();
		} finally {
			app.dispose();
		} 
	}

	@Test (expected = ObjectDisposedException.class)
	public void cancelDisposedApplication() throws ApplicationInitializationException {
		Application app = new Application();
		try {
			app.start();
			app.cancel();
			app.dispose();
		} finally {
			app.dispose();
		}
		Assert.assertEquals("The status of a cancelled application should be 'Stopped'",
				ApplicationStatus.STOPPED, app.getStatus());
		//by now the app is disposed
		app.cancel();
	}

	/**
	*  Translator: Was marked: [Test]
	*  Translator: Was marked: [ExpectedException(typeof(ObjectDisposedException))]
	*          #region CancelJob          [Test]         [ExpectedException(typeof(ArgumentNullException))]         public void CancelJobNull()         {             Application app = new Application();             using (app as IDisposable)             {                 app.CancelJob(null);             }         }
	**/
	
	@Test (expected = ObjectDisposedException.class)
	public void cancelJobOnDisposedApplication() {
		Application app = new Application();
		Job rj = null;
		try {
			app.addJob(new TestJob());
		} finally {
			app.dispose();
		}
		app.cancelJob(rj);
	}

	@Test
	public void cancelJob() throws ApplicationInitializationException, InterruptedException {
		Application app = new Application();
		Job rj = null;
		try {
			rj = app.addJob(new TestJob());
			app.start();
			Thread.sleep(5000); // wait to have the job submitted remotely
			app.cancelJob(rj);
			Thread.sleep(5000); // wait to get the latest status from the remote side again
									
			JobStatus s = rj.getStatus();
			boolean correctStatus = (s == JobStatus.COMPLETED || s == JobStatus.CANCELLED);
			Assert.assertTrue("The status of a cancelled job should be 'completed' or 'cancelled' but was "
					+ s.toString(),
					correctStatus);
		} finally {
			app.dispose();
		}		
	}

	@Test (expected = IllegalStateException.class)
	public void pauseNewApplication() {
		Application app = new Application();
		try { 
			app.pause();
		} finally {
			app.dispose();
		}
	}

	@Test
	public void pauseSubmittedApplication() throws ApplicationInitializationException {
		Application app = new Application();
		try { 
			app.start();
			app.pause();
			Assert.assertEquals("The status of a Paused application should be 'Paused'",
					ApplicationStatus.PAUSED, app.getStatus());
		} finally {
			app.dispose();
		}
	}

	@Test (expected = IllegalStateException.class)
	public void pauseApplicationTwice() throws ApplicationInitializationException {
		Application app = new Application();
		try { 
			app.start();
			app.pause();
			Assert.assertEquals("The status of a Paused application should be 'Paused'",
					ApplicationStatus.PAUSED, app.getStatus());
			app.pause();
		} finally {
			app.dispose();
		}
	}

	@Test (expected = ObjectDisposedException.class)
	public void pauseDisposedApplication() throws ApplicationInitializationException {
		Application app = new Application();
		try { 
			app.start();
		} finally {
			app.dispose();
		}
		
		app.pause(); //blow up
	}

	@Test (expected = IllegalStateException.class)
	public void resumeNewApplication() throws ApplicationInitializationException {
		Application app = new Application();
		try { 
			app.resume();
		} finally {
			app.dispose();
		}
	}

	@Test (expected = IllegalStateException.class)
	public void resumeSubmittedApplication() throws ApplicationInitializationException {
		Application app = new Application();
		try { 
			app.start();
			app.resume();
		} finally {
			app.dispose();
		}
	}

	@Test
	public void resumeApplication() throws ApplicationInitializationException {
		Application app = new Application();
		try { 
			app.start();
			app.pause();
			app.resume();
			Assert.assertFalse("The status of a Paused application should be 'Paused'",
					(ApplicationStatus.PAUSED == app.getStatus()));
		} finally {
			app.dispose();
		}
	}

	@Test (expected = IllegalStateException.class)
	public void resumeApplicationTwice() throws ApplicationInitializationException {
		Application app = new Application();
		try { 
			app.start();
			app.pause();
			app.resume();
			Assert.assertFalse("The status of a Paused application should be 'Paused'",
					(ApplicationStatus.PAUSED == app.getStatus()));
			app.resume();
		} finally {
			app.dispose();
		}
	}

	@Test (expected = IllegalStateException.class)
	public void resumeCancelledApplication() throws ApplicationInitializationException {
		Application app = new Application();
		try { 
			app.start();
			app.cancel();
			app.resume();
		} finally {
			app.dispose();
		}
	}

	@Test (expected = ObjectDisposedException.class)
	public void resumeDisposedApplication() {
		Application app = new Application();
		app.dispose();
		//by now the app is disposed
		app.resume();
	}

	@Test
	public void startApplicationEmpty() throws ApplicationInitializationException {
		Application app = new Application();
		try { 
			app.start();
			Assert.assertEquals("Application status immediately after submission should be 'submitted'",
					ApplicationStatus.SUBMITTED, app.getStatus());
		} finally {
			app.dispose();
		}
	}

	@Test (expected = IllegalStateException.class)
	public void startApplicationTwice() throws ApplicationInitializationException {
		Application app = new Application();
		try { 
			app.start();
			Assert.assertEquals("Application status immediately after submission should be 'submitted'",
					ApplicationStatus.SUBMITTED, app.getStatus());
			app.start();
		} finally {
			app.dispose();
		}
	}

	@Test
	public void startApplicationWithAJob() throws ApplicationInitializationException  {
		Application app = new Application();
		try { 
			app.addJob(new TestJob());
			app.start();
			Assert.assertEquals("Application status immediately after submission should be 'submitted'",
					ApplicationStatus.SUBMITTED, app.getStatus());
		} finally {
			app.dispose();
		}
	}

	@Test
	public void startApplicationWithJobsAndDeps() throws ApplicationInitializationException, IOException, MalformedURLException {
		List<DependencyInfo> deps = new ArrayList<DependencyInfo>();
		deps.add(DependencyInfo.fromClass(TestJob.class));
		Application app = new Application("myApp", deps);
		try { 
			//add a few jobs
			for (int i = 0; i <= 3; i++) {
				app.addJob(new TestJob());
			}
			app.start();
			Assert.assertEquals("Application status immediately after submission should be 'submitted'",
					ApplicationStatus.SUBMITTED, app.getStatus());
		} finally {
			app.dispose();
		}
	}

	@Test (expected = ObjectDisposedException.class)
	public void startApplicationDisposed() throws ApplicationInitializationException {
		Application app = new Application();
		app.dispose();
		app.start();
	}
}