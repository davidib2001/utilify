using System;

namespace Utilify.Platform.Execution.Shared
{
    //This class needs to be in the Shared lib, since it will be loaded by a secondary app-domain created by the DotNetExecutor process.
    //The DotNetExecutor exe file will not be in the secondary app-domain's application base dir - so this file cannot be compiled into that exe.
    //The only module that uses this is really just the DotNetExecutor - to manage callbacks between the main app-domain and the secondary ones, 
    //through remoting events.

    /// <summary>
    /// Helper class for callbacks - across AppDomains.
    /// </summary>
    /// <remark>
    /// See the following KB article for an explanation of why this class is needed
    /// http://support.microsoft.com/default.aspx?scid=kb;en-us;312114 
    /// </remark>
    public sealed class RemotingEventHelper : MarshalByRefObject
    {
        private event EventHandler<ExecutionCompleteEventArgs> executionComplete;
        public event EventHandler<ExecutionCompleteEventArgs> ExecutionComplete
        {
            add
            {
                executionComplete += value;
                if (Executor != null)
                {
                    Executor.ExecutionComplete += new EventHandler<ExecutionCompleteEventArgs>(Execution_Complete);
                }
            }
            remove
            {
                executionComplete -= value;
                if (Executor != null)
                {
                    Executor.ExecutionComplete -= Execution_Complete;
                }
            }
        }

        private IExecutor executor;
        public RemotingEventHelper(IExecutor executor)
        {
            this.Executor = executor;
        }

        [System.Runtime.Remoting.Messaging.OneWay]
        public void Execution_Complete(object sender, ExecutionCompleteEventArgs args)
        {
            OnExecutionComplete(args);
        }

        private void OnExecutionComplete(ExecutionCompleteEventArgs args)
        {
            EventHelper.RaiseEvent<ExecutionCompleteEventArgs>(executionComplete, this, args);
        }

        public IExecutor Executor
        {
            get { return executor; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("executor");
                this.executor = value;
            }
        }

        // This override ensures that if the object is idle for an extended 
        // period, waiting for messages, it won't lose its lease. Without this 
        // override (or an alternative, such as implementation of a lease 
        // sponsor), an idle object that inherits from MarshalByRefObject 
        // may be (garbage?)collected even though references to it still exist.
        public override object InitializeLifetimeService()
        {
            return null;
        }
    }
}
