#if !MONO

using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;

namespace Utilify.Platform
{
    internal class ServiceErrorHandler : IErrorHandler
    {
        private Logger logger = new Logger();

        public ServiceErrorHandler() { } //should have a public ctor : or else Activator.CreateInstance call in the ErrorBehaviorAttribute will fail.

        #region IErrorHandler Members

        public bool HandleError(Exception error)
        {
            if (error.GetType() == typeof(PlatformException))
            {
                logger.Warn("UnhandledError : ", error); //just log it
                //return true; 
            }
            // Returning true indicates we've performed our behavior
            return true;
        }

        public void ProvideFault(Exception error, System.ServiceModel.Channels.MessageVersion version, ref System.ServiceModel.Channels.Message fault)
        {
            Type faultType = null;
            if (fault == null)
            {
                if (error is ArgumentNullException)
                {
                    ArgumentNullException argEx = error as ArgumentNullException;
                    ArgumentFault argFault = new ArgumentFault(argEx.Message, argEx.ParamName, true);
                    fault = CreateFaultMessage<ArgumentFault>(argFault, version, error.Message, FaultCodeType.Client);
                    faultType = typeof(ArgumentFault);
                }
                else if (error is ArgumentException)
                {
                    ArgumentException argEx = error as ArgumentException;
                    ArgumentFault argFault = new ArgumentFault(argEx.Message, argEx.ParamName, false);
                    fault = CreateFaultMessage<ArgumentFault>(argFault, version, error.Message, FaultCodeType.Client);
                    faultType = typeof(ArgumentFault);
                }
                else if (error is InvalidOperationException || error is InvalidTransitionException)
                {
                    InvalidOperationFault ixFault = new InvalidOperationFault(error.Message);
                    fault = CreateFaultMessage<InvalidOperationFault>(ixFault, version, error.Message, FaultCodeType.Client);
                    faultType = typeof(InvalidOperationFault);
                }
                else if (error is ApplicationNotFoundException)
                {
                    ApplicationNotFoundException nfx = error as ApplicationNotFoundException;
                    EntityNotFoundFault nfFault = new EntityNotFoundFault(nfx.Message, nfx.ApplicationId, EntityType.Application);
                    fault = CreateFaultMessage<EntityNotFoundFault>(nfFault, version, error.Message, FaultCodeType.Client);
                    faultType = typeof(EntityNotFoundFault);
                }
                else if (error is JobNotFoundException)
                {
                    JobNotFoundException nfx = error as JobNotFoundException;
                    EntityNotFoundFault nfFault = new EntityNotFoundFault(nfx.Message, nfx.JobId, EntityType.Job);
                    fault = CreateFaultMessage<EntityNotFoundFault>(nfFault, version, error.Message, FaultCodeType.Client);
                    faultType = typeof(EntityNotFoundFault);
                }
                else if (error is DependencyNotFoundException)
                {
                    DependencyNotFoundException nfx = error as DependencyNotFoundException;
                    EntityNotFoundFault nfFault = new EntityNotFoundFault(nfx.Message, nfx.DependencyId, EntityType.Dependency);
                    fault = CreateFaultMessage<EntityNotFoundFault>(nfFault, version, error.Message, FaultCodeType.Client);
                    faultType = typeof(EntityNotFoundFault);
                }
                else if (error is ResultNotFoundException)
                {
                    ResultNotFoundException nfx = error as ResultNotFoundException;
                    EntityNotFoundFault nfFault = new EntityNotFoundFault(nfx.Message, nfx.ResultId, EntityType.Result);
                    fault = CreateFaultMessage<EntityNotFoundFault>(nfFault, version, error.Message, FaultCodeType.Client);
                    faultType = typeof(EntityNotFoundFault);
                }
                else if (error is ExecutorNotFoundException)
                {
                    ExecutorNotFoundException nfx = error as ExecutorNotFoundException;
                    EntityNotFoundFault nfFault = new EntityNotFoundFault(nfx.Message, nfx.ExecutorId, EntityType.Executor);
                    fault = CreateFaultMessage<EntityNotFoundFault>(nfFault, version, error.Message, FaultCodeType.Client);
                    faultType = typeof(EntityNotFoundFault);
                }
                else if (error is UserNotFoundException)
                {
                    UserNotFoundException nfx = error as UserNotFoundException;
                    EntityNotFoundFault nfFault = new EntityNotFoundFault(nfx.Message, nfx.UserName, EntityType.User);
                    fault = CreateFaultMessage<EntityNotFoundFault>(nfFault, version, error.Message, FaultCodeType.Client);
                    faultType = typeof(EntityNotFoundFault);
                }
                else if (error is GroupNotFoundException)
                {
                    GroupNotFoundException nfx = error as GroupNotFoundException;
                    EntityNotFoundFault nfFault = new EntityNotFoundFault(nfx.Message, nfx.GroupId.ToString(), EntityType.Group);
                    fault = CreateFaultMessage<EntityNotFoundFault>(nfFault, version, error.Message, FaultCodeType.Client);
                    faultType = typeof(EntityNotFoundFault);
                }
                else if (error is DirectoryNotFoundException)
                {
                    DirectoryNotFoundException nfx = error as DirectoryNotFoundException;
                    EntityNotFoundFault nfFault = new EntityNotFoundFault(nfx.Message, string.Empty, EntityType.Directory);
                    fault = CreateFaultMessage<EntityNotFoundFault>(nfFault, version, error.Message, FaultCodeType.Client);
                    faultType = typeof(EntityNotFoundFault);
                }
                else if (error is FileNotFoundException)
                {
                    FileNotFoundException nfx = error as FileNotFoundException;
                    EntityNotFoundFault nfFault = new EntityNotFoundFault(nfx.Message, string.Empty, EntityType.File);
                    fault = CreateFaultMessage<EntityNotFoundFault>(nfFault, version, error.Message, FaultCodeType.Client);
                    faultType = typeof(EntityNotFoundFault);
                }
                else if (error is System.Security.Authentication.AuthenticationException)
                {
                    AuthenticationFault authnFault = new AuthenticationFault(error.Message);
                    fault = CreateFaultMessage<AuthenticationFault>(authnFault, version, error.Message, FaultCodeType.Client);
                    faultType = typeof(AuthenticationFault);
                }
                else if (error is UnauthorizedAccessException)
                {
                    AuthorizationFault authzFault = new AuthorizationFault(error.Message);
                    fault = CreateFaultMessage<AuthorizationFault>(authzFault, version, error.Message, FaultCodeType.Client);
                    faultType = typeof(AuthorizationFault);
                }
                else
                {
                    //any other Exception including InternalServerException, PersistenceException etc...
                    //we convert to InternalServerFault: to shield the client from it.
                    //this is an internal server error!
                    // IMP: do not expose any information to the client
                    ServerFault serverFault = new ServerFault(Messages.InternalServerError, Messages.InternalServerErrorDescription);

                    //there seems to be some problem with saying - FaultCodeType.Server - here : 
                    //it causes a strange MessageSecurityException on the client
                    //instead of a nice FaultException<ServerFault>.
                    //so changed it back to FaultCodeType.Client.
                    fault = CreateFaultMessage<ServerFault>(serverFault, version, serverFault.Message, FaultCodeType.Client);
                    faultType = typeof(ServerFault);
                }
            }
            if (error != null && faultType != null)
            {
                logger.Warn("ProvideFault called. Converted {0} to {1}....", error.GetType(), faultType);
                logger.Warn("The error was: ", error);
            }
        }

        #endregion

        private enum FaultCodeType
        {
            Client = 0,
            Server = 1
        }

        private static Message CreateFaultMessage<T>(T faultDetail, MessageVersion version, string errorMessage, FaultCodeType faultCodeType)
        {
            FaultCode faultCode = null;
            if (faultCodeType == FaultCodeType.Server)
            {
                faultCode = FaultCode.CreateReceiverFaultCode(null);
            }
            else // faultCodeType == FaultCodeType.Client
            {
                faultCode = FaultCode.CreateSenderFaultCode(null);
            }

            FaultException<T> fe = new FaultException<T>(faultDetail, errorMessage, faultCode);
            MessageFault mf = fe.CreateMessageFault();

            Message faultMessage = Message.CreateMessage(version, mf, null);
            return faultMessage;
        }
    }
}

#endif
