using System;
using System.Collections.Generic;
using System.IdentityModel.Claims;
using System.IdentityModel.Policy;
using System.Security.Principal;
using System.ServiceModel;
using Utilify.Platform.Manager.Persistence;

namespace Utilify.Platform.Manager.Security
{
    // Custom claims-based Authorization for access to services.
    // 
    // Reasons we are using Claims-based authorization and the Identity model is because:
    // 1) We need to handle disparate credential types with the same code
    // 2) We require a richer/more customised authorization
    
    public class ManagerAuthorizationPolicy : IAuthorizationPolicy
    {
        internal static readonly string IdPrefix = typeof(ManagerAuthorizationPolicy).FullName;

        private readonly Logger logger = new Logger();
        private string id;

        public ManagerAuthorizationPolicy()
        {
            id = IdPrefix + Guid.NewGuid().ToString(); 
        }

        #region IAuthorizationPolicy Members

        bool IAuthorizationPolicy.Evaluate(EvaluationContext evaluationContext, ref object state)
        {
            bool evaluated = false;

            CustomAuthState customstate = null;
            string username = null;
            IdentityType idType = IdentityType.Unknown;

            // Get Username and Identity Type
            List<IIdentity> identities = (List<IIdentity>)evaluationContext.Properties["Identities"];
            if (identities.Count > 0)
            {                
                // Expect only one identity
                IIdentity id = identities[0];                
                username = id.Name;
                string idTypeName = id.GetType().Name;
                if (Enum.IsDefined(typeof(IdentityType), idTypeName))
                {
                    idType = (IdentityType)Enum.Parse(typeof(IdentityType), idTypeName);
                }
            }
            
            //logger.Debug("Username: " + username);
            //logger.Debug("Authentication Type: " + idType);

            // If the state is null, then this has not been called before so set up a custom state.
            if (state == null)
            {
                customstate = new CustomAuthState();
                state = customstate;
            }
            else
                customstate = (CustomAuthState)state;

            if (username != null)
            {
                // If claims have not been added yet...
                if (!customstate.ClaimsAdded)
                {
                    // Get our internal username to add it as an Identity Claim
                    string internalUsername = GetInternalUsername(username, idType);
                    //logger.Debug("Got internal user name (ManagerAuthzPolicy): {0}", internalUsername);
                    string errorMsg = string.Format("Access is denied for user {0} using {1}", username, idType);
                    if (string.IsNullOrEmpty(internalUsername))
                    {
                        throw new FaultException<AuthenticationFault>(new AuthenticationFault(errorMsg),
                            errorMsg, FaultCode.CreateSenderFaultCode(null));
                    }

                    // Create an empty list of claims.
                    IList<Claim> claims = new List<Claim>();
                    List<Permission> permissions = GetUserPermissions(internalUsername);

                    List<string> roles = new List<string>();
                    if (permissions != null)
                    {
                        foreach (Permission p in permissions)
                        {
                            claims.Add(new Claim(SecurityHelper.PermissionClaimType, p, Rights.PossessProperty));

                            //also add it to the list of roles
                            roles.Add(p.ToString());
                        }
                    }

                    claims.Add(new Claim(SecurityHelper.IdentityClaimType, internalUsername, Rights.Identity));

                    // Add claims to the evaluation context.                    
                    evaluationContext.AddClaimSet(this, 
                        new DefaultClaimSet((this as IAuthorizationPolicy).Issuer, 
                            claims));

                    //also set the Thread's current principal:
                    evaluationContext.Properties["Principal"] =
                        new GenericPrincipal(
                            new GenericIdentity(internalUsername, IdentityType.GenericIdentity.ToString()),
                                roles.ToArray());
                    
                    // Record in our custom state object that claims were added.
                    customstate.ClaimsAdded = true;

                    // Return true, indicating that this method does not need to be called again.
                    evaluated = true;
                }
                else
                {
                    // Should never get here, but just in case, return true.
                    evaluated = true;
                }

                //foreach (ClaimSet cs in evaluationContext.ClaimSets)
                //{
                //    DisplayClaimSet(cs);
                //}
            }

            return evaluated;
        }

        private List<Utilify.Platform.Manager.Security.Permission> GetUserPermissions(string username)
        {
            // Get list of permissions available to this user
            List<Utilify.Platform.Manager.Security.Permission> perms = null;
            try
            {
                perms =  SecurityDao.GetUserPermissions(username);
            }
            catch (Exception ex)
            {
                logger.Warn("Error getting user permissions", ex.ToString());
            }
            return perms;
        }

        private string GetInternalUsername(string username, IdentityType idType)
        {
            // Links up the given username with the internal username
            return SecurityDao.GetInternalUsername(username, idType);
        }


        #region PRINT OUT CLAIMS

        const string CLAIM_FORMAT_STRING = "{0, -5} {1, -15} {2}";

        private void DisplayClaimSet(ClaimSet cs)
        {
            logger.Debug("");
            if (cs == ClaimSet.System)
                logger.Debug("* System ClaimSet *");
            logger.Debug("");
            logger.Debug(CLAIM_FORMAT_STRING, "RIGHT", "CLAIM_TYPE", "RESOURCE");

            // display claims for subject of this claimset
            foreach (Claim c in cs)
            {
                DisplayClaim(c);
            }

            logger.Debug("");

            // display claims for issuer of this claimset
            if (null == cs.Issuer)
            {
                logger.Debug("ISSUED BY: null");
            }
            else if (object.ReferenceEquals(cs, cs.Issuer))
            {
                // self-asserted claims (issuer is the same as subject)
                logger.Debug("ISSUED BY: self");
            }
            // Commented out. Dont follow through to issuer's ClaimSet
            else
            {
                DisplayClaimSet(cs.Issuer);
            }
        }

        // print a compact display of the supplied claim
        private void DisplayClaim(Claim c)
        {
            string right;
            if (c.Right.Equals(Rights.Identity))
            {
                right = "ID";
            }
            else if (c.Right.Equals(Rights.PossessProperty))
            {
                right = "PP";
            }
            else right = c.Right;

            // make things a little more readable
            string claimType = c.ClaimType.Replace(
                "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/",
                ".../");
            string value = StringizeResource(c.Resource);

            logger.Debug(CLAIM_FORMAT_STRING, right, claimType, value);
        }

        private string StringizeResource(object resource)
        {
            // sometimes the claim's value will simply be a string
            string stringResource = resource as string;
            if (null != stringResource) return stringResource + " (string)";

            System.Security.Principal.SecurityIdentifier sid = resource as System.Security.Principal.SecurityIdentifier;
            if (null != sid) return GetAccountName(sid) + " (SecurityIdentifier)";

            System.Security.Cryptography.X509Certificates.X500DistinguishedName dn = resource as System.Security.Cryptography.X509Certificates.X500DistinguishedName;
            if (null != dn) return dn.Name + " (X500DistinguishedName)";

            System.Security.Cryptography.RSACryptoServiceProvider key = resource as System.Security.Cryptography.RSACryptoServiceProvider;
            if (null != key) return string.Format(
                "{0} bit RSA public key (RSACryptoServiceProvider)",
                key.KeySize);

            if (resource is Permission) return ((Permission)resource) + " (Permission)";

            // for anything else, just display the type name
            return string.Format("({0})", resource.GetType().Name);
        }

        private string GetAccountName(System.Security.Principal.SecurityIdentifier sid)
        {
            try
            {
                return sid.Translate(typeof(System.Security.Principal.NTAccount)).ToString();
            }
            catch (System.Security.Principal.IdentityNotMappedException)
            {
                return sid.Value;
            }
        }

        #endregion

        System.IdentityModel.Claims.ClaimSet IAuthorizationPolicy.Issuer
        {
            get { return ClaimSet.System; }
        }

        #endregion

        #region IAuthorizationComponent Members

        string IAuthorizationComponent.Id
        {
            get { return id; }
        }

        #endregion

        #region CustomAuthState
        // Internal class for keeping track of state.
        class CustomAuthState
        {
            bool claimsAdded;

            public CustomAuthState()
            {
                claimsAdded = false;
            }

            public bool ClaimsAdded
            {
                get { return claimsAdded; }
                set { claimsAdded = value; }
            }
        }
        #endregion
    }
}