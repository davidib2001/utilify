﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Utilify.Platform.Manager")]
[assembly: AssemblyDescription("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("fa51c23b-ff52-4df4-a05b-a30cf4841b4b")]

[assembly: CLSCompliant(true)]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]

//Need to merge Utilify.Manager into CloudProxy - orelse, we can't obfuscate CloudProxy.
[assembly: InternalsVisibleTo("Utilify.CloudProxy")]

#if TEST
[assembly: InternalsVisibleTo("Utilify.Platform.Manager.Tests")]
[assembly: InternalsVisibleTo("Utilify.Platform.Executor.Tests")]
[assembly: InternalsVisibleTo("Utilify.Platform.Tests.Common")]
[assembly: InternalsVisibleTo("TestConsole")]
#endif