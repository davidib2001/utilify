using System;
using System.Collections.Generic;
using Utilify.Platform.Contract;

namespace Utilify.Platform.Manager.Persistence
{
    //unfortunately NHibernate forces us to use a 'public' class to generate its proxy.
    //and we need to use a proxy for good performance.
    [Serializable]
    public class Job : 
        ITransformable<JobSubmissionInfo>,
        ITransformable<JobStatusInfo>,
        ITransformable<JobCompletionInfo>,
        ITransformable<JobInfo>
    {
        private Guid id;
        private long version = DataStore.UnsavedVersionValue;
        private Application application;
        private IList<Dependency> dependencies;
        private BinaryData initialJobInstance;
        private BinaryData jobInstance;
        private BinaryData jobException;
        private IList<Result> results;
        private JobType jobType;
        private ClientPlatform clientPlatform;

        private Executor executionHost;
        private JobStatus status = JobStatus.UnInitialized;

        private DateTime submittedTime; 
        private DateTime scheduledTime;
        private DateTime startTime;
        private DateTime finishTime;

        private long cpuTime; //duration in milliseconds.
        private int mappedCount;

        private String executionLog;
        private String executionError;

        public virtual Guid Id
        {
            get { return id; }
            protected set 
            {
                if (value == Guid.Empty)
                    throw new ArgumentException("Job id cannot be empty", "id");
                id = value; 
            }
        }

        /// <summary>
        /// Gets the persistent version of this object
        /// </summary>
        public virtual long Version
        {
            get { return version; }
            protected set { version = value; }
        }

        public virtual Application Application
        {
            get { return application; }
            internal set
            {
                if (value == null)
                    throw new ArgumentNullException("application");
                application = value;
            }
        }

        public virtual DateTime StartTime
        {
            get { return startTime; }
            set { startTime = Helper.MakeUtc(value); }
        }

        public virtual DateTime SubmittedTime
        {
            get { return submittedTime; }
            set { submittedTime = Helper.MakeUtc(value); }
        }

        public virtual Executor ExecutionHost
        {
            get { return executionHost; }
            set { executionHost = value;  } //can be null if the job has not yet started running anywhere
        }

        /// <summary>
        /// Gets or sets the job status.
        /// </summary>
        /// <exception cref="Utilify.Platform.Manager.InvalidTransitionException">
        /// If the status is set to an invalid value, which is not allowed for the current status.
        /// </exception>
        public virtual JobStatus Status
        {
            get { return status; }
            set 
            {
                //IMP: We double-check here as well as the caller to ensure correct state transitions are happening.
                string msg = "Invalid argument 'status'. Job state cannot transition from {0} to {1} for job {2}.";
                //only certain state transitions are allowed:
                //Un-initialized -> Submitted
                //Submitted -> Ready / Cancelled
                //Ready -> Scheduled / Cancelled
                //Scheduled -> Executing / Ready (if un-mapped) / Cancelled
                //Executing -> Completing / Ready (if reset) / Cancelled
                //Completing -> Completed / Ready (if reset) / Cancelled
                //Completed

                InvalidTransitionException ex = new InvalidTransitionException(string.Format(msg, status, value, this.Id));
                // Check the current status
                switch (status)
                {
                    case JobStatus.UnInitialized:
                        if (value != JobStatus.Submitted)
                            throw ex;
                        break;
                    case JobStatus.Submitted:
                        if (value != JobStatus.Ready &&
                            value != JobStatus.Cancelled)
                            throw ex;
                        break;
                    case JobStatus.Ready:
                        if (value != JobStatus.Scheduled &&
                            value != JobStatus.Cancelled)
                            throw ex;
                        break;
                    case JobStatus.Scheduled:
                        if (value != JobStatus.Executing &&
                            value != JobStatus.Ready &&
                            value != JobStatus.Cancelled)
                            throw ex;
                        break;
                    case JobStatus.Executing:
                        if (value != JobStatus.Completing &&
                            value != JobStatus.Ready &&
                            value != JobStatus.Cancelled)
                            throw ex;
                        break;
                    case JobStatus.Completing:
                        if (value != JobStatus.Completed &&
                            value != JobStatus.Ready &&
                            value != JobStatus.Cancelled)
                            throw ex;
                        break;
                    case JobStatus.Completed:
                        throw ex;
                    default:
                        throw ex;
                }
                status = value;
            }
        }

        public virtual DateTime FinishTime
        {
            get { return finishTime; }
            set { finishTime = Helper.MakeUtc(value); }
        }

        public virtual DateTime ScheduledTime
        {
            get { return scheduledTime; }
            set { scheduledTime = Helper.MakeUtc(value); }
        }

        public virtual JobType JobType
        {
            get { return jobType; }
            protected set { jobType = value; }
        }

        public virtual ClientPlatform ClientPlatform
        {
            get { return clientPlatform; }
            protected set { clientPlatform = value; }
        }

        public virtual String ExecutionLog
        {
            get { return executionLog; }
            set { executionLog = value; }
        }

        public virtual String ExecutionError
        {
            get { return executionError; }
            set { executionError = value; }
        }

        public virtual long CpuTime
        {
            get { return cpuTime; }
            set { cpuTime = value; }
        }

        public virtual int MappedCount
        {
            get { return mappedCount; }
            set { mappedCount = value; }
        }

        #region DateTime persistence properties (using Ticks)

        protected virtual long StartTimeTicks
        {
            get { return startTime.Ticks; }
            set
            {
                if (value < DateTime.MinValue.Ticks)
                    throw new ArgumentOutOfRangeException("startTime", string.Format("Start time cannot be less than {0}", DateTime.MinValue));
                startTime = new DateTime(value, DateTimeKind.Utc);
            }
        }
        protected virtual long SubmittedTimeTicks
        {
            get { return submittedTime.Ticks; }
            set
            {
                if (value < DateTime.MinValue.Ticks)
                    throw new ArgumentOutOfRangeException("submittedTime", string.Format("Submitted time cannot be less than {0}", DateTime.MinValue));
                submittedTime = new DateTime(value, DateTimeKind.Utc);
            }
        }
        protected virtual long FinishTimeTicks
        {
            get { return finishTime.Ticks; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("finishTime", string.Format("Finish time cannot be less than {0}", DateTime.MinValue));
                finishTime = new DateTime(value, DateTimeKind.Utc);
            }
        }
        protected virtual long ScheduledTimeTicks
        {
            get { return scheduledTime.Ticks; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("scheduledTime", string.Format("Scheduled time cannot be less than {0}", DateTime.MinValue));
                scheduledTime = new DateTime(value, DateTimeKind.Utc);
            }
        }

        #endregion

        protected Job() { }

        public Job(JobSubmissionInfo jobInfo) : this()
        {
            if (jobInfo == null)
                throw new ArgumentNullException("jobInfo");

            //only validate. we attach the app to the job when it is saved 
            Guid appId = Helper.GetGuidSafe(jobInfo.ApplicationId);
            if (appId == Guid.Empty)
                throw new ArgumentException(
                    string.Format(Messages.NullEmptyOrInvalidFormat, "application id", jobInfo.ApplicationId));

            this.JobType = jobInfo.JobType;
            this.ClientPlatform = jobInfo.ClientPlatform;
            //set both initial instance and instance
            this.jobInstance = new BinaryData(jobInfo.GetJobInstance());
            this.initialJobInstance = new BinaryData(jobInfo.GetJobInstance());
            this.SubmittedTime = DateTime.UtcNow;

            DependencyInfo[] deps = jobInfo.GetDependencies();
            this.dependencies = Dependency.ConvertFromInfo(deps).ToArray();

            ResultInfo[] expectedResults = jobInfo.GetExpectedResults();
            this.results = Result.ConvertFromInfo(expectedResults).ToArray();

            this.Status = JobStatus.Submitted; //default state after the ctor, for the Job on the Manager
        }

        public virtual byte[] GetInitialJobInstance()
        {
            if (initialJobInstance != null)
                return initialJobInstance.GetValue();
            else
                return null;
        }

        public virtual byte[] GetJobInstance()
        {
            if (jobInstance != null)
                return jobInstance.GetValue();
            else
                return null;
        }

        // We have initialJobInstance and finalJobInstance like in the Executor.
        // seems that it could be useful to keep the original and the final one.
        // Will help with resubmitting jobs or for tracking.
        internal virtual void UpdateJobInstance(byte[] instance)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");
            if(instance.Length == 0)
                throw new ArgumentException("Job instance cannot be empty.", "instance");

            jobInstance = new BinaryData(instance);
        }

        internal virtual void UpdateJobException(byte[] exceptionInstance)
        {
            if (exceptionInstance == null)
                throw new ArgumentNullException("exceptionInstance");
            if (exceptionInstance.Length == 0)
                throw new ArgumentException("Job exception instance cannot be empty.", "exceptionInstance");

            jobException = new BinaryData(exceptionInstance);
        }

        public virtual Result[] GetResults()
        {
            return Helper.GetCopy<Result>(results);
        }

        public virtual Dependency[] GetDependencies()
        {
            return Helper.GetCopy<Dependency>(dependencies);
        }

        //This is needed to be able to update the dependency links with existing cached dependencies in the db
        internal virtual IList<Dependency> Dependencies
        {
            get
            {
                return this.dependencies;
            }
        }

        #region ITransformable<JobSubmissionInfo> Members

        JobSubmissionInfo ITransformable<JobSubmissionInfo>.Transform()
        {
            List<DependencyInfo> depInfos =
                DataStore.Transform<Dependency, DependencyInfo>(dependencies);

            List<ResultInfo> resultInfos = 
                DataStore.Transform<Result, ResultInfo>(results);

            return new JobSubmissionInfo(
                this.Id.ToString(),
                this.Application.Id.ToString(),
                this.JobType,
                this.ClientPlatform,
                this.GetJobInstance(),
                depInfos,
                resultInfos);
        }

        #endregion

        #region ITransformable<JobStatusInfo> Members

        JobStatusInfo ITransformable<JobStatusInfo>.Transform()
        {
            string executionHost = (this.ExecutionHost == null) ? string.Empty : this.ExecutionHost.Uri;

            JobStatusInfo status = new JobStatusInfo(this.Application.Id.ToString(), this.Id.ToString(),
                executionHost,
                this.SubmittedTime, this.ScheduledTime,
                this.StartTime, this.FinishTime,
                this.Status);

            return status;
        }

        #endregion

        #region ITransformable<JobCompletionInfo> Members

        JobCompletionInfo ITransformable<JobCompletionInfo>.Transform()
        {
            string executionHost = (this.ExecutionHost == null) ? string.Empty : this.ExecutionHost.Uri;
            JobCompletionInfo info = new JobCompletionInfo(
                this.Id.ToString(),
                this.Application.Id.ToString(),
                this.GetJobInstance(),
                this.GetJobException(),
                executionHost,
                this.ExecutionLog,
                this.ExecutionError,
                this.SubmittedTime,
                this.ScheduledTime,
                this.StartTime,
                this.FinishTime,
                this.CpuTime,
                this.MappedCount);
            return info;
        }

        public virtual byte[] GetJobException()
        {
            if (jobException != null)
                return jobException.GetValue();
            else
                return null;
        }

        #endregion

        #region ITransformable<JobInfo> Members

        JobInfo ITransformable<JobInfo>.Transform()
        {
            return new JobInfo(this.Id.ToString(), this.Application.Id.ToString(), this.JobType, this.ClientPlatform);
        }

        #endregion
    }
}
