﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilify.Platform.Security;

namespace Utilify.Platform.Manager.Persistence
{
    [Serializable]
    internal class User
    {
        //private long version = DataStore.UnsavedVersionValue;

        private string name; //this is the db pk as well.
        private string password;
        private string windowsUsername;
        private string certificateName;
        private Group group;

        private User() { }

        public User(UserInfo info) : this()
        {
            if (info == null)
                throw new ArgumentNullException("info");

            this.Name = info.Name;
            this.CertificateName = info.CertificateName;
            this.WindowsUsername = info.WindowsUsername;
            this.Password = info.Password;
            this.Group = new Group(info.Group);
        }

        public string Name
        {
            get { return name; }
            private set 
            {
                if (value == null)
                    throw new ArgumentNullException("Name");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("User name cannot be empty", "Name");
                name = value;
            }
        }

        private string Password
        {
            get { return password; }
            set 
            {
                if (value == null)
                    throw new ArgumentNullException("Password");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Password cannot be empty", "Password");

                password = value; 
            }
        }

        public string WindowsUsername //todoLater: improve this, so that a number of windows / certs can be mapped to the same user
        {
            get { return windowsUsername; }
            set { windowsUsername = value; }
        }

        public string CertificateName
        {
            get { return certificateName; }
            set { certificateName = value; }
        }

        public Group Group
        {
            get { return group; }
            internal set 
            {
                if (value == null)
                    throw new ArgumentNullException("Group");

                group = value; 
            }
        }

        public UserInfo GetUserInfo()
        {
            return new UserInfo(name, password, windowsUsername, certificateName, group.GetGroupInfo());
        }

    }
}
