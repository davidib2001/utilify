using System;
using System.Collections.Generic;
using Utilify.Platform.Contract;
using Utilify.Platform.Manager.Persistence;
using Utilify.Platform.Manager.Security;

namespace Utilify.Platform.Manager
{
    /*
     * We implement an interface to share the code between the client and the service.
     */
    /// <summary>
    /// 
    /// </summary>
#if !MONO
    [ErrorBehavior(typeof(ServiceErrorHandler))] //attach the error handling behaviour which does Exception to Fault mapping
    [System.ServiceModel.ServiceBehavior(
        Namespace = Namespaces.Manager,
        InstanceContextMode = System.ServiceModel.InstanceContextMode.Single,
        ConcurrencyMode = System.ServiceModel.ConcurrencyMode.Multiple,
        AddressFilterMode = System.ServiceModel.AddressFilterMode.Any)]
#endif
    internal class JobManager : MarshalByRefObject, IJobManager
    {
        private Logger logger = new Logger();

        #region IManagerService Members

        public void CheckService() { }

        #endregion

        #region IJobManager Members

        public Utilify.Platform.ApplicationSubmissionInfo GetApplication(string executorId, string applicationId) //todoDoc
        {
            // Ensure user has permissions to Execute jobs
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ExecuteJob);

            if (applicationId == null)
                throw new ArgumentNullException("applicationId");
            if (applicationId.Trim().Length == 0)
                throw new ArgumentException(Messages.NullOrEmpty, "applicationId");

            //we only check that the executorId passed in is valid.
            //nothing special to check here, other than this
            if (executorId == null)
                throw new ArgumentNullException("executorId");
            if (executorId.Trim().Length == 0)
                throw new ArgumentException(Messages.NullOrEmpty, "applicationId");

            Guid id = Helper.GetGuidSafe(applicationId);
            if (id == Guid.Empty)
                throw new ApplicationNotFoundException(Messages.ApplicationNotFound, "applicationId");

            ApplicationSubmissionInfo info = null;
            try
            {
                info = ApplicationDao.GetApplicationSubmissionInfo(id);

                if (info == null)
                    throw new ApplicationNotFoundException(Messages.ApplicationNotFound, applicationId);
            }
            catch (PersistenceException px) //need to do this for the remoting interface
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
            return info;
        }

        public Utilify.Platform.JobSubmissionInfo[] GetJobs(string executorId) //todoDoc
        {
            // Ensure user has permissions to Execute jobs
            SecurityHelper.EnsurePermission(PermissionRequirement.All, 
                Utilify.Platform.Manager.Security.Permission.ExecuteJob);

            if (executorId == null)
                throw new ArgumentNullException("executorId");
            if (String.IsNullOrEmpty(executorId))
                throw new ArgumentException("Executor Id cannot be empty", "executorId");
            if (Helper.GetGuidSafe(executorId) == Guid.Empty)
                throw new ArgumentException("Executor Id is not in the correct format", "executorId");

            logger.Debug("Getting Jobs for Executor " + executorId);

            Guid executorGuid = Helper.GetGuidSafe(executorId);

            List<JobSubmissionInfo> jobsToReturn = new List<JobSubmissionInfo>();
            try
            {
                if (!SystemInfoDao.ExecutorExists(executorGuid))
                    throw new ExecutorNotFoundException("Cannot get Jobs for Executor.", executorId);

                IList<JobSubmissionInfo> jobs = ApplicationDao.GetJobsForExecutor(executorGuid);
                foreach (JobSubmissionInfo job in jobs)
                {
                    try
                    {
                        ApplicationDao.SetJobStarted(Helper.GetGuidSafe(job.JobId));
                        jobsToReturn.Add(job);
                    }
                    catch (InvalidTransitionException ix)
                    {
                        logger.Info(ix.Message + "InvalidTransitionException {1}, for Job id: {0}. Not sending job to Executor.", 
                            job.JobId, ix.Message);
                    }
                }
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
            return jobsToReturn.ToArray();
        }

        public string[] GetCancelledJobs(string executorId)
        {
            // Ensure user has permissions to Execute jobs
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ExecuteJob);

            if (executorId == null)
                throw new ArgumentNullException("executorId");

            if (String.IsNullOrEmpty(executorId))
                throw new ArgumentException("Executor Id cannot be empty", "executorId");

            Guid executorGuid = Helper.GetGuidSafe(executorId);
            if (executorGuid == Guid.Empty)
                throw new ArgumentException("Executor Id is not in the correct format", "executorId");

            List<string> jobIdsToReturn = new List<string>();            
            try
            {
                if (!SystemInfoDao.ExecutorExists(executorGuid))
                    throw new ExecutorNotFoundException("Cannot get Cancelled Jobs for Executor.", executorId);
                
                jobIdsToReturn = ApplicationDao.UnmapCancelledJobs(executorGuid);

            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }

            return jobIdsToReturn.ToArray();
        }

        public Utilify.Platform.DependencyContent GetDependency(string dependencyId) //todoDoc
        {
            // Ensure user has permissions to Execute jobs
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ExecuteJob);

            if (dependencyId == null)
                throw new ArgumentNullException("dependencyId");
            if (String.IsNullOrEmpty(dependencyId))
                throw new ArgumentException("Dependency Id cannot be empty", "dependencyId");

            Guid id = Helper.GetGuidSafe(dependencyId);
            if (id == Guid.Empty)
                throw new DependencyNotFoundException("Dependency with specified Id could not be found.", "dependencyId");

            DependencyContent content = null;
            try
            {
                Dependency dependency = ApplicationDao.GetDependency(id, FetchStrategy.Eager);

                if (dependency == null)
                    throw new DependencyNotFoundException("Dependency with sepcified Id could not be found.", "dependencyId");

                if (dependency.GetContent() != null)
                {
                    content = new DependencyContent(dependency.Id.ToString(), dependency.GetContent());
                } //else it will return null

                // todoDiscuss: should we rename DependencyContent.content to data? likewise with Results
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
            return content;
        }

        // Issue 016, 017, 018: Discuss how to update the jobInstance. Maybe we want to keep original and completed job instance in database.
        // Add a field to the Job containing some execution history and completion time
        public void SendCompletedJob(JobCompletionInfo info)
        {
            // Ensure user has permissions to Execute jobs
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ExecuteJob);

            if (info == null)
                throw new ArgumentNullException("info");
            if (String.IsNullOrEmpty(info.JobId))
                throw new ArgumentException("Job Id cannot be empty", "info.JobId");
            if (String.IsNullOrEmpty(info.ApplicationId))
                throw new ArgumentException("Application Id cannot be empty", "info.ApplicationId");

            logger.Debug("Received Completed Job : " + info.JobId);

            Guid id = Helper.GetGuidSafe(info.JobId);
            if (id == Guid.Empty)
                throw new JobNotFoundException("Job Id was invalid or empty.", "info.JobId");

            if (info.GetJobInstance() == null)
                throw new ArgumentException("Job InitialInstance cannot be empty.", "JobInstance");

            Utilify.Platform.Manager.Persistence.Job job = null;
            try
            {
                job = ApplicationDao.GetJob(id); // dont need eager fetch here anymore.
                if (job.Status != JobStatus.Cancelled)
                {
                    ApplicationDao.SetJobCompleting(job.Id, info);
                }
            }
            catch (InvalidTransitionException ix)
            {
                logger.Info(ix.Message + "InvalidTransitionException {1}, for Job id: {0}. Not setting job to completing.",
                    job.Id, ix.Message);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
        }

        // Issue 019
        public void SendResult(ResultContent content)
        {
            // Ensure user has permissions to Execute jobs
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ExecuteJob);

            if (content == null)
                throw new ArgumentNullException("content");
            if (String.IsNullOrEmpty(content.JobId))
                throw new ArgumentException("Job Id cannot be empty", "content.JobId");
            if (String.IsNullOrEmpty(content.ResultId))
                throw new ArgumentException("Result Id cannot be empty", "content.ResultId");

            Guid id = Helper.GetGuidSafe(content.ResultId);
            if (id == Guid.Empty)
                throw new ResultNotFoundException("Result Id was invalid or empty.", "content.ResultId");
            if (content.GetContent() == null)
                throw new ArgumentException("Result content byte[] cannot be null", "content");

            logger.Debug("Received Result : " + content.JobId + " " + content.ResultId);

            try
            {
                // Issue 020: Check for AppId/JobId?
                if (!ApplicationDao.ResultExists(id))
                    throw new ArgumentException(Messages.ResultNotFound + ": " + id.ToString(),
                        "content.ResultId");
                ApplicationDao.SaveResultContent(content);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
        }

        /// <summary>
        /// Tells the Manager which Jobs the Executor is currently working on.
        /// Jobs assigned to the Executor on the Manager that are not in this list will be reset.
        /// </summary>
        /// <param name="info"></param>
        public void UpdateCurrentJobs(JobExecutionInfo info)
        {
            // Ensure user has permissions to Execute jobs
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ExecuteJob);

            if (info == null)
                throw new ArgumentNullException("info");
            if (String.IsNullOrEmpty(info.ExecutorId))
                throw new ArgumentException("ExecutorId cannot be empty.", "executorId");
            if (info.JobIds == null)
                throw new ArgumentNullException("info.JobIds");

            Guid executorGuid = Helper.GetGuidSafe(info.ExecutorId);
            try
            {
                if (!SystemInfoDao.ExecutorExists(executorGuid))
                    throw new ExecutorNotFoundException("Cannot update Jobs for Executor.", info.ExecutorId);

                // Reset mapping and status of any job not in this executing list that is assigned to this executor.
                IList<Guid> currentJobIds = ApplicationDao.GetExecutingJobIds(executorGuid);
                List<String> jobIdsToUpdate = new List<String>(info.JobIds);

                int count = 0;
                foreach (Guid jobId in currentJobIds)
                {
                    if (!jobIdsToUpdate.Contains(jobId.ToString()))
                    {
                        ApplicationDao.UnmapJob(jobId, JobStatus.Ready);
                        count++;
                    }
                }
                if (count > 0)
                    logger.Info("Resetting {0} jobs that were running on executor {1}", count, executorGuid);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
        }

        #endregion
    }
}

