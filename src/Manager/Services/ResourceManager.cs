using System;
using System.Collections.Generic;


using Utilify.Platform.Contract;
using Utilify.Platform;
using Utilify.Platform.Manager.Persistence;
using Utilify.Platform.Manager.Security;

namespace Utilify.Platform.Manager
{
#if !MONO
    [ErrorBehavior(typeof(ServiceErrorHandler))] //attach the error handling behaviour which does Exception to Fault mapping
    [System.ServiceModel.ServiceBehavior(
        Namespace = Namespaces.Manager,
        InstanceContextMode = System.ServiceModel.InstanceContextMode.Single,
        ConcurrencyMode = System.ServiceModel.ConcurrencyMode.Multiple,
        AddressFilterMode = System.ServiceModel.AddressFilterMode.Any)]
#endif
    internal class ResourceManager : MarshalByRefObject, IResourceManager
    {
        private Logger logger = new Logger();

        #region IResourceManager Members

        public void CheckService() { }

        public SystemInfo[] GetExecutors() //todoTest: need to test with WCF security
        {
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ManageExecutors);

            List<SystemInfo> infos = null;
            try
            {
                logger.Info("Call to get list of all executors.");

                infos = SystemInfoDao.GetExecutors();

                if (infos == null)
                    infos = new List<SystemInfo>();

                logger.Info("# executors = " + infos.Count);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }

            return infos.ToArray();
        }

        public PerformanceInfo[] GetSystemPerformance() //todoTest: need to test with WCF security
        {
            SecurityHelper.EnsurePermission(PermissionRequirement.All, 
                Utilify.Platform.Manager.Security.Permission.ManageExecutors);

            List<PerformanceInfo> infos = null;
            try
            {
                //logger.Debug("Call to get list of all executor perfs.");

                infos = SystemInfoDao.GetExecutorPerformance();

                if (infos == null)
                    infos = new List<PerformanceInfo>();

                //logger.Debug("# executor perfs = " + infos.Count);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }

            return infos.ToArray();
        }

        #endregion
    }
}
