using System;
using System.Collections.Generic;


using Utilify.Platform.Manager.Persistence;
using Utilify.Platform.Manager.Security;

namespace Utilify.Platform.Manager
{
    //todo: (ApplicationManager) expand class summary docs

    /// <summary>
    /// Manages applications and jobs that are submitted by client programs for distributed execution. The ApplicationManager
    /// is the remote interface to the job scheduler that includes various functions such as :
    /// - accepting application / job submissions from the client
    /// - performing application / job actions requested by the client such as: start / stop / cancel / get status etc
    /// - serving requests to get information about past jobs and applications and 'connect' to them
    /// </summary>
#if !MONO
    [ErrorBehavior(typeof(ServiceErrorHandler))] //attach the error handling behaviour which does Exception to Fault mapping
    [System.ServiceModel.ServiceBehavior(
        Namespace = Namespaces.Manager, 
        InstanceContextMode = System.ServiceModel.InstanceContextMode.Single,
        ConcurrencyMode = System.ServiceModel.ConcurrencyMode.Multiple,
        AddressFilterMode = System.ServiceModel.AddressFilterMode.Any)]
#endif
    //[WebService (Namespace = Namespaces.Manager)] 
    //not doing plain ASMX - since that has no security + XmlSerializer means we need seperate *Info classes
    internal class ApplicationManager : MarshalByRefObject, IApplicationManager
    {
        private Logger logger = new Logger();

        #region IManagerService Members

        public void CheckService() { }

        #endregion

        #region IApplicationManager members

        /// <summary>
        /// Aborts the specified application. 
        /// Adding (submitting) more jobs to an aborted application will cause an exception. 
        /// </summary>
        /// <exception cref="System.ArgumentNullException">applicationId is a null reference</exception>
        /// <exception cref="System.ArgumentException">applicationId is an empty string or in an invalid format</exception>
        /// <exception cref="Utilify.Platform.ApplicationNotFoundException">An application with the specified applicationId is not found</exception>
        public void AbortApplication(string applicationId)
        {
            if (applicationId == null)
                throw new ArgumentNullException("applicationId");

            Guid id = Helper.GetGuidSafe(applicationId);
            if (id == Guid.Empty)
                throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "application id", applicationId),
                    "applicationId");

            try
            {
                //need to pull out the app object and not just an exists check here, 
                //since we use the app object later to check status etc.
                //This method should be safe to use, as long as we don't touch lazy-loaded collections in the application
                //since the owning session is closed
                Utilify.Platform.Manager.Persistence.Application application = ApplicationDao.GetApplication(id);
                if (application == null)
                    throw new ApplicationNotFoundException(Messages.ApplicationNotFound, applicationId);

                //since the id is needed for checking, we do argument validation before security checks
                // AppId: Check user has access to this app
                SecurityHelper.EnsurePermission(applicationId, EntityType.Application);
                
                //check if it is valid to abort this app:
                if (application.Status != ApplicationStatus.Stopped)
                {
                    //we don't throw an exception for multiple abort calls for a stopped app.
                    //we just ignore such calls.

                    //Issue 013: fire off a thread to send a message to all the executors that are running any jobs that belong to this app
                    //to abort and stop the jobs
                    ApplicationDao.UpdateApplicationStatus(application.Id, ApplicationStatus.Stopped);
                    ApplicationDao.AbortJobs(application.Id);
                }
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
        }

        /// <summary>
        /// Aborts the specified job.  
        /// </summary>
        /// <exception cref="System.ArgumentNullException">jobId is a null reference</exception>
        /// <exception cref="System.ArgumentException">jobId is an empty string or in an invalid format</exception>
        /// <exception cref="System.InvalidOperationException">
        /// The application is already <see cref="F:Utilify.Platform.JobStatus.Completed">Completed</see> or 
        /// <see cref="F:Utilify.Platform.JobStatus.Cancelled">Cancelled</see>
        /// </exception>
        /// <exception cref="Utilify.Platform.JobNotFoundException">A job with the specified id is not found</exception>
        public void AbortJob(string jobId)
        {
            if (jobId == null)
                throw new ArgumentNullException("jobId");

            Guid id = Helper.GetGuidSafe(jobId);
            if (id == Guid.Empty)
                throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "job id", jobId),
                    "jobId");
            try
            {
                Utilify.Platform.Manager.Persistence.Job job = ApplicationDao.GetJob(id);
                if (job == null)
                    throw new JobNotFoundException(Messages.JobNotFound, jobId);

                logger.Debug("Call to Abort Job : " + jobId);

                //since the id is needed for checking, we do argument validation before security checks
                // JobId: Check user has access to this job
                SecurityHelper.EnsurePermission(jobId, EntityType.Job);

                //check if it is valid to abort this job:
                if (job.Status != JobStatus.Completed && job.Status != JobStatus.Cancelled)
                {
                    // This doesn't need to throw an exception if the job was already completed or cancelled before.
                    // We don't want this to throw an exception when somone tries to abort a job that had just finished a second ago

                    //Issue 014: fire off a thread to send a message to the executor that is running the job to abort and stop the job
                    ApplicationDao.AbortJob(job.Id);
                }
            }
            catch (InvalidTransitionException ix)
            {
                logger.Info("Error trying to abort job : " + jobId + "\n" + ix.Message);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
        }

        /// <summary>
        /// Gets a list of unresolved dependencies for the specified application or job
        /// </summary>
        /// <param name="query">an object containing the application or job id to query</param>
        /// <returns>a array of unresolved dependencies</returns>
        /// <exception cref="System.ArgumentNullException">query is a null reference</exception>
        /// <exception cref="System.ArgumentException">The application or job id is null, empty, or in an invalid format</exception>
        /// <exception cref="Utilify.Platform.ApplicationNotFoundException">An application with specified id is not found</exception>
        /// <exception cref="Utilify.Platform.JobNotFoundException">A job with the specified id was not found</exception>
        /// <remarks>
        /// Dependencies of an application or a job are either 'resolved' or 'unresolved'.
        /// A dependency is said to be 'resolved' if the Manager has a copy of the dependency file locally
        /// or if it knows how to get to the file without contacting the client that submits the application (or job).
        /// Dependencies of type <see cref="Utilify.Platform.DependencyType">DependencyType</see>.
        /// <see cref="Utilify.Platform.DependencyType.RemoteUrl">RemoteUrl</see>
        /// are always resolved, since they can be reached using the url.
        /// </remarks>
        /// <seealso cref="Utilify.Platform.DependencyInfo"/>
        public DependencyInfo[] GetUnresolvedDependencies(DependencyQueryInfo query)
        {
            if (query == null)
                throw new ArgumentNullException("query");

            List<DependencyInfo> statuses = new List<DependencyInfo>();
            try
            {
                Guid appOrJobId = Helper.GetGuidSafe(query.ApplicationOrJobId);
                if (appOrJobId == Guid.Empty)
                    throw new ArgumentException(
                        string.Format(Messages.NullEmptyOrInvalidFormat, "application or job id of the query", query.ApplicationOrJobId),
                        "query.ApplicationOrJobId");
                
                bool exists = false;
                List<Dependency> dependencies = new List<Dependency>();
                if (query.Scope == DependencyScope.Application)
                {
                    exists = ApplicationDao.ApplicationExists(appOrJobId);
                    if (!exists)
                        throw new ApplicationNotFoundException(Messages.ApplicationNotFound, query.ApplicationOrJobId);
                    
                    //Check user has access to this app
                    SecurityHelper.EnsurePermission(query.ApplicationOrJobId, EntityType.Application);

                    dependencies = ApplicationDao.GetUnresolvedDependencies(appOrJobId, DependencyScope.Application);
                }
                else if (query.Scope == DependencyScope.Job)
                {
                    exists = ApplicationDao.JobExists(appOrJobId);
                    if (!exists)
                        throw new JobNotFoundException(Messages.JobNotFound, query.ApplicationOrJobId);

                    //Check user has access to this job
                    SecurityHelper.EnsurePermission(query.ApplicationOrJobId, EntityType.Job);

                    dependencies = ApplicationDao.GetUnresolvedDependencies(appOrJobId, DependencyScope.Job);
                }
                //should be safe: since this does not touch any lazy loaded instances.
                statuses = DataStore.Transform<Dependency, DependencyInfo>(dependencies);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
            return statuses.ToArray();
        }

        //Issue 015: We need these types of methods so users can recover Applications and Jobs after failure (ie. client crashes).

        ///// <summary>
        ///// Gets a list of job for the specified application or job
        ///// </summary>
        ///// <exception cref="System.ArgumentNullException">applicationId is a null reference</exception>
        ///// <exception cref="System.ArgumentException">applicationId is an empty string or in an invalid format</exception>
        ///// <exception cref="Utilify.Platform.ApplicationNotFoundException">An application with the specified applicationId is not found</exception>
        //public string[] GetJobsForApplication(string applicationId)
        //{
        //    if (applicationId == null)
        //        throw new ArgumentNullException("applicationId");

        //    Guid id = Helper.GetGuidSafe(applicationId);
        //    if (id == Guid.Empty)
        //        throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "application id", applicationId),
        //            "applicationId");

        //    if (!ApplicationDao.ApplicationExists(id))
        //        throw new ApplicationNotFoundException(Messages.ApplicationNotFound, applicationId);

        //    //ApplicationDao
        //}

        /// <summary>
        /// Pauses the specified application. Adding (submitting) more jobs to a paused application will result in the jobs being suspended for later execution, 
        /// when the application is resumed.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">applicationId is a null reference</exception>
        /// <exception cref="System.ArgumentException">applicationId is an empty string or in an invalid format</exception>
        /// <exception cref="System.InvalidOperationException">
        /// The application is already <see cref="F:Utilify.Platform.ApplicationStatus.Paused">paused</see>
        /// or <see cref="F:Utilify.Platform.ApplicationStatus.Stopped">stopped</see>
        /// </exception>
        /// <exception cref="Utilify.Platform.ApplicationNotFoundException">An application with the specified applicationId is not found</exception>
        public void PauseApplication(string applicationId)
        {

            if (applicationId == null)
                throw new ArgumentNullException("applicationId");

            Guid id = Helper.GetGuidSafe(applicationId);
            if (id == Guid.Empty)
                throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "application id", applicationId),
                    "applicationId");

            try
            {                
                //need to pull out the app object and not just an exists check here, 
                //since we use the app object later to check status etc.
                //This method should be safe to use, as long as we don't touch lazy-loaded collections in the application
                //since the owning session is closed
                Utilify.Platform.Manager.Persistence.Application application = ApplicationDao.GetApplication(id);
                if (application == null)
                    throw new ApplicationNotFoundException(Messages.ApplicationNotFound, applicationId);

                // AppId: Check user has access to this app
                SecurityHelper.EnsurePermission(applicationId, EntityType.Application);

                //check if it is valid to pause this app:
                if (application.Status == ApplicationStatus.Stopped)
                    throw new InvalidOperationException("Cannot pause a stopped application");

                if (application.Status == ApplicationStatus.Paused)
                    throw new InvalidOperationException("Cannot pause a paused application");

                ApplicationDao.UpdateApplicationStatus(application.Id, ApplicationStatus.Paused);

            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
        }

        /// <summary>
        /// Gets the status of the application with the specified id
        /// </summary>
        /// <param name="applicationId">id of the application whose status is required</param>
        /// <returns>an instance of ApplicationStatusInfo representing the application status</returns>
        /// <exception cref="System.ArgumentNullException">applicationId is a null reference</exception>
        /// <exception cref="System.ArgumentException">applicationId is an empty string or in an invalid format</exception>
        /// <exception cref="Utilify.Platform.ApplicationNotFoundException">An application with the specified applicationId is not found</exception>
        public ApplicationStatusInfo GetApplicationStatus(string applicationId)
        {            
            if (applicationId == null)
                throw new ArgumentNullException("applicationId");

            Guid id = Helper.GetGuidSafe(applicationId);
            if (id == Guid.Empty)
                throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "application id", applicationId),
                    "applicationId");

            ApplicationStatusInfo info = null;
            try
            {
                if (!ApplicationDao.ApplicationExists(id))
                    throw new ApplicationNotFoundException(Messages.ApplicationNotFound, applicationId);

                // AppId: Check user has access to this app
                SecurityHelper.EnsurePermission(applicationId, EntityType.Application);

                info = ApplicationDao.GetApplicationStatus(id);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
            return info;
        }

        /// <summary>
        /// Resumes the specified application. This will allow waiting jobs in the application to be eligible for scheduling.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">applicationId is a null reference</exception>
        /// <exception cref="System.ArgumentException">applicationId is an empty string or in an invalid format</exception>
        /// <exception cref="System.InvalidOperationException">
        /// The application is not <see cref="F:Utilify.Platform.ApplicationStatus.Paused">paused</see>
        /// </exception>
        /// <exception cref="Utilify.Platform.ApplicationNotFoundException">An application with the specified applicationId is not found</exception>
        public void ResumeApplication(string applicationId)
        {
            if (applicationId == null)
                throw new ArgumentNullException("applicationId");

            Guid id = Helper.GetGuidSafe(applicationId);
            if (id == Guid.Empty)
                throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "application id", applicationId),
                    "applicationId");
            try
            {
                //need to pull out the app object and not just an exists check here, 
                //since we use the app object later to check status etc.
                //This method should be safe to use, as long as we don't touch lazy-loaded collections in the application
                //since the owning session is closed
                Utilify.Platform.Manager.Persistence.Application application = ApplicationDao.GetApplication(id);
                if (application == null)
                    throw new ApplicationNotFoundException(Messages.ApplicationNotFound, applicationId);
                
                // AppId: Check user has access to this app
                SecurityHelper.EnsurePermission(applicationId, EntityType.Application);
                
                //check if it is valid to resume this app:
                if (application.Status != ApplicationStatus.Paused)
                    throw new InvalidOperationException("Cannot resume the application because it is not in the 'paused' state.");

                //this goes back to Submitted and will eventually be set back to Ready if the scheduler decides it is ready.
                ApplicationDao.UpdateApplicationStatus(application.Id, ApplicationStatus.Submitted);
                
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
        }

        /// <summary>
        /// Submits the specified application to the scheduler for distributed execution
        /// </summary>
        /// <param name="appInfo">the application to run on the network</param>
        /// <returns>id of the newly submitted application</returns>
        /// <exception cref="System.ArgumentNullException">appInfo is a null reference</exception>
        /// <exception cref="System.ArgumentException">One or more properties of the submitted application have invalid values</exception>
        public string SubmitApplication(ApplicationSubmissionInfo appInfo)
        {
            if (appInfo == null)
                throw new ArgumentNullException("appInfo");

            string appId = null;
            try
            {
                //this will validate the other parameters inside an app
                Utilify.Platform.Manager.Persistence.Application application =
                    new Utilify.Platform.Manager.Persistence.Application(appInfo, SecurityHelper.GetCurrentUserName());

                // Need to ensure that user can manage apps
                SecurityHelper.EnsurePermission(
                    PermissionRequirement.Any,
                    Utilify.Platform.Manager.Security.Permission.ManageApplications,
                    Utilify.Platform.Manager.Security.Permission.ManageAllApplications);

                logger.Debug("Call to submit application with info = " + appInfo);
                Guid id = ApplicationDao.SaveApplication(application);
                appId = id.ToString();
                logger.Debug("** Earliest Start Time (appInfo): " + appInfo.Qos.EarliestStartTime.Ticks);
                logger.Debug("** Earliest Start Time (application): " + application.Qos.EarliestStartTime.Ticks);
                logger.Debug("** Current Time : " + DateTime.UtcNow.Ticks);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
            return appId;
        }

        /// <summary>
        /// Sends the contents of the dependency to the manager for use during execution later on.
        /// The dependency is linked to an existing job or application.
        /// </summary>
        /// <param name="dependencies">Dependency contents</param>
        /// <exception cref="System.ArgumentNullException">dependency is a null reference</exception>
        /// <exception cref="System.ArgumentException">
        /// <see cref="P:Utilify.Platform.DependencyContent.DependentId">dependentId</see> is null, empty or in an invalid format
        /// </exception>
        /// <exception cref="Utilify.Platform.ApplicationNotFoundException">
        /// the application referred to 
        /// (with <see cref="Utilify.Platform.DependencyScope">DependencyScope</see>
        /// set to Application) is not found
        /// </exception>
        /// <exception cref="Utilify.Platform.JobNotFoundException">
        /// the job referred to (with <see cref="Utilify.Platform.DependencyScope">DependencyScope</see> set to Job)
        /// is not found
        /// </exception>
        /// <exception cref="System.InvalidOperationException">
        /// if the dependency has already been sent to the server
        /// </exception>
        public void SendDependency(DependencyContent dependency)
        {
            if (dependency == null)
                throw new ArgumentNullException("dependency");

            byte[] obj = dependency.GetContent();
            if (obj == null || obj.Length == 0)
                throw new ArgumentException("Dependency contents cannot be empty");

            Guid depId = Helper.GetGuidSafe(dependency.DependencyId);
            if (depId == Guid.Empty)
                throw new ArgumentException(
                    string.Format(Messages.NullEmptyOrInvalidFormat, "dependency id", dependency.DependencyId), "DependencyId"
                    );
            try
            {
                //no point doing a seperate 'Exists' here, since we're trying to pull the object next anyway:
                Dependency dep = ApplicationDao.GetDependency(depId, FetchStrategy.Default);
                if (dep == null)
                    throw new DependencyNotFoundException(Messages.DependencyNotFound, dependency.DependencyId);

                // DepId: Check user is amongst the names of users that have access to this dependency BOTH Job and App
                SecurityHelper.EnsurePermission(dependency.DependencyId, EntityType.Dependency);

                //restrict if this dependency has already been resolved
                if (dep.IsResolved)
                    throw new InvalidOperationException("The dependency content has already been sent to the server.");

                ApplicationDao.SaveDependencyContent(dependency);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
        }

        /// <summary>
        /// Submits a set of jobs to the manager for distributed execution.
        /// </summary>
        /// <param name="jobInfo">List of jobs to submit</param>
        /// <returns>List of ids for the submitted jobs. Ids WILL be returned in same order they were submitted.</returns>
        /// <exception cref="System.ArgumentNullException">the parameter jobInfo is null</exception>
        /// <exception cref="System.ArgumentException">
        /// jobInfo is an empty array, <br/> 
        /// or the application id of the job is null or empty, <br/>
        /// or the serialized instance of the job is null
        /// </exception>
        /// <exception cref="Utilify.Platform.ApplicationNotFoundException">
        /// the application referred to in 
        /// a job element is not found
        /// </exception>
        public string[] SubmitJobs(JobSubmissionInfo[] jobInfo)
        {
            if (jobInfo == null)
                throw new ArgumentNullException("jobInfo");

            if (jobInfo.Length == 0)
                throw new ArgumentException("List of jobs to submit cannot be empty");

            logger.Debug("SubmitJobs got {0} jobs.", jobInfo.Length);

            List<string> ids = new List<string>();
            try
            {
                //typically this method is called with jobs from the same app.
                //but still we cant assume that - we must validate and verify all the data

                //keep a dictionary of unique apps that we find - to save time.
                Dictionary<Guid, ApplicationStatus> appStatusLookup = new Dictionary<Guid, ApplicationStatus>();
                //keep another list of apps that don't exist: to avoid calling the db each time:
                List<Guid> invalidAppIds = new List<Guid>();

                //keep track of jobs to save
                Dictionary<Guid, List<Utilify.Platform.Manager.Persistence.Job>> jobsToSave = 
                    new Dictionary<Guid, List<Utilify.Platform.Manager.Persistence.Job>>();

                foreach (JobSubmissionInfo jsi in jobInfo)
                {
                    if (jsi == null)
                        throw new ArgumentException("Job cannot be null");

                    if (string.IsNullOrEmpty(jsi.ApplicationId))
                        throw new ArgumentException("Application id cannot be null or empty : " + jsi);
                    if (jsi.GetJobInstance() == null)
                        throw new ArgumentException("Job instance cannot be null : " + jsi);

                    //need to do these steps each time - since appIds may be different for each job.
                    Guid appId = Helper.GetGuidSafe(jsi.ApplicationId);
                    ApplicationStatus appStatus;
                    
                    //lookup our dictionary first
                    if (appStatusLookup.ContainsKey(appId))
                    {
                        //app exists!
                        appStatus = appStatusLookup[appId];
                    }
                    else
                    {
                        //don't have it in the dictionary: 
                        //first check if we already know if this appId is invalid:
                        bool isInvalidId = invalidAppIds.Contains(appId);
                        //now, check in db
                        if (!isInvalidId)
                            isInvalidId = !ApplicationDao.ApplicationExists(appId);

                        //we've checked our local list and the db: we're sure this is invalid now:
                        if (isInvalidId)
                        {
                            if (!invalidAppIds.Contains(appId))
                                invalidAppIds.Add(appId); //put it in out list for later use.

                            throw new ApplicationNotFoundException("Could not find application: " +
                                appId.ToString(), appId.ToString());
                        }
                        appStatus = ApplicationDao.GetApplicationStatus(appId).Status;
                        appStatusLookup[appId] = appStatus; //put it in the lookup table for later use.
                    }

                    //prevent submitting jobs to a stopped app
                    if (appStatus == ApplicationStatus.Stopped)
                        throw new InvalidOperationException("Submittings jobs for an aborted application is not allowed. The application '"
                            + appId.ToString() + "' is already stopped.");

                    //logger.Debug("* Received Job. platform{0}, type:{1}", jsi.ClientPlatform, jsi.JobType);

                    Utilify.Platform.Manager.Persistence.Job job =
                        new Utilify.Platform.Manager.Persistence.Job(jsi);

                    //logger.Debug("* Saving Job. platform:{0}, type:{1}", job.ClientPlatform, job.JobType);
                    if (!jobsToSave.ContainsKey(appId))
                        jobsToSave[appId] = new List<Utilify.Platform.Manager.Persistence.Job>();

                    //if we have permissions on the app, we can submit jobs as part of that app
                    SecurityHelper.EnsurePermission(appId.ToString(), EntityType.Application);

                    jobsToSave[appId].Add(job);                  
                }
                
                //save the jobs - for each app in the lookup
                foreach (Guid appId in jobsToSave.Keys)
                {
                    logger.Debug("Saving {0} jobs for app {1}", jobsToSave[appId].Count, appId);

                    foreach (Utilify.Platform.Manager.Persistence.Job job in jobsToSave[appId])
                    {
                        ids.Add(ApplicationDao.SaveNewJob(appId, job).ToString());                        
                    }
                }

                logger.Debug("Saved all jobs");
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
            return ids.ToArray();
        }

        /// <summary>
        /// Gets the status of the list of jobs with the specified ids.
        /// </summary>
        /// <param name="jobIds">an array of the ids of the jobs whose status is required</param>
        /// <returns>Array of JobStatusInfo objects that contain the status of the jobs</returns>
        /// <exception cref="System.ArgumentNullException">jobIds in a null reference</exception>
        /// <exception cref="System.ArgumentException">jobIds is an empty list, or the elements in the list are null, empty or in an invalid format</exception>
        /// <exception cref="Utilify.Platform.JobNotFoundException">An element of the jobIds list, refers to a non-existent job</exception>
        public JobStatusInfo[] GetJobStatus(string[] jobIds)
        {
            if (jobIds == null)
                throw new ArgumentNullException("jobIds");
            if (jobIds.Length == 0)
                throw new ArgumentException("The list of job ids to query cannot be empty", "jobIds");

            List<Utilify.Platform.Manager.Persistence.Job> jobs = new List<Utilify.Platform.Manager.Persistence.Job>();

            JobStatusInfo[] statuses = null;
            try
            {
                List<Guid> jobGuids = new List<Guid>();
                foreach (string id in jobIds)
                {
                    Guid jobGuid = Helper.GetGuidSafe(id);
                    if (jobGuid == Guid.Empty)
                        throw new ArgumentException("Elements of the job id list cannot be null, empty or invalid");

                    if (!ApplicationDao.JobExists(jobGuid))
                        throw new JobNotFoundException(Messages.JobNotFound, id);

                    jobGuids.Add(jobGuid);
                }

                // Check if user has access to these jobs
                SecurityHelper.EnsurePermission(jobIds, EntityType.Job);

                //don't eager load instances: since we don't need the job instances here.
                jobs = ApplicationDao.GetJobs(jobGuids, FetchStrategy.Lazy);

                //should be safe: since this doesn't touch any lazy loaded instances.
                statuses =
                    DataStore.Transform<Utilify.Platform.Manager.Persistence.Job, JobStatusInfo>(jobs).ToArray();
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
            return statuses;
        }

        /// <summary>
        /// Gets a list of results expected for the specified job
        /// </summary>
        /// <param name="jobId">id of the job for which results are required</param>
        /// <returns>Array of ResultStatusInfo instances containing the information about expected results</returns>
        /// <exception cref="System.ArgumentNullException">jobId in a null reference</exception>
        /// <exception cref="System.ArgumentException">jobId is null, empty or in an invalid format</exception>
        /// <exception cref="Utilify.Platform.JobNotFoundException">jobId refers to a non-existent job</exception>
        public ResultStatusInfo[] GetJobResults(string jobId)
        {
            if (jobId == null)
                throw new ArgumentNullException("jobId");

            Guid jobGuid = Helper.GetGuidSafe(jobId);
            if (jobGuid == Guid.Empty)
                throw new ArgumentException(Messages.NullEmptyOrInvalidFormat, "jobId");

            ResultStatusInfo[] statuses = null;
            try
            {
                if (!ApplicationDao.JobExists(jobGuid))
                    throw new JobNotFoundException(Messages.JobNotFound, jobId);

                // JobId: Check user has access to this job
                SecurityHelper.EnsurePermission(jobId, EntityType.Job);

                List<Result> results = ApplicationDao.GetResults(jobGuid);
                List<ResultStatusInfo> resultInfos = DataStore.Transform<Result, ResultStatusInfo>(results);
                statuses = resultInfos.ToArray();
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
            return statuses;
        }

        public ResultContent GetResult(string resultId) //todoDoc
        {
            if (resultId == null)
                throw new ArgumentNullException("resultId");
            
            Guid resultGuid = Helper.GetGuidSafe(resultId);
            if (resultGuid == Guid.Empty)
                throw new ArgumentException(Messages.NullEmptyOrInvalidFormat, "resultId");

            ResultContent content = null;
            try
            {
                if (!ApplicationDao.ResultExists(resultGuid))
                    throw new ResultNotFoundException(Messages.ResultNotFound, resultId);

                // ResultId: Check user has access to this result
                SecurityHelper.EnsurePermission(resultId, EntityType.Result);

                // todoFindOut: Without Eager Fetch the call to result.GetContent() hangs and never returns. not sure why.
                Result result = ApplicationDao.GetResult(resultGuid, FetchStrategy.Eager);
                content = new ResultContent(result.Id.ToString(), result.JobId.GetValueOrDefault().ToString(), result.GetContent());
                logger.Debug("Sending result content {0} {1}", resultId, content.GetContent());
                // The content inside the ResultContent may be null but we still want to send an instance of the ResultCOntent.
            }
            catch (PersistenceException px)
            {

                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
            return content;
        }

        public JobCompletionInfo GetCompletedJob(string jobId)
        {
            if (jobId == null)
                throw new ArgumentNullException("jobId");

            Guid jobGuid = Helper.GetGuidSafe(jobId);
            if (jobGuid == Guid.Empty)
                throw new ArgumentException(Messages.NullEmptyOrInvalidFormat, "jobId");

            JobCompletionInfo info = null;
            try
            {
                if (!ApplicationDao.JobExists(jobGuid))
                    throw new JobNotFoundException(Messages.JobNotFound, jobId);

                // JobId: Check user has access to this Job
                SecurityHelper.EnsurePermission(jobId, EntityType.Job);

                //todoTest : what do we do if status is not completed / cancelled?
                Utilify.Platform.Manager.Persistence.Job job = ApplicationDao.GetJob(jobGuid, FetchStrategy.Eager);
                if (job.Status == JobStatus.Completed || job.Status == JobStatus.Cancelled)
                {
                    info = (job as ITransformable<JobCompletionInfo>).Transform();
                }
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }

            return info;
        }

        #endregion

    }
}
