using System;
using System.Diagnostics;


using Utilify.Platform.Contract;
using Utilify.Platform.Manager.Persistence;
using Utilify.Platform.Manager.Security;

namespace Utilify.Platform.Manager
{
    /*
     * We implement an interface to share the code between the client and the service.
     */
    /// <summary>
    /// 
    /// </summary>
#if !MONO
    [ErrorBehavior(typeof(ServiceErrorHandler))] //attach the error handling behaviour which does Exception to Fault mapping
    [System.ServiceModel.ServiceBehavior(
        Namespace = Namespaces.Manager,
        InstanceContextMode = System.ServiceModel.InstanceContextMode.Single,
        ConcurrencyMode = System.ServiceModel.ConcurrencyMode.Multiple,
        AddressFilterMode = System.ServiceModel.AddressFilterMode.Any)]
#endif
    internal class ResourceMonitor : MarshalByRefObject, IResourceMonitor
    {
        private Logger logger = new Logger();

        //If we are using anything other than WCF - we don't have the luxury of the 'ManagerErrorHandler' to prevent errors from flowing to
        //the client. so things like a PersistenceException will try to go through a remoting channel, for example,
        //and fail to de-serialize at the other end, since the client won't have the Manager assembly with them - where PersistenceException is declared.
        //To overcome that, we explicitly handle all persistence exceptions and convert them into InternalServerException-s.

        #region IManagerService Members

        public void CheckService() { }

        #endregion

        #region IResourceMonitor Members

        /// <summary>
        /// Pings the manager.
        /// </summary>
        /// <param name="id"></param>
        public void Ping(string executorId)
        {
            // Ensure user has permissions to Execute jobs
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ExecuteJob);

            if (executorId == null)
                throw new ArgumentNullException();
            if (String.IsNullOrEmpty(executorId))
                throw new ArgumentException("Executor Id cannot be empty", "executorId");

            try
            {
                bool exists = SystemInfoDao.ExecutorExists(Helper.GetGuidSafe(executorId));
                if (!exists)
                    throw new ExecutorNotFoundException(Messages.ExecutorNotFound, "executorId");

                SystemInfoDao.UpdateLastPingTime(executorId);
            }
            catch (PersistenceException px)
            {
                throw new InternalServerException(Messages.InternalServerError, px);
            }
        }

        /// <summary>
        /// Registers the executor with the Manager.
        /// </summary>
        /// <param name="info"></param>
        /// <returns>
        /// - If SystemInfo contains an existing Id the same Id will be returned to the Executor
        /// - If SystemInfo contains no Id or one that is unknown, the Manager will generate a new Id and register the Executor.
        /// </returns>
        public string Register(SystemInfo info)
        {
            // Ensure user has permissions to Execute jobs
            SecurityHelper.EnsurePermission(PermissionRequirement.All, 
                Utilify.Platform.Manager.Security.Permission.ExecuteJob);

            if (info == null)
                throw new ArgumentNullException("info");

            string id = null;

            try
            {
                // register the executor if it hasnt been registered already
                Guid execId = Helper.GetGuidSafe(info.Id);
                bool exists = SystemInfoDao.ExecutorExists(execId);

                Utilify.Platform.Manager.Persistence.Executor exec = null;

                if (exists) //check hostname
                {
                    //get it from the db to compare the id and url
                    exec = SystemInfoDao.GetExecutor(execId, FetchStrategy.Lazy); //we only need the id and url here.

                    //if the hostname's don't match, let's assume the id is wrong - the exec does NOT exist:
                    //we register a new one. This situation can happen in cloud/VM based environments.
                    if (!exec.Uri.Equals(info.Uri, StringComparison.CurrentCultureIgnoreCase))
                        exists = false;
                }

                //whether it exists or not, we still need to validate the inputs
                // and/or update the values: so create the 'exec' object again from the input
                // this will validate the other parameters inside the system info
                exec = new Utilify.Platform.Manager.Persistence.Executor(info, SecurityHelper.GetCurrentUserName());

                //reset the input id, since the executor 'thinks' its got a valid id, 
                //but it does not - according to the manager. The manager wins.
                //this can happen if the executor hostname has changed for example: in virtual environments.
                if (!exists)
                    exec.ClearId();

                if (!exists || (info.Id == null || info.Id.Trim().Length == 0))
                {
                    logger.Debug("Call to register NEW executor with info = " + info);
                    exec.LastPingTime = DateTime.UtcNow;
                    Guid newId = SystemInfoDao.RegisterExecutor(exec);
                    id = newId.ToString();
                }
                else
                {
                    logger.Debug("Call to register EXISTING executor with info = " + info);
                    // This executor already exists. Just return the existing Id back.
                    id = info.Id;
                    SystemInfoDao.Update(execId, info);
                }
            }
            catch (PersistenceException px)
            {
                logger.Debug("COULD NOT REGISTER EXECUTOR: ", px);
                throw new InternalServerException(Messages.InternalServerError, px);
            }

            return id;
        }

        // todoDiscuss : (UpdatePerformance()) As well as having an 'update performance' called by the Executor, we should probably also have
        //        the Executor send an updated System info via an 'update system'. This will allow us to update any dynamic changes to
        //        the Executor.

        /// <summary>
        /// Updates the performance information for an Executor
        /// </summary>
        /// <param name="info"></param>
        public void UpdatePerformance(PerformanceInfo info)
        {
            // Ensure user has permissions to Execute jobs
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ExecuteJob);

            if (info == null)
                throw new ArgumentNullException("info");

            if (String.IsNullOrEmpty(info.SystemId))
                throw new ArgumentNullException("info.SystemId","Cannot update performance. No Id was specified.");

            if (!SystemInfoDao.ExecutorExists(Helper.GetGuidSafe(info.SystemId)))
                throw new ExecutorNotFoundException(Messages.ExecutorNotFound, "info.SystemId");

            try
            {
                SystemInfoDao.UpdatePerformance(info);
                //logger.Debug("Call to Update Executor Performance with info : " + info);
            }
            catch (PersistenceException px)
            {
                string msg = "Trying to update perf stats: " + px.Message;
                if (px.InnerException != null)
                    msg += " >> " + px.InnerException.Message;
                logger.Warn(msg);
                //throw new InternalServerException(Messages.InternalServerError, px);
            }
        }

        // Not implemented/tested
        public void UpdateLimits(LimitInfo info)
        {
            //// Ensure user has permissions to Execute jobs
            //SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ExecuteJob);

            logger.Debug("Call to update limits: not implemented");

//            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ExecuteJob);

//            if (info == null)
//                throw new ArgumentNullException("info");

//            if (String.IsNullOrEmpty(info.SystemId))
//                throw new ArgumentNullException("info.SystemId", "Cannot update limits. No Id was specified.");

//            if (!SystemInfoDao.ExecutorExists(Helper.GetGuidSafe(info.SystemId)))
//                throw new ExecutorNotFoundException(Messages.ExecutorNotFound, "info.SystemId");
//            try
//            {
//                SystemInfoDao.UpdateLimits(info);
//                logger.Debug("Call to Update Executor Limits with info : " + info);
//            }
//            catch (PersistenceException px)
//            {
//                logger.Warn(px.ToString());
//#if DEBUG
//                throw new InternalServerException(px.ToString());
//#else
//                throw new InternalServerException(Messages.InternalServerError);
//#endif
//            }
        }

        #endregion
    }
}

