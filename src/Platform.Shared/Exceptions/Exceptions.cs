using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Utilify.Platform
{
    //Reference for implementations of ISerializable GetObjectData:
    //http://msdn2.microsoft.com/en-au/ms182342.aspx

    /// <summary>
    /// The exception that is thrown when a general platform error occurs. Most of the exceptions thrown by the framework are derived from
    /// the PlatformException.
    /// </summary>
    [Serializable]
    public class PlatformException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlatformException" /> class.
        /// </summary>
        public PlatformException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="PlatformException" /> class 
        /// with the specified error message..
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        public PlatformException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="PlatformException" /> class 
        /// with the specified error message and inner exception that references the cause of this exception.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="innerException"></param>
        public PlatformException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="PlatformException" /> class with
        /// the serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        protected PlatformException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    /// <summary>
    /// The exception that is thrown when an error occurs during communication between a service and a client.
    /// </summary>
    [Serializable]
    public class CommunicationException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommunicationException">CommunicationException</see> class.
        /// </summary>
        public CommunicationException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="CommunicationException">CommunicationException</see> class 
        /// with the specified error message.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        public CommunicationException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="CommunicationException">CommunicationException</see> class 
        /// with the specified error message and inner exception that references the cause of this exception.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public CommunicationException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Initializes a new instance of <see cref="CommunicationException">CommunicationException</see> with
        /// the serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        protected CommunicationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    /// <summary>
    /// The exception that is thrown when an unexpected error occurs in a service.
    /// </summary>
    [Serializable]
    public class InternalServerException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InternalServerException">InternalServerException</see> class.
        /// </summary>
        public InternalServerException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="InternalServerException">InternalServerException</see> class 
        /// with the specified error message.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        public InternalServerException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="InternalServerException">InternalServerException</see> class 
        /// with the specified error message and inner exception that references the cause of this exception.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public InternalServerException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Initializes a new instance of <see cref="InternalServerException">InternalServerException</see> with
        /// the serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        protected InternalServerException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    /// <summary>
    /// The exception that is thrown when a job state is set to an invalid value for its current context.
    /// </summary>
    [Serializable]
    public class InvalidTransitionException : PlatformException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidTransitionException">InvalidTransitionException</see> class.
        /// </summary>
        public InvalidTransitionException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidTransitionException">InvalidTransitionException</see> class 
        /// with the specified error message.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        public InvalidTransitionException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidTransitionException">InvalidTransitionException</see> class 
        /// with the specified error message and inner exception that references the cause of this exception.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public InvalidTransitionException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Initializes a new instance of <see cref="InvalidTransitionException">InvalidTransitionException</see> with
        /// the serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        protected InvalidTransitionException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
    
    /// <summary>
    /// The exception that is thrown when the application could not be initialized on the remote service for any reason.
    /// </summary>
    [Serializable]
    public class ApplicationInitializationException : PlatformException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationInitializationException">ApplicationInitializationException</see> class.
        /// </summary>
        public ApplicationInitializationException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationInitializationException">ApplicationInitializationException</see> class 
        /// with the specified error message.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        public ApplicationInitializationException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationInitializationException">ApplicationInitializationException</see> class 
        /// with the specified error message and inner exception that references the cause of this exception.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public ApplicationInitializationException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Initializes a new instance of <see cref="ApplicationInitializationException">ApplicationInitializationException</see> with
        /// the serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        protected ApplicationInitializationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    /// <summary>
    /// The exception that is thrown when an application with the specified Id is not found.
    /// </summary>
    [Serializable]
    public class ApplicationNotFoundException : PlatformException 
    {
        private string applicationId;

        /// <summary>
        /// Initializes a new instance of <see cref="ApplicationNotFoundException">ApplicationNotFoundException</see> with the 
        /// specified error message and application Id.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="applicationId">The Id of the application that could not be found.</param>
        public ApplicationNotFoundException(string message, string applicationId)
            : this (message)
        {
            this.applicationId = applicationId;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationNotFoundException">ApplicationNotFoundException</see> class.
        /// </summary>
        public ApplicationNotFoundException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationNotFoundException">ApplicationNotFoundException</see> class
        /// with the specified error message.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        public ApplicationNotFoundException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationNotFoundException">ApplicationNotFoundException</see> class
        /// with the specified error message and inner exception that references the cause of this exception.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public ApplicationNotFoundException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Initializes a new instance of <see cref="ApplicationNotFoundException">ApplicationNotFoundException</see> with
        /// the serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        protected ApplicationNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) 
        {
            if (info != null)
                applicationId = info.GetString("ApplicationId");
        }

        /// <summary>
        /// Gets the application id value that could not be found
        /// </summary>
        public string ApplicationId
        {
            get
            {
                return applicationId;
            }
        }

        /// <summary>
        /// See <see cref="M:System.Exception.GetObjectData">Exception.GetObjectData</see>
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //need to override this since this class adds a new field. (and one of the classes in the heirarchy implements ISerializable
            if (info == null)
                throw new ArgumentNullException("info");

            info.AddValue("ApplicationId", applicationId);
            base.GetObjectData(info, context);
        }
    }

    //use this wherever invalid dependency ids are passed and the dependency is not found
    /// <summary>
    /// The exception that is thrown when a dependency with the specified Id is not found.
    /// </summary>
    [Serializable]
    public class DependencyNotFoundException : PlatformException
    {
        private string dependencyId;

        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyNotFoundException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="dependencyId">The dependency id.</param>
        public DependencyNotFoundException(string message, string dependencyId)
            : this (message)
        {
            this.dependencyId = dependencyId;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyNotFoundException"/> class.
        /// </summary>
        public DependencyNotFoundException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyNotFoundException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public DependencyNotFoundException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyNotFoundException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public DependencyNotFoundException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyNotFoundException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// The <paramref name="info"/> parameter is null.
        /// </exception>
        /// <exception cref="T:System.Runtime.Serialization.SerializationException">
        /// The class name is null or <see cref="P:System.Exception.HResult"/> is zero (0).
        /// </exception>
        protected DependencyNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) 
        {
            if (info != null)
                dependencyId = info.GetString("DependencyId");
        }

        /// <summary>
        /// Gets the dependency id value that could not be found
        /// </summary>
        public string DependencyId
        {
            get
            {
                return dependencyId;
            }
        }

        /// <summary>
        /// See <see cref="M:System.Exception.GetObjectData">Exception.GetObjectData</see>
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //need to override this since this class adds a new field. (and one of the classes in the heirarchy implements ISerializable
            if (info == null)
                throw new ArgumentNullException("info");

            info.AddValue("DependencyId", dependencyId);
            base.GetObjectData(info, context);
        }
    }

    //use this wherever invalid job ids are passed and the job is not found
    /// <summary>
    /// The exception that is thrown when a job with the specified Id is not found.
    /// </summary>
    [Serializable]
    public class JobNotFoundException : PlatformException
    {
        private string jobId;

        /// <summary>
        /// Initializes a new instance of the <see cref="JobNotFoundException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="jobId">The job id.</param>
        public JobNotFoundException(string message, string jobId)
            : this(message)
        {
            this.jobId = jobId;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="JobNotFoundException"/> class.
        /// </summary>
        public JobNotFoundException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="JobNotFoundException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public JobNotFoundException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="JobNotFoundException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public JobNotFoundException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="JobNotFoundException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// The <paramref name="info"/> parameter is null.
        /// </exception>
        /// <exception cref="T:System.Runtime.Serialization.SerializationException">
        /// The class name is null or <see cref="P:System.Exception.HResult"/> is zero (0).
        /// </exception>
        protected JobNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            if (info != null)
                jobId = info.GetString("JobId");
        }

        /// <summary>
        /// Gets the job id value that could not be found
        /// </summary>
        public string JobId
        {
            get
            {
                return jobId;
            }
        }

        /// <summary>
        /// See <see cref="M:System.Exception.GetObjectData">Exception.GetObjectData</see>
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //need to override this since this class adds a new field. (and one of the classes in the heirarchy implements ISerializable
            if (info == null)
                throw new ArgumentNullException("info");

            info.AddValue("JobId", jobId);
            base.GetObjectData(info, context);
        }
    }

    //used when invalid result ids are passed to get the result content and the result with the given id is not found
    /// <summary>
    /// The exception that is thrown when the result with the specified Id is not found.
    /// </summary>
    [Serializable]
    public class ResultNotFoundException : PlatformException
    {
        private string resultId;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResultNotFoundException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="resultId">The result id.</param>
        public ResultNotFoundException(string message, string resultId)
            : this(message)
        {
            this.resultId = resultId;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ResultNotFoundException"/> class.
        /// </summary>
        public ResultNotFoundException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ResultNotFoundException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public ResultNotFoundException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ResultNotFoundException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public ResultNotFoundException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ResultNotFoundException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// The <paramref name="info"/> parameter is null.
        /// </exception>
        /// <exception cref="T:System.Runtime.Serialization.SerializationException">
        /// The class name is null or <see cref="P:System.Exception.HResult"/> is zero (0).
        /// </exception>
        protected ResultNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) 
        {
            if (info != null)
                resultId = info.GetString("ResultId");
        }

        /// <summary>
        /// Gets the result id value that could not be found
        /// </summary>
        public string ResultId
        {
            get
            {
                return resultId;
            }
        }

        /// <summary>
        /// See <see cref="M:System.Exception.GetObjectData">Exception.GetObjectData</see>
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //need to override this since this class adds a new field. (and one of the classes in the heirarchy implements ISerializable
            if (info == null)
                throw new ArgumentNullException("info");

            info.AddValue("ResultId", resultId);
            base.GetObjectData(info, context);
        }
    }

    /// <summary>
    /// The exception that is thrown when a job could not be submitted to the remote manager.
    /// </summary>
    [Serializable]
    public class JobSubmissionException : PlatformException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JobSubmissionException">JobSubmissionException</see> class.
        /// </summary>
        public JobSubmissionException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="JobSubmissionException">JobSubmissionException</see> class
        /// with the specified error message.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        public JobSubmissionException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="JobSubmissionException">JobSubmissionException</see> class
        /// with the specified error message and inner exception that references the cause of this exception.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public JobSubmissionException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Initializes a new instance of <see cref="JobSubmissionException">JobSubmissionException</see> with
        /// the serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        protected JobSubmissionException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}