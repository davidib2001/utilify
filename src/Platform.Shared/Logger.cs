using System;
using System.Diagnostics;

namespace Utilify.Platform
{

    /// <summary>
    /// Represents the logger used to log messages and exceptions.
    /// </summary>
    [Serializable]
    public class Logger : MarshalByRefObject
    {
        //todo: how do we fix the memory leak with static events?
        //have a private static class and attach the event to that class instance!?
        /// <summary>
        /// Occurs when a message is logged.
        /// </summary>
        public static event EventHandler<LogEventArgs> MessageLogged;

        /// <summary>
        /// Gets the log level for this logger.
        /// </summary>
        public LogLevel Level { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logger"/> class.
        /// </summary>
        public Logger() 
        {
//#if DEBUG
            Level = LogLevel.Debug; //set it to Debug here, and in the config, control the level using the real logger library
//#else
//            Level = LogLevel.Info;
//#endif
        }

        //Issue 053: need to have a look at this class. performance and usability wise.
        //there seems to be a problem with the Debug("{0}", exception) and Debug("message", exception) methods - getting mixed up.

        #region Debug Logging

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        //[Conditional("DEBUG")]
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
        public void Debug(string message)
        {
            RaiseEvent(message, LogLevel.Debug, null, null);
        }
        
        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageParams">The message params.</param>
        //[Conditional("DEBUG")]
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
        public void Debug(string message, params object[] messageParams)
        {
            RaiseEvent(message, LogLevel.Debug, null, messageParams);
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The exception to log.</param>
        //[Conditional("DEBUG")]
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
        public void Debug(string message, Exception ex)
        {
            RaiseEvent(message, LogLevel.Debug, ex, null);
        }

        #endregion

        #region Info Logging

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
        public void Info(string message)
        {
            RaiseEvent(message, LogLevel.Info, null, null);
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageParams">The message params.</param>
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
        public void Info(string message, params object[] messageParams)
        {
            RaiseEvent(message, LogLevel.Info, null, messageParams);
        }

        #endregion

        #region Warn Logging

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
        public void Warn(string message)
        {
            RaiseEvent(message, LogLevel.Warn, null, null);
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageParams">The message params.</param>
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
        public void Warn(string message, params object[] messageParams)
        {
            RaiseEvent(message, LogLevel.Warn, null, messageParams);
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The exception to log.</param>
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
        public void Warn(string message, Exception ex)
        {
            RaiseEvent(message, LogLevel.Warn, ex, null);
        }

        #endregion

        #region Error Logging

        /// <summary>
        /// Logs the specified error message.
        /// </summary>
        /// <param name="message">The message.</param>
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
        public void Error(string message)
        {
            RaiseEvent(message, LogLevel.Error, null, null);
        }

        /// <summary>
        /// Logs the specified error message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageParams">The message params.</param>
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
        public void Error(string message, params object[] messageParams)
        {
            RaiseEvent(message, LogLevel.Error, null, messageParams);
        }

        /// <summary>
        /// Logs the specified error message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The exception.</param>
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
        public void Error(string message, Exception ex)
        {
            RaiseEvent(message, LogLevel.Error, ex, null);
        }

        #endregion

        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
        private void RaiseEvent(string message, LogLevel level, Exception ex, params object[] messageFormattingParams)
        {
            if (this.Level > level)
                return; //simply skip

            string finalMessage = (messageFormattingParams == null) ? message : string.Format(message, messageFormattingParams);
//#if DEBUG
            StackFrame frame = new StackFrame(2, true);
            LogEventArgs e = new LogEventArgs(finalMessage, level, ex, frame);
//#else
            //in non-debug mode, we can't get a meaningful stack-frame with obfuscated type names anyway.
            //also, we don't want to reduce performance by getting stack frames with every logging call.
            //LogEventArgs e = new LogEventArgs(finalMessage, level, ex);
//#endif
            OnMessageLogged(this, e); //Issue 053: check if the performance of this thing is ok. we could try AyncEvents / Tracing
        }

        private void OnMessageLogged(object sender, LogEventArgs e)
        {
            EventHelper.RaiseEvent<LogEventArgs>(MessageLogged, sender, e);
        }
    }

    /// <summary>
    /// Represents the log levels used by the Logger.
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        ///  Represents the Debug level
        /// </summary>
        Debug = -1,
        /// <summary>
        /// Represents the Info level
        /// </summary>
        Info = 0,
        /// <summary>
        ///  Represents the Warn level
        /// </summary>
        Warn = 1,
        /// <summary>
        ///  Represents the Error level
        /// </summary>
        Error = 2
    }

    /// <summary>
    /// Represents the event arguments for the log event.
    /// </summary>
    [Serializable]
    public class LogEventArgs : EventArgs
    {
        private LogLevel level;
        private string message;
        private Exception exception;


        /// <summary>
        /// Initializes a new instance of the <see cref="LogEventArgs"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="level">The level.</param>
        /// <param name="exception">The exception.</param>
        public LogEventArgs(string message, LogLevel level, Exception exception)
        {
            this.message = message;
            this.level = level;
            this.exception = exception;
        }

        //For non-debug builds, we don't want the stack frame
//#if DEBUG
        private StackFrame stackFrame;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogEventArgs"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="level">The level.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="frame">The frame.</param>
        public LogEventArgs(string message, LogLevel level, Exception exception, StackFrame frame)
            : this(message, level, exception)
        {
            this.stackFrame = frame;
        }

        /// <summary>
        /// Gets the stack frame.
        /// </summary>
        /// <value>The stack frame.</value>
        public StackFrame StackFrame
        {
            get { return stackFrame; }
        }
//#endif

        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message
        {
            get { return message; }
        }

        /// <summary>
        /// Gets the level.
        /// </summary>
        /// <value>The level.</value>
        public LogLevel Level
        {
            get { return level; }
        }

        /// <summary>
        /// Gets the exception.
        /// </summary>
        /// <value>The exception.</value>
        public Exception Exception
        {
            get { return exception; }
        }

    }

}
