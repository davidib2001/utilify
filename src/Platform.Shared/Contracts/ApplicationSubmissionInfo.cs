using System;
using System.Net;
using System.Collections.Generic;

#if !MONO
using System.Runtime.Serialization;

#endif

namespace Utilify.Platform
{
    /// <summary>
    /// Represents the information required to submit an application to the application manager service.
    /// An instance of this class is used to transfer application information when submitting a new application.
    /// </summary>
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class ApplicationSubmissionInfo
    {
        private string id;
        private string applicationName;
        private string clientHost;
        private DependencyInfo[] dependencies;
        private QosInfo qos;

        private ApplicationSubmissionInfo() { }
        
        //Issue 021: enable app security : how to pass credentials. etc. may effect these ctors.
        // Removed 'username' from constructors because security code determines the username. Real internal username is passed to the
        // Persistence.Application along with the ApplicationSubmissionInfo.

        // todoLater: Realise now that on the Executor side (because we send ApplicationSubmissionInfo to the Executor) we don't really know which
        // user we are executing the job for since we've removed the 'username' field. We should probably create an ApplicationExecutionInfo
        // and JobExecutionInfo to be sent to the Executor.

        /// <summary>
        /// Creates an instance of the ApplicationSubmissionInfo class with the given parameters
        /// </summary>
        /// <param name="applicationName">Name of the submitted application</param>
        public ApplicationSubmissionInfo(string applicationName)
            : this(applicationName, null, QosInfo.GetDefault())
        { }

        /// <summary>
        /// Creates an instance of the ApplicationSubmissionInfo class with the given parameters
        /// </summary>
        /// <param name="applicationName">Name of the submitted application</param>
        /// <param name="dependencies">Dependencies for the application</param>
        public ApplicationSubmissionInfo(string applicationName, 
            IEnumerable<DependencyInfo> dependencies) 
            : this(applicationName, dependencies, QosInfo.GetDefault())
        { }

        /// <summary>
        /// Creates an instance of the ApplicationSubmissionInfo class with the given parameters
        /// </summary>
        /// <param name="applicationName">Name of the submitted application</param>
        /// <param name="dependencies">Dependencies for the application</param>
        /// <param name="qos">Quality of service parameters required for the application to run</param>
        public ApplicationSubmissionInfo(string applicationName, 
            IEnumerable<DependencyInfo> dependencies, QosInfo qos) : this()
        {
            this.Id = string.Empty; //to ensure it is not a null string

            this.ApplicationName = applicationName;
            this.ClientHost = Dns.GetHostName();
            this.Qos = qos;

            this.dependencies = Helper.GetCopy(dependencies);
        }

        // Internal use only: for communication between the manager and the executor.
        /// <summary>
        /// Creates an instance of the ApplicationSubmissionInfo class with the given parameters.
        /// This constructor is for internal use only
        /// </summary>
        /// <param name="id">id of the submitted application</param>
        /// <param name="applicationName">Name of the submitted application</param>
        /// <param name="dependencies">Dependencies for the application</param>
        /// <param name="qos">Quality of service parameters required for the application to run</param>
        public ApplicationSubmissionInfo(string id, string applicationName, 
            IEnumerable<DependencyInfo> dependencies, QosInfo qos)
            : this(applicationName, dependencies, qos)
        {
            //the Id itself can be not-set when submitting the app.
            //but when using this ctor, it needs to be valid.
            //so in this case, we do the check in the ctor as opposed to in the property setter (which was done for other properties).
            if (id == null)
                throw new ArgumentNullException("id");
            if (id.Trim().Length == 0)
                throw new ArgumentException("Application id cannot be empty.", "id");

            this.Id = id;
        }

        /// <summary>
        /// Gets the id of the application
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Id
        {
            get { return id; }
            private set { id = value ?? string.Empty; }
        }

        /// <summary>
        /// Gets the name of the application submitted
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ApplicationName
        {
            get { return applicationName; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("applicationName");

                if (value.Trim().Length == 0)
                    throw new ArgumentException("Application name cannot be empty");

                applicationName = value;
            }
        }

        /// <summary>
        /// Gets the host name of the client that submitted the application
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ClientHost
        {
            get { return clientHost; }
            private set 
            {
                if (value == null)
                    throw new ArgumentNullException("clientHost", "Client hostname cannot be null");

                if (value.Trim().Length == 0)
                    throw new ArgumentException("Client hostname cannot be empty");
                
                clientHost = value; 
            }
        }

        /// <summary>
        /// Gets the dependencies of the application
        /// </summary>
        public DependencyInfo[] GetDependencies()
        {
            return dependencies;
        }

#if !MONO
        [DataMember]
#endif
        private DependencyInfo[] Dependencies
        {
            get { return dependencies; }
            set 
            { 
                dependencies = Helper.GetCopy(value);
            }
        }

        /// <summary>
        /// Gets the QoS requirements for the  application. 
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public QosInfo Qos
        {
            get { return qos; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("qos", "Qos cannot be null");

                qos = value;
            }
        }
    }
}