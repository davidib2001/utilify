using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform
{

#if !MONO
    /// <summary>
    /// Specifies the type of entity, when describing an <see cref="C:Utilify.Platform.EntityNotFoundException"/> that occurs in the system.
    /// </summary>
    [DataContract]
#endif
    public enum EntityType
    {
#if !MONO
        /// <summary>
        /// Represents an application entity.
        /// </summary>
        [EnumMember]
#endif
        Application = 0,
#if !MONO
        /// <summary>
        /// Represents a job entity.
        /// </summary>
        [EnumMember]
#endif
        Job = 1,
#if !MONO
        /// <summary>
        /// Represents a dependency entity.
        /// </summary>
        [EnumMember]
#endif
        Dependency = 2,
#if !MONO
        /// <summary>
        /// Represents a result entity.
        /// </summary>
        [EnumMember]
#endif
        Result = 3,
#if !MONO
        /// <summary>
        /// Represents an executor entity.
        /// </summary>
        [EnumMember]
#endif
        Executor = 4,
#if !MONO
        /// <summary>
        /// Represents a user entity.
        /// </summary>
        [EnumMember]
#endif
        User = 5,
#if !MONO
        /// <summary>
        /// Represents a group entity.
        /// </summary>
        [EnumMember]
#endif
        Group = 6,
#if !MONO
        /// <summary>
        /// Represents a file entity.
        /// </summary>
        [EnumMember]
#endif
        File = 7,
#if !MONO
        /// <summary>
        /// Represents a directory entity.
        /// </summary>
        [EnumMember]
#endif
        Directory = 8
    }

#if !MONO
    /// <summary>
    /// Specifies the status of an application.
    /// </summary>
	[DataContract]
#endif
	public enum ApplicationStatus
    {
#if !MONO
        /// <summary>
        /// The application is not yet initialised.
        /// </summary>
        [EnumMember]
#endif
        UnInitialized = 0,
#if !MONO
        /// <summary>
        /// The application is submitted to the service.
        /// </summary>
        [EnumMember]
#endif
        Submitted = 1,
#if !MONO
        /// <summary>
        /// The application is ready to be scheduled.
        /// </summary>
        [EnumMember]
#endif
        Ready = 2,
#if !MONO
        /// <summary>
        /// The application is paused.
        /// </summary>
        [EnumMember]
#endif
        Paused = 3,
#if !MONO
        /// <summary>
        /// The application is stopped.
        /// </summary>
        [EnumMember]
#endif
        Stopped = 4
    }

#if !MONO
    /// <summary>
    /// Specifies the status of a job.
    /// </summary>
	[DataContract]
#endif
	public enum JobStatus
    {
#if !MONO
        /// <summary>
        /// The job is not yet initialised.
        /// </summary>
        [EnumMember]
#endif
        UnInitialized = 0,
#if !MONO
        /// <summary>
        /// The job is submitted to the service.
        /// </summary>
        [EnumMember]
#endif
        Submitted = 1,
#if !MONO
        /// <summary>
        /// The job is ready to be scheduled.
        /// </summary>
        [EnumMember]
#endif
        Ready = 2,
#if !MONO
        /// <summary>
        /// The job is scheduled to run.
        /// </summary>
        [EnumMember]
#endif
        Scheduled = 3,
#if !MONO
        /// <summary>
        /// The job is currently executing on a remote machine.
        /// </summary>
        [EnumMember]
#endif
        Executing = 4,
#if !MONO
        /// <summary>
        /// The job is currently completing execution (and transferring any outputs).
        /// </summary>
        [EnumMember]
#endif
        Completing = 5,
#if !MONO
        /// <summary>
        /// The job has finished execution.
        /// </summary>
        [EnumMember]
#endif
        Completed = 6,
#if !MONO
        /// <summary>
        /// The job was cancelled.
        /// </summary>
        [EnumMember]
#endif
        Cancelled = 7
    }

#if !MONO
    /// <summary>
    /// Specifies the application or job priority level.
    /// </summary>
    [DataContract]
#endif
    public enum PriorityLevel
    {
#if !MONO
        /// <summary>
        /// The application/job has low priority.
        /// </summary>
        [EnumMember]
#endif
        Low = -1,
#if !MONO
        /// <summary>
        /// The application/job has normal priority (default).
        /// </summary>
        [EnumMember]
#endif
        Normal = 0,
#if !MONO
        /// <summary>
        /// The application/job has high priority.
        /// </summary>
        [EnumMember]
#endif
        High = 1
    }

#if !MONO
    /// <summary>
    /// Specifies the type of job.
    /// </summary>
    [DataContract]
#endif
    public enum JobType
    {
#if !MONO
        /// <summary>
        /// The job is a serialised object (that implements the interfaces from the Utilify Framework API)
        /// </summary>
        [EnumMember]
#endif
        CodeModule = 0,
#if !MONO
        /// <summary>
        /// The job is an executable file that does not implement the Utilify Framework API interfaces. (for example, an .exe or a batch file)
        /// </summary>
        [EnumMember]
#endif
        NativeModule = 1
    }

#if !MONO
    /// <summary>
    /// Specifies the platform that is needed to run a job.
    /// </summary>
    [DataContract]
#endif
    public enum ClientPlatform
    {
#if !MONO
        /// <summary>
        /// The job runs on the .NET CLR.
        /// </summary>
        [EnumMember]
#endif
        DotNet = 0,
#if !MONO
        /// <summary>
        /// The job runs on the Java Virtual Machine.
        /// </summary>
        [EnumMember]
#endif
        Java = 1
    }

#if !MONO
    /// <summary>
    /// Specifies the type of dependency of an application or a job.
    /// </summary>
    [DataContract]
#endif
    public enum DependencyType
    {
#if !MONO
        /// <summary>
        /// The dependency is a data file.
        /// </summary>
        [EnumMember]
#endif
        DataFile = 0,
#if !MONO
        /// <summary>
        /// The dependency is a binary compiled to MSIL (Microsoft Intermediate Language), i.e a .NET assembly.
        /// </summary>
        [EnumMember]
#endif
        ClrModule = 1,
#if !MONO
        /// <summary>
        /// The dependency is a binary compiled to Java bytecode (generally in the form of a .jar file)
        /// </summary>
        [EnumMember]
#endif
        JavaModule = 2,
#if !MONO
        /// <summary>
        /// The dependency is a binary executable file. (for example, a .exe file)
        /// </summary>
        [EnumMember]
#endif
        NativeModule = 3,
#if !MONO
        /// <summary>
        /// The dependency is a file that can be fetched from a remote URL accessible from the Executor machine. 
        /// </summary>
        [EnumMember]
#endif
        RemoteUrl = 4
    }

#if !MONO
    /// <summary>
    /// Specifies the mode of retrieval for a result file produced by the execution of a job.
    /// </summary>
    [DataContract]
#endif
    public enum ResultRetrievalMode
    {
#if !MONO
        /// <summary>
        /// The result file is to be transferred back to the client machine that submitted the job.
        /// </summary>
        [EnumMember]
#endif
        BackToOrigin = 0,
#if !MONO
        /// <summary>
        /// The result file is to be stored remotely at a specified server/url.
        /// </summary>
        [EnumMember]
#endif
        StoreRemotely = 1
    }

#if !MONO
    /// <summary>
    /// Specifies the scope of a dependency.
    /// </summary>
    [DataContract]
#endif
    public enum DependencyScope
    {
#if !MONO
        /// <summary>
        /// The dependency is specific to a job.
        /// </summary>
        [EnumMember]
#endif
        Job = 0,
#if !MONO
        /// <summary>
        /// The dependency is specific to an application. All jobs in the application may make use of the dependency.
        /// </summary>
        [EnumMember]
#endif
        Application = 1
    }
}
