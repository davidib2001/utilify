using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform
{
    /// <summary>
    /// Represents the contents of a result file produced by a job.
    /// </summary>
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class ResultContent
    {
#if !MONO
        [DataMember(IsRequired = false)]
#endif
        private byte[] content;

        private string resultId;
        private string jobId;

        private ResultContent() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResultContent"/> class.
        /// </summary>
        /// <param name="resultId">The result id.</param>
        /// <param name="jobId">The job id.</param>
        public ResultContent(string resultId, string jobId)
            : this ()
        { 
            this.JobId = jobId;
            this.ResultId = resultId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResultContent"/> class.
        /// </summary>
        /// <param name="resultId">The result id.</param>
        /// <param name="jobId">The job id.</param>
        /// <param name="content">The content.</param>
        public ResultContent(string resultId, string jobId, byte[] content)
            : this(resultId, jobId)
        {

            //Changed behaviour: if the ctor is used, the instance can still be null, since the result's content has not yet arrived at the manager!

            //if this ctor is used, the content has to be non-null
            //if (content == null)
            //    throw new ArgumentNullException("content");
            //if (content.Length == 0)
            //    throw new ArgumentException("Result content cannot be empty", "content");

            this.content = content;
        }

        /// <summary>
        /// Gets or sets the result id.
        /// </summary>
        /// <value>The result id.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ResultId
        {
            get { return resultId; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("resultId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Result id cannot be empty", "resultId");
                resultId = value;
            }
        }

        /// <summary>
        /// Gets or sets the job id.
        /// </summary>
        /// <value>The job id.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string JobId
        {
            get { return jobId; }
            private set 
            {
                if (value == null)
                    throw new ArgumentNullException("jobId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Job id cannot be empty", "jobId");
                jobId = value;
            }
        }

        /// <summary>
        /// Gets the content.
        /// </summary>
        /// <returns>The result file contents.</returns>
        public byte[] GetContent()
        {
            return content;
        }
    }
}