using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform
{
    /// <summary>
    /// Represents a Dependency such as a file or remote url
    /// </summary>
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class DependencyInfo
    {
        private string id;
        private string filename;
        private string hash;
        private DateTime modified;
        private string name;
        private long size;
        private DependencyType type;

        #region Local file path for use on the client only
        private string localPath = string.Empty;
        /// <summary>
        /// Gets the local path of the dependency file. This property is for internal use only.
        /// </summary>
        public string LocalPath
        {
            get { return localPath; }
        }
        #endregion

        private DependencyInfo() {}

        /// <summary>
        /// Creates an instance of the DependencyInfo class with the specified name, path, and type.
        /// </summary>
        /// <param name="name">name of the dependency</param>
        /// <param name="filePathOrUrl">a local file path or a remote url that specified the dependency location</param>
        /// <param name="type">type of the dependency, which is one of the <see cref="DependencyType">DependencyType</see> values.</param>
        /// <exception cref="System.ArgumentNullException">name is a null reference</exception>
        /// <exception cref="System.ArgumentException">name or filePathOrUrl is an empty string</exception>
        /// <exception cref="System.IO.FileNotFoundException">the type is not a remote url, and the local file is not found at the specified path</exception>
        public DependencyInfo(string name, string filePathOrUrl, DependencyType type) : this()
        {
            //this ctor is used only on the client

            this.Id = string.Empty; //to ensure it is not a null string

            this.Name = name;
            this.Type = type;

            if (type != DependencyType.RemoteUrl)
            {
                
                //it is a local file
                System.IO.FileInfo fi = new System.IO.FileInfo(filePathOrUrl);
                if (!fi.Exists)
                    throw new System.IO.FileNotFoundException("Could not find dependency file", fi.FullName);
                
                this.Modified = fi.LastWriteTimeUtc;
                this.Size = fi.Length;
                this.Filename = fi.Name; //we need only the name : don't expose local path to the remote service

                this.localPath = fi.FullName; //keep it for later : if/when we need to read the file to send its contents

                //calculate hash and set it  
                this.Hash = Hasher.ComputeHash(fi, HashType.SHA256);
            }
            else
            {
                new Uri(filePathOrUrl); //this may throw a UriFormatException, which can be bubbled up
                this.Filename = filePathOrUrl;

                //set the hash to the hash of the url so that we don't get ArgumentExceptions
                this.Hash = Hasher.ComputeHash(filePathOrUrl, HashType.SHA256);

                //remote url : can't set modified / size
                this.Modified = Helper.MinUtcDate;
                this.Size = 0;
            }            
        }

        //krishna: Removed this ctor to bring it in sync with the Java version.
        ///// <summary>
        ///// Creates an instance of the DependencyInfo class with the specified parameters
        ///// </summary>
        ///// <param name="name">name of the dependency</param>
        ///// <param name="filePathOrUrl">filename or url of the dependency</param>
        ///// <param name="modified">last modified time of the dependency file</param>
        ///// <param name="size">size of the dependency file</param>
        ///// <param name="type">the type of dependency</param>
        ///// <param name="hash">a hash of the dependency file contents</param>
        ///// <exception cref="System.ArgumentNullException">name, filePathOrUrl or hash is a null reference</exception>
        ///// <exception cref="System.ArgumentException">name, filePathOrUrl or hash is an empty string</exception>
        //public DependencyInfo(string name, string filePathOrUrl, DateTime modified,
        //    long size, DependencyType type, string hash) : this()
        //{
        //    //this ctor is used only on the client

        //    this.Id = string.Empty; //to ensure it is not a null string

        //    //all arguments are checked in the property setters
        //    this.Name = name;
        //    this.Modified = modified;
        //    this.Size = size;
        //    this.Type = type;
        //    this.Hash = hash;

        //    //set the localfile path too, just in case this ctor is used on the client
        //    if (type != DependencyType.RemoteUrl)
        //    {
        //        //it is a local file
        //        System.IO.FileInfo fi = new System.IO.FileInfo(filePathOrUrl);
        //        if (!fi.Exists)
        //            throw new System.IO.FileNotFoundException("Could not find dependency file", fi.FullName);
        //        this.Filename = fi.Name;

        //        this.localPath = fi.FullName; //keep it for later : if/when we need to read the file to send its contents
        //    }
        //    else
        //    {
        //        //just see if the format is correct:
        //        Uri uri = new Uri(filePathOrUrl); //this may throw a UriFormatException, which can be bubbled up
        //        this.Filename = filePathOrUrl;
        //    }
        //}

        //This constructor is supposed to be used internally for communication between the manager and the executor.
        /// <summary>
        /// Creates an instance of the DependencyInfo class with the specified parameters.
        /// This constructor is for internal use only.
        /// </summary>
        /// <param name="id">id of the dependency</param>
        /// <param name="name">name of the dependency</param>
        /// <param name="filePathOrUrl">filename or url of the dependency</param>
        /// <param name="modified">last modified time of the dependency file</param>
        /// <param name="size">size of the dependency file</param>
        /// <param name="type">the type of dependency</param>
        /// <param name="hash">a hash of the dependency file contents</param>
        /// <exception cref="System.ArgumentNullException">name, filePathOrUrl or hash is a null reference</exception>
        /// <exception cref="System.ArgumentException">name, filePathOrUrl or hash is an empty string</exception>
        public DependencyInfo(string id, string name, string filePathOrUrl, DateTime modified,
            long size, DependencyType type, string hash)
            : this()
        {
            //this ctor is used only on the manager

            //if this ctor is used directly, we still need to validate the Id
            if (id == null)
                throw new ArgumentNullException("id");
            if (id.Trim().Length == 0)
                throw new ArgumentException("Dependency id cannot be empty", "id");

            this.Id = id;
            this.Name = name;

            if (type == DependencyType.RemoteUrl)
            {
                //just see if the format is correct:
                Uri uri = new Uri(filePathOrUrl); //this may throw a UriFormatException, which can be bubbled up
            }

            this.Filename = filePathOrUrl;

            this.Modified = modified;
            this.Size = size;
            this.Type = type;
            this.Hash = hash;
        }

        /// <summary>
        /// Gets the id of the dependency
        /// </summary>
#if !MONO
        [DataMember(IsRequired = false)]
#endif
        public string Id
        {
            get { return id; }
            private set { id = value ?? string.Empty; }
        }

        /// <summary>
        /// Gets the filename
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Filename
        {
            get { return filename; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("filename");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Dependency file/url cannot be empty", "filename");
                filename = value;
            }
        }

        /// <summary>
        /// Gets the hash of the file contents
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Hash
        {
            get { return hash; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("hash");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Dependency file/url cannot be empty", "hash");
                hash = value;
            }
        }

        /// <summary>
        /// Gets the last modified time of the dependency
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime Modified
        {
            get { return modified; }
            private set { modified = Helper.MakeUtc(value); }
        }

        /// <summary>
        /// Gets the name of the dependency
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Name
        {
            get { return name; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("name");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Dependency name cannot be empty", "name");

                name = value;
            }
        }

        /// <summary>
        /// Gets the size of the dependency file. (This value is non-zero only for dependencies which are not of type DependencyType.RemoteUrl
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public long Size
        {
            get { return size; }
            private set 
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Size cannot be less than zero", "size");
                size = value; 
            }
        }

        /// <summary>
        /// Gets the type of the dependency
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DependencyType Type
        {
            get { return type; }
            private set
            {
                type = value;
            }
        }

        /// <summary>
        /// Creates a DependencyInfo object from the given type. The type must belong to an assembly 
        /// which is on the local file system. (i.e dynamic types are not supported)
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static DependencyInfo FromType(Type type)
        {
            if (type == null)
                throw new ArgumentNullException("type");

            System.IO.FileInfo fi = new System.IO.FileInfo(type.Assembly.Location);
            if (!fi.Exists)
                throw new System.IO.FileNotFoundException("Could not load type " + type.Name, type.Assembly.Location);

            DependencyInfo dep = new DependencyInfo(type.Name + "_Assembly", fi.FullName, DependencyType.ClrModule);
            return dep;
        }

        /// <summary>
        /// See <see cref="M:System.Object.Equals">System.Object.Equals()</see>
        /// </summary>
        /// <param name="other">the DependencyInfo object to compare this object to</param>
        /// <returns>true, if the specified object is equal to this object, false otherwise</returns>
        public override bool Equals(object other)
        {
            if (other == null)
                return false;

            if (other.GetType() != this.GetType())
                return false;

            bool areEqual = false;
            DependencyInfo otherInfo = other as DependencyInfo;
            areEqual = (otherInfo.Name == this.Name) &&
                (otherInfo.Type == this.Type) &&
                (otherInfo.Filename == this.Filename) &&
                (otherInfo.Modified == this.Modified) &&
                (otherInfo.Size == this.Size) && 
                (otherInfo.Hash == this.Hash);
            return areEqual;
        }

        private int hashCode = 0;
        /// <summary>
        /// See <see cref="M:System.Object.GetHashCode">System.Object.GetHashCode</see>
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            if (hashCode == 0)
            {
                hashCode = 42 ^ modified.GetHashCode();
                if (type != 0)
                    hashCode = type.GetHashCode() ^ hashCode;
                if (hash != null)
                    hashCode = hash.GetHashCode() ^ hashCode;
                if (filename != null)
                    hashCode = filename.GetHashCode() ^ hashCode;
                if (name != null)
                    hashCode = name.GetHashCode() ^ hashCode;
                if (size != 0)
                    hashCode = hashCode ^ ((int)size ^ (int)(size >> 32));
            }
            return hashCode;
        } //this object is not really intended to be used as a key in a hashtable, but its good to override GetHashCode when overriding Equals

        /// <summary>
        /// Gets a string representation of this object
        /// </summary>
        /// <returns>a string representation of the dependency</returns>
        public override string ToString()
        {
            return string.Format("DependencyInfo : {0}, {1}, type: {2}, modified: {3:r}, hash {4}",
                name, filename, type, modified, hash);
        }
    }
}