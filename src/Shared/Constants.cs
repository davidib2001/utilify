﻿namespace Utilify.Platform
{
    internal static class Constants
    {
        internal const string CompanyName = "Utilify";
        internal const string ProductName = "Utilify Distributed Application Platform";
        internal const string Copyright = "Copyright © 2011 Utilify";
        internal const string Trademarks = CompanyName;

        internal const string FullDateTimeFormat = "dd-MMM-yyyy HH:mm:ss.fff %K";

        internal const int Kilo = 1024;
        internal const int Mega = Kilo * Kilo;
        internal const int Giga = Mega * Kilo;

    }
}
