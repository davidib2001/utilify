#if !MONO
using System.ServiceModel;
using System;

namespace Utilify.Platform
{
    /// <summary>
    /// This class is for internal use only. It wraps the WCF Proxy that derives implements IApplicationManager, and 
    /// IDisposable which handles any errors raised by the Dispose/Close call on the WCF proxy.
    /// </summary> //class name needs to be excluded from Obfuscation, since we create it using reflection.
    internal class ApplicationManagerProxy : WCFProxyBase<IApplicationManager>, IApplicationManager
    {
        private Logger logger = new Logger();

        #region Constructors

        public ApplicationManagerProxy(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        #endregion

        #region IApplicationManager Members

        void IApplicationManager.AbortApplication(string applicationId)
        {
            try
            {
                base.Channel.AbortApplication(applicationId);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        void IApplicationManager.AbortJob(string jobId)
        {
            try
            {
                base.Channel.AbortJob(jobId);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        void IApplicationManager.PauseApplication(string applicationId)
        {
            try
            {
                base.Channel.PauseApplication(applicationId);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        ApplicationStatusInfo IApplicationManager.GetApplicationStatus(string applicationId)
        {
            try
            {
                return base.Channel.GetApplicationStatus(applicationId);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        void IApplicationManager.ResumeApplication(string applicationId)
        {
            try
            {
                base.Channel.ResumeApplication(applicationId);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        string IApplicationManager.SubmitApplication(ApplicationSubmissionInfo appInfo)
        {
            try
            {
                return base.Channel.SubmitApplication(appInfo);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        void IApplicationManager.SendDependency(DependencyContent dependency)
        {
            try
            {
                base.Channel.SendDependency(dependency);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        string[] IApplicationManager.SubmitJobs(JobSubmissionInfo[] jobs)
        {
            try
            {
                return base.Channel.SubmitJobs(jobs);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        JobStatusInfo[] IApplicationManager.GetJobStatus(string[] jobIds)
        {
            try
            {
                return base.Channel.GetJobStatus(jobIds);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        DependencyInfo[] IApplicationManager.GetUnresolvedDependencies(DependencyQueryInfo query)
        {
            try
            {
                return base.Channel.GetUnresolvedDependencies(query);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        ResultStatusInfo[] IApplicationManager.GetJobResults(string jobId)
        {
            try
            {
                return base.Channel.GetJobResults(jobId);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        ResultContent IApplicationManager.GetResult(string resultId)
        {
            try
            {
                return base.Channel.GetResult(resultId);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        JobCompletionInfo IApplicationManager.GetCompletedJob(string jobId)
        {
            try
            {
                return base.Channel.GetCompletedJob(jobId);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        #endregion

    }
}

#endif