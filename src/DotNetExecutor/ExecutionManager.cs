using System;
using System.Collections.Generic;
using System.Security;
using System.Security.Policy;
using System.Threading;
using Utilify.Platform.Execution.Shared;

namespace Utilify.Platform.Executor
{    
    internal class ExecutionManager
    {
        private Dictionary<String, AppDomain> appDomains = new Dictionary<String, AppDomain>();
        private Dictionary<String, IExecutor> executors = new Dictionary<String, IExecutor>();
        private List<JobCompletionInfo> completedJobs = new List<JobCompletionInfo>();
        private Dictionary<String, Thread> executingThreads = new Dictionary<String, Thread>();

        private static readonly Logger logger = new Logger();

        private readonly object syncLock = new object();

        //todoNow: this method has to catch its own exceptions and put it into the execution error log
        /// <summary>
        /// Executes the given job in a seperate app-domain.
        /// This method is thread-safe.
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="applicationId"></param>
        /// <param name="jobDirectory"></param>
        /// <param name="applicationDirectory"></param>
        /// <param name="type"></param>
        /// <param name="jobInstance"></param>
        internal void ExecuteJob(String jobId, String applicationId, String jobDirectory, 
            String applicationDirectory, JobType type, byte[] jobInstance) 
        {
            //todoNow: argument validation

            logger.Debug("Starting execution for job : " + jobId);

            String typeName = ExecutionHelper.GetExecutorTypeName(type);
            if (String.IsNullOrEmpty(typeName))
                throw new ArgumentException("Invalid Job Type specified: " + type,"type");

            // Check if AppDomain exists for this Application
            // Create appDomain if needed or get existing AppDomain
            AppDomain domain;
            lock (syncLock)
            {
                if (!appDomains.TryGetValue(applicationId, out domain))
                {
                    domain = CreateAppDomain(applicationId, applicationDirectory);
                    appDomains[applicationId] = domain;
                }
            }

            String executionAssemblyLocation = ExecutionHelper.GetExecutionAssemblyLocation(domain);
            // Load IExecutor into AppDomain
            IExecutor executor = 
                (IExecutor)domain.CreateInstanceFromAndUnwrap(executionAssemblyLocation, typeName);
            logger.Debug("Created executor in secondary app domain: " + domain);

            // Subscribe to Job finished and error event from IExecutor - via the RemotingEventHelper.
            RemotingEventHelper helper = new RemotingEventHelper(executor);
            helper.ExecutionComplete += new EventHandler<ExecutionCompleteEventArgs>(this.Execution_Complete);

            logger.Debug("Running job in new thread.");
            // Start executing Job in new thread so that we can return to the client without timing out.
            JobExecutionInfo jobExecutionInfo = new JobExecutionInfo(jobId, applicationId, jobInstance, jobDirectory);
            Thread thread = new Thread(new ParameterizedThreadStart(executor.ExecuteTask));
            thread.Priority = ThreadPriority.AboveNormal;
            thread.IsBackground = true;

            // Keep reference to Thread by JobId. This is so we can quickly kill the execution when abort job is called.
            lock (syncLock)
            {
                // Note: If Job has been reset on the Manager, we could be overwriting the values at jobId index for the
                //       aborted job with the new version here. This is OK and checked in the Execution_Complete event handler.

                // Keep reference to IExecutor by JobId (moved this line here from a few lines above so that adding
                // executor and thread references happen during the same lock).
                executors[jobId] = executor;
                // Keep reference to the Thread
                executingThreads[jobId] = thread;
            }
            logger.Debug("JobId:" + jobId + " added to lists");

            //start the thread at the end:
            //to ensure the thread doesn't complete *before* we even add the executor to our internal list
            thread.Start(jobExecutionInfo);
        }

        private void Execution_Complete(object sender, ExecutionCompleteEventArgs args)
        {
            // If Complete event is raised due to the Job being Aborted, then we just want to
            // discard the Job and not add to completedJobs list or even create the JobCompletionInfo.

            try
            {
                logger.Debug("Handling execution complete event for Job : " + args.JobId);
                logger.Debug("Job exec log: " + args.ExecutionLog + "\nJob error log:" +
                    args.ExecutionError);
                //Get a reference to the executor from the helper
                RemotingEventHelper helper = (RemotingEventHelper)sender;
                //unsubscribe from the event - so that the helper unsubscribes from the remoted-executor in the other appDomain
                helper.ExecutionComplete -= this.Execution_Complete;

                lock (syncLock)
                {
                    // If Aborted already just return
                    // We can tell if the Job has been aborted if this event is raised but the Job no longer has entries in the
                    // executors or executingThreads lists. There is a special case where the Job could have been reset and removed from
                    // the lists, but the aborted/reset job was then reassigned to this executor and entries put back into the lists.
                    // In this case we check that the executor raising this event is the same as the one stored in the executors list.
                    // if not, then the Job was aborted, removed and replaced with a new version of the same Job. We return here. 
                    // If not, the event raised by the aborted Job will be counted as the complete event of the reassigned Job, 
                    // and the aborted/reset (incorrect) Job will be sent back to the Manager.
                    if (!executingThreads.ContainsKey(args.JobId) || 
                        !executors.ContainsKey(args.JobId) ||
                        helper.Executor != executors[args.JobId]) // Check if Reset Job has been replaced with re-assigned version.
                    {
                        logger.Debug("Job was Aborted, so discarding the Job here. " + args.JobId);
                        return;
                    }
                }

                logger.Debug("Creating job completion info instance. Execution exception is null? " + (args.ExecutionException == null));

                JobCompletionInfo info =
                    new JobCompletionInfo(
                        args.JobId,
                        args.ApplicationId,
                        args.FinalJobInstance,
                        args.ExecutionException,
                        args.ExecutionLog.ToString(),
                        args.ExecutionError.ToString(),
                        args.CpuTime);
                
                lock (syncLock)
                {
                    executors.Remove(args.JobId);
                    completedJobs.Add(info);
                    executingThreads.Remove(args.JobId);
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error handling Execution_Complete event: " + ex.ToString());
            }
        }

        /// <summary>
        /// Aborts the job with the given id. This method is thread-safe.
        /// </summary>
        /// <param name="jobId"></param>
        public void AbortJob(String jobId)
        {
            //don't throw an exception on invalid jobId
            logger.Debug("Aborting job : " + jobId);
            // get the job thread.
            // kill the thread!
            // remove the thread.
            lock (syncLock)
            {
                try
                {
                    // Does this throw an exception Aborting the thread?
                    Thread t = executingThreads[jobId];
                    Helper.WaitAndAbort(t, TimeSpan.FromMilliseconds(1000));
                    executingThreads.Remove(jobId);
                    executors.Remove(jobId);
                    logger.Debug("JobId:" + jobId + " removed from lists");
                }
                catch (Exception e)
                {
                    logger.Warn("Exception caught while Aborting Thread!", e);
                }
            }
            // should we check completedJobs too?
        }

        /// <summary>
        /// Gets the jobs completed since the last call to this method.
        /// This method is thread-safe.
        /// </summary>
        /// <returns></returns>
        internal JobCompletionInfo[] GetCompletedJobs()
        {
            JobCompletionInfo[] infos = null;
            int numExecuting = 0;
            lock (syncLock)
            {
                infos = completedJobs.ToArray();
                //inside the lock region, to get an accurate count.
                numExecuting = executingThreads.Count;
            }
            logger.Debug("# executing threads: " + numExecuting);
            logger.Debug("# completed jobs: " + infos.Length);

            return infos;
        }

        internal void RemoveCompletedJobs(JobCompletionInfo[] completedJobs)
        {
            if (completedJobs == null || completedJobs.Length == 0)
                return;

            lock (syncLock)
            {
                foreach (JobCompletionInfo job in completedJobs)
                {
                    if (job != null && this.completedJobs.Contains(job))
                    {
                        this.completedJobs.Remove(job);
                    }
                }
            }
        }

        private static AppDomain CreateAppDomain(String applicationId, String applicationDirectory)
        {
            AppDomainSetup info = new AppDomainSetup();
            info.ApplicationBase = applicationDirectory;
            logger.Info("Creating app-domain with base dir = '{0}'", info.ApplicationBase);
            logger.Info("Shadow copy enabled = '{0}', at '{1}'", info.ShadowCopyFiles, info.ShadowCopyDirectories);

            // todoLater: Set access permissions
            PermissionSet ps = (PolicyLevel.CreateAppDomainLevel()).GetNamedPermissionSet("FullTrust");
#if MONO
            AppDomain domain = AppDomain.CreateDomain(applicationId, null, info);
#else
            AppDomain domain = AppDomain.CreateDomain(applicationId, null, info, ps, null);
#endif

            return domain;

            //todoLater: if the appdomains are in the same process - 
            //we need to change the CLR Policy (using IClrPolicyManager) to prevent unhandled exceptions from threads in secondary
            //from crashing the main app.
        }
    }
}
