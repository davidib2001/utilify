using System;

using Utilify.Platform.Contract;

namespace Utilify.Platform.Executor
{
    internal class Processor
    {
        private string name; // OperatingSystem's name/id for this CPU used for linking by performance and limit infos
        private string description; // CPU string
        private string vendor;
        private Architecture architecture;
        private double speedMax; //(in  Ghz)
        private double speedCurrent; //(in Ghz)
        //private int powerLevel;
        private double loadTotalCurrent; //(in %)
        private double loadUsageCurrent; //(in %)
        private double loadUsageLimit; //(in %)

        private Processor(){}

        public Processor(string name, string description, string vendor, Architecture architecture,
            double speedMax, double speedCurrent, double loadTotalCurrent, double loadUsageCurrent, double loadUsageLimit)
        {
            this.name = name;
            this.description = description;
            this.vendor = vendor;
            this.architecture = architecture;
            this.speedMax = speedMax;
            this.speedCurrent = speedCurrent;
            this.loadTotalCurrent = loadTotalCurrent;
            this.loadUsageCurrent = loadUsageCurrent;
            this.loadUsageLimit = loadUsageLimit;
        }

        public Processor(ProcessorSystemInfo info)
        {
            this.name = info.Name;
            this.description = info.Description;
            this.vendor = info.Vendor;
            this.architecture = info.Architecture;
            this.speedMax = info.SpeedMax;
        }

        public void UpdatePerformance(ProcessorPerformanceInfo info)
        {
            if (!info.Name.Equals(this.name))
                throw new ArgumentException("Specified Processor Name does not match current Name. Cannot update.", "info.Name");

            this.speedCurrent = info.SpeedCurrent;
            this.loadTotalCurrent = info.LoadTotalCurrent;
            this.loadUsageCurrent = info.LoadUsageCurrent;
        }

        public void UpdateLimits(ProcessorLimitInfo info)
        {
            if (!info.Name.Equals(this.name))
                throw new ArgumentException("Specified Processor Name does not match current Name. Cannot update.", "info.Name");

            this.loadUsageLimit = info.LoadUsageLimit;
        }

        public string Name
        {
            get { return name; }
        }

        public string Description
        {
            get { return description; }
        }

        public string Vendor
        {
            get { return vendor; }
        }

        public Architecture Architecture
        {
            get { return architecture; }
        }

        public double SpeedMax
        {
            get { return speedMax; }
        }

        public double SpeedCurrent
        {
            get { return speedCurrent; }
        }

        public double LoadTotalCurrent
        {
            get { return loadTotalCurrent; }
        }

        public double LoadUsageCurrent
        {
            get { return loadUsageCurrent; }
        }

        public double LoadUsageLimit
        {
            get { return loadUsageLimit; }
        }

        public ProcessorSystemInfo ToProcessorSystemInfo()
        {
            return new ProcessorSystemInfo(this.name, this.description, this.vendor, this.architecture, this.speedMax);
        }

        public ProcessorPerformanceInfo ToProcessorPerformanceInfo()
        {
            return new ProcessorPerformanceInfo(this.name, this.speedCurrent, this.loadTotalCurrent, this.loadUsageCurrent);
        }

        public ProcessorLimitInfo ToProcessorLimitInfo()
        {
            return new ProcessorLimitInfo(this.name, this.loadUsageLimit);
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} : {4} Ghz Max {5} Ghz Current : {6}% Total Load {7}({8})% Usage Current (Limit)", name, description, vendor, architecture, 
                speedMax, speedCurrent, loadTotalCurrent, loadUsageCurrent, loadUsageLimit);
        }

        //kna: changed this to use Enum.Parse in place of this
        //public static Architecture ParseArchitecture(String value)
        //{
        //    Architecture arch = Architecture.None;
        //    value = value.ToUpperInvariant();

        //    switch (value)
        //    {
        //        case "X86":
        //            arch = Architecture.X86;
        //            break;
        //        case "AMD64":
        //            arch = Architecture.Amd64;
        //            break;
        //        case "IA64":
        //            arch = Architecture.IA64;
        //            break;
        //        case "MSIL":
        //            arch = Architecture.MSIL;
        //            break;
        //        default:
        //            arch = Architecture.None;
        //            break;
        //    }

        //    return arch;
        //}
    }
}
