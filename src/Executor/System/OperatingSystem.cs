using Utilify.Platform.Contract;

namespace Utilify.Platform.Executor
{
    internal class OperatingSystem
    {
        private string name;
        private string version;

        private OperatingSystem() { }

        public OperatingSystem(string name, string version)
        {
            this.name = name;
            this.version = version;
        }

        public OperatingSystem(OSInfo info)
        {
            this.name = info.Name;
            this.version = info.Version;
        }

        public string Name
        {
            get { return name; }
        }

        public string Version
        {
            get { return version; }
        }

        public OSInfo ToOSInfo()
        {
            return new OSInfo(this.name, this.version);
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", name, version);
        }
    }
}
