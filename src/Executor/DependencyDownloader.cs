﻿using System;
using System.IO;

namespace Utilify.Platform.Executor
{
    interface IDependencyDownloader
    {
        bool GetDependency(DependencyInfo depInfo, string localPath);
    }

    abstract class DependencyDownloader<T> : IDependencyDownloader where T : IManagerService
    {
        protected Logger logger = new Logger();
        protected T proxy;

        protected DependencyDownloader(T proxy)
        {
            if (proxy == null)
                throw new ArgumentNullException(Errors.ValueCannotBeNull, "proxy");

            this.proxy = proxy;
        }

        public abstract bool GetDependency(DependencyInfo depInfo, string localPath);
    }

    class RemoteDependencyDownloader : DependencyDownloader<IDataTransferService>
    {
        internal RemoteDependencyDownloader(IDataTransferService proxy) : base(proxy)
        {
        }

        public override bool GetDependency(DependencyInfo dependency, string localPath)
        {
            if (dependency == null || string.IsNullOrEmpty(dependency.Filename))
            {
                throw new ArgumentException(Errors.ValueCannotBeNullOrEmpty, "dependency");
            }

            if (dependency.Type != DependencyType.RemoteUrl)
            {
                throw new ArgumentException(string.Format("Invalid dependency type: {0}", dependency.Type), "dependency");
            }

            //we don't really know the object id here :/
            DataObjectInfo info = new DataObjectInfo() {                 
                ObjectName = dependency.Filename 
            };

            DataTransferInfo data = proxy.Get(info);

            using (data)
            {
                if (!File.Exists(localPath))
                {
                    IOHelper.WriteFile(localPath, data.Data, 32 * Constants.Kilo);
                }
            }

            return true;
        }
    }

    class SimpleDependencyDownloader : DependencyDownloader<IJobManager>
    {
        internal SimpleDependencyDownloader(IJobManager proxy)
            : base(proxy)
        {
        }

        public override bool GetDependency(DependencyInfo dependency, string localPath)
        {
            if (dependency == null || string.IsNullOrEmpty(dependency.Filename))
            {
                throw new ArgumentException(Errors.ValueCannotBeNullOrEmpty, "dependency");
            }

            if (dependency.Type == DependencyType.RemoteUrl)
            {
                throw new ArgumentException(string.Format("Invalid dependency type: {0}", dependency.Type), "dependency");
            }

            DependencyContent content = proxy.GetDependency(dependency.Id);
            byte[] data = (content != null) ? content.GetContent() : null;
            
            if (data != null && data.Length > 0)
            {
                logger.Debug("Downloaded dependency content. {0} bytes", data.Length);

                // Write the file if the file doesnt already exist
                if (!File.Exists(localPath))
                {
                    logger.Debug(String.Format("Writing file {0} to {1}.", Path.GetFileName(localPath), localPath));
                    IOHelper.WriteFile(localPath, new MemoryStream(data), 8 * Constants.Kilo);
                }

                return true;
            }

            return false;
        }
    }
}
